﻿(function () {
    'use strict';

    angular
        .module('SaludSystem')
        .controller('CamasController', ['$scope', '$q', '$timeout', 'ssDate', 'Service', 'Auth', 'Consultas', CamasController])
        .controller('RecienNacController', ['$scope', '$q', '$timeout', 'ssDate', 'Service', 'Auth', RecienNacController])
        .controller('EgresoPacController', ['$scope', '$q', '$timeout', 'ssDate', 'Service', 'Auth', EgresoPacController])
        .controller('AdmisionesController', ['$scope', '$q', '$timeout', 'ssDate', 'Service', 'Auth', AdmisionesController])
        .controller('UnificacionDocumentoController', ['$scope', '$q', '$timeout', 'ssDate', 'Service', 'Auth', UnificacionDocumentoController])
        .controller('Cumplimiento_OrdenesController', ['$scope', '$q', '$timeout', 'ssDate', 'Datos', 'Service', 'Auth','Paciente', Cumplimiento_OrdenesController])
        .controller('F_OrdenesmedicasController', ['$scope', '$q', '$timeout', 'ssDate', 'Datos', 'Service', 'Auth', 'Consultas', F_OrdenesmedicasController])
        .controller('ResultOrdenesDiagController', ['$scope', '$q', '$timeout', 'ssDate', 'Datos', 'Service', 'Auth', ResultOrdenesDiagController])
        .controller('RecaudoCitaController', ['$scope', '$q', '$timeout', 'ssDate', 'Service', 'Auth', 'Datos', 'Consultas', RecaudoCitaController])
        .controller('CajaController', ['$scope', '$q', '$timeout', 'ssDate', 'Service', 'Auth', 'Datos', CajaController])
        .controller('OrdenFisicaController', ['$scope', '$q', '$timeout', 'ssDate', 'Service', 'Auth', 'Datos', OrdenFisicaController])
        .controller('ReportesLabController', ['$scope', '$q', '$timeout', 'ssDate', 'Datos', 'Service', 'Auth', ReportesLabController])
        .controller('RemisionesLabController', ['$scope', '$q', '$timeout', 'ssDate', 'Datos', 'Service', 'Auth', RemisionesLabController])
        .controller('PanelFisioController', ['$scope', '$q', '$timeout', 'ssDate', 'Datos', 'Service', 'Auth', 'Paciente', '$sce', PanelFisioController])
        .controller('ConceptosController', ['$scope', 'Service', 'Auth', '$q', 'Paciente', 'ssDate', '$timeout', ConceptosController])
        .controller('ReservarCamaController', ['$scope', 'Service', 'Auth', '$q', 'Paciente', 'ssDate', '$timeout', ReservarCamaController])
        .controller('RecaudoCitaRESController', ['$scope', '$q', '$timeout', 'ssDate', 'Service', 'Auth', 'Datos', 'Consultas', RecaudoCitaRESController])
        .controller('cuadre_de_cajaController', ['$scope', '$q', '$timeout', 'ssDate', 'Service', 'Auth', 'Datos', 'Consultas', cuadre_de_cajaController])
        .controller('caja_egresoController', ['$scope', '$q', '$timeout', 'ssDate', 'Service', 'Auth', 'Datos', 'Consultas', caja_egresoController])
        


    function CamasController($scope, $q, $timeout, ssDate, Service, Auth, Consultas) {
        $scope.json = {};
        $scope.cod_user = Auth.getUserObj().codigo;
        $scope.auxuser = Auth.getUserObj().usuario1;
        $scope.swExist = false;
        $scope.openWaiting = false;
        $scope.keySede = { value: '' };
        $scope.tablacamas = [];
     console.log("yera")
     console.log("yera rama")
     console.log("chris rama")
        console.log("yera CONFLICTO")
        console.log("yera CONFLICTO 2")

        console.log("A PRUEBA DE FUEGO!")
        $scope.profilenivel = angular.toJson({ modulo: 'ADMISIONES', ventana: '17', usuario: $scope.auxuser });

        $scope.disabledbtn = { buscar: false, imprimir: false, nuevo: false, editar: true, eliminar: true };
        $scope.showbtn = { buscar: true, imprimir: true, nuevo: true, editar: false, eliminar: false };

        $scope.dataSede = {
            id: 'dataSede',
            component: [{ id: 'formCamas-buscar', type: 'btn' }],
            table: 'centros',
            column: [
                { name: 'rtrim(ltrim(codigo))', as: 'Codigo', visible: true },
                { name: 'rtrim(ltrim(nombre))', as: 'Nombre', visible: true },

            ],
            inner_join: [
            ],
            where: [{ col: 'piso', filter: "='1'", join: 'or' }, { col: 'qx', filter: "='1'", join: '' }],
            groupby: false,
            orderby: [],
            title: 'Busqueda Sede o Piso',
            required_name: "Busqueda Sede o Piso"
        };
        $scope.rsSede = function (v) {
            var x = JSON.parse(v);

            $scope.json.codigoiss = x.Codigo;
        };

        $scope.dataBuscador = {
            id: 'dataBuscador',
            component: [{ id: 'formCamas-buscar', type: 'btn' }],
            table: 'camas',
            column: [
                { name: 'rtrim(ltrim(id))', as: 'Id', visible: false },
                { name: 'rtrim(ltrim(codigo))', as: 'Codigo', visible: true },
                { name: 'rtrim(ltrim(nombre))', as: 'Nombre', visible: true },
                { name: 'rtrim(ltrim(codigoiss))', as: 'Codigoiss', visible: false },
                { name: 'rtrim(ltrim(reservada))', as: 'Reservada', visible: false },
                { name: 'rtrim(ltrim(detalle))', as: 'Detalle', visible: false },
            ],
            inner_join: [
            ],
            where: {},
            groupby: false,
            orderby: [],
            title: 'Busqueda'
        };
        $scope.rsBuscador = function (v) {
            $scope.json = {};
            var x = JSON.parse(v);

            $scope.json.codigo = x.Codigo;
            $scope.json.nombre = x.Nombre;
            $scope.json.id = x.Id.trim();
            $scope.json.detalle = x.Detalle.trim();
            $scope.keySede.value = x.Codigoiss;
            if (x.Reservada == 1) {
                $('#reservada').prop("checked", true);
                $scope.json.reservada = 1;
            }
            if (x.Reservada == 0) {
                $('#reservada').prop("checked", false);
                $scope.json.reservada = 0;
            }
            $scope.disabledbtn.editar = false;
            $scope.disabledbtn.eliminar = false;
        };

        $scope.buscarCamas = function () {
            $scope.query = "select rtrim(ltrim(c.codigo)) as codigo, rtrim(ltrim(c.nombre)) as cama, rtrim(ltrim(c.codigoiss)) as codigoiss, rtrim(ltrim(ce.nombre)) as sede, c.reservada, rtrim(ltrim(c.detalle)) as detalle from camas as c inner join centros as ce on ce.codigo = c.codigoiss order by c.codigoiss, c.codigo";
            $scope.y = Consultas.read($scope.query);
            console.log($scope.y);
            $timeout(function () {
                var rs = $scope.y.$$state.value;
                console.log(rs);
                $scope.tablacamas = rs;
            }, 3000);
        }

        $scope.nuevo = function () {
            $scope.json = {};
            $scope.json.detalle = '';
        };
        $scope.guardar = function () {
            var defender = $q.defer();
            var promise = defender.promise;
            //if ($scope.json.reservada == true) {
            //    $scope.json.reservada = 1;
            //}
            //if ($scope.json.reservada == false) {
            //    $scope.json.reservada = 0;
            //}
            $scope.json.codigosoat = '-';
            $scope.json.estado = '2';
            if ($scope.json.detalle == undefined) {
                $scope.json.detalle = '';
            }
            $timeout(function () {
                try {
                    var rs = false;
                    Service.getCrud({ json: angular.toJson($scope.json), codigo_usuario: $scope.cod_user }, "/Admision.svc/CreateCamas").then(function (d) {
                        rs = JSON.parse(d.data);
                        if (rs[0] == '?') {
                            alert(rs);
                        }
                        defender.resolve(rs);
                    });

                } catch (e) {
                    defender.reject(e);
                }
            }, 3000);
            return promise;
        };
        $scope.existcod = function (item) {
            $scope.query1 = '';
            $scope.y1 = undefined;
            if (item != undefined) {
                if (item != '') {
                    if (item != null) {
                        console.log('exist:', item);
                        $scope.query1 = "select * from camas where codigo = '" + item + "'";
                        $scope.y1 = Consultas.read($scope.query1);

                        $timeout(function () {
                            var rs1 = $scope.y1.$$state.value[0];

                            if (rs1 != undefined && rs1 != null && rs1 != '') {
                                $scope.swExist = true;
                            } else {
                                $scope.swExist = false;
                            }
                        }, 1000);
                    }
                }
            }

        }
        $scope.existcc = function (item) {
            if (item != undefined) {
                if (item != '') {
                    if (item != null) {
                        Service.getCrud({ codigo_cc: item }, "/Admision.svc/ExistCC").then(function (d) {
                            var rs = JSON.parse(d.data);
                            if (rs == false) {
                                $scope.json.codigoiss = '';
                                $('#focus').focus();
                                alert("SEDE / PISO NO EXISTE");
                            }
                        });
                    }
                }
            }

        }
        $scope.editar = function () {
            $('#codigo').prop('disabled', true);
        }
        $scope.actualizar = function () {
            var defender = $q.defer();
            var promise = defender.promise;
            //if ($scope.json.reservada == true) {
            //    $scope.json.reservada = 1;
            //}
            //if ($scope.json.reservada == false) {
            //    $scope.json.reservada = 0;
            //}
            $scope.json.codigosoat = '-';
            $scope.json.estado = '2';
            $timeout(function () {
                try {
                    var rs = false;
                    Service.getCrud({ json: angular.toJson($scope.json), codigo_usuario: $scope.cod_user }, "/Admision.svc/UpdateCamas").then(function (d) {
                        rs = JSON.parse(d.data);
                        if (rs[0] == '?') {
                            alert(rs);
                        }
                        defender.resolve(rs);
                    });
                } catch (e) {
                    defender.reject(e);
                }
            }, 3000);
            return promise;
        }
        $scope.eliminar = function () {
            var defender = $q.defer();
            var promise = defender.promise;
            $timeout(function () {
                try {
                    var rs = false;
                    Service.getCrud({ codigo: $scope.json.codigo, codigo_usuario: $scope.cod_user }, "/Admision.svc/DeleteCamas").then(function (d) {
                        rs = JSON.parse(d.data);
                        if (rs[0] == '?') {
                            alert(rs);
                        }
                        defender.resolve(rs);
                    });
                } catch (e) {
                    defered.reject(e);
                }
            }, 3000);

            return promise;
        }
        $scope.imprimir = function () {
            $scope.openWaiting = true;
            $scope.buscarCamas();
            $timeout(function () {
                $('#menu').addClass("ss-nav-inactive");
                $('#wrapper').addClass("ss-full-view");
                window.print();
                $scope.openWaiting = false;
            }, 6000);
        }
    }
    function RecienNacController($scope, $q, $timeout, ssDate, Service, Auth) {
        $scope.json = {};
        $scope.cod_user = Auth.getUserObj().codigo;
        $scope.key1 = { value: '' };
        $scope.key2 = { value: '' };
        $scope.key3 = { value: '' };
        $scope.shw1 = true;
        $scope.shw2 = true;
        $scope.shw3 = true;
        $scope.auxuser = Auth.getUserObj().usuario1;

        $scope.profilenivel = angular.toJson({ modulo: 'ADMISIONES', ventana: '15', usuario: $scope.auxuser });

        $scope.guardar = function () {
            var defender = $q.defer();
            var promise = defender.promise;
            if ($scope.json.factura == undefined) {
                if ($scope.json.factura == null) {
                    $scope.json.factura = 0;

                }
            }
            if ($scope.json.hora_muerte == undefined) {
                if ($scope.json.hora_muerte == null) {
                    $scope.json.hora_muerte = ' ';

                }
            }
            if ($scope.fecha_muerte == undefined) {
                if ($scope.fecha_muerte == null) {
                    $scope.json.fecha_muerte = new Date('01/01/1901');

                }
            } else {
                $scope.json.fecha_muerte = $scope.fecha_muerte;
            }

            if ($scope.json.cod_cau_bas == undefined) {
                if ($scope.json.cod_cau_bas == null) {
                    $scope.json.cod_cau_bas = ' ';

                }
            }

            if ($scope.hora_muerte != undefined) {
                if ($scope.hora_muerte.length != 0) {
                    $scope.json.hora_muerte = ssDate.dateToString($scope.hora_muerte, 'HH:mm');
                }
            }
            else {
                $scope.json.hora_muerte = '';
            }

            if ($scope.hora_nac.length != 0) {
                $scope.json.hora_nac = ssDate.dateToString($scope.hora_nac, 'HH:mm');
            }

            $timeout(function () {
                try {
                    var rs = false;
                    Service.getCrud({ json: angular.toJson($scope.json), codigo_usuario: $scope.cod_user }, "/Admision.svc/CreateRecienNac").then(function (d) {
                        rs = JSON.parse(d.data);
                        defender.resolve(rs);
                    });

                } catch (e) {
                    defender.reject(e);
                }
            }, 3000);
            return promise;
        };
        $scope.nuevo = function () {
            $scope.json.fuente = '2016';
            $scope.json.sexo = 1;
            $scope.json.control_pre = 1;
        }
        $scope.rsBuscadorRecienNac = function (v) {
            $scope.json = {};
            $scope.cod = JSON.parse(v).Codigo;
            if ($scope.cod.trim() != '') {
                Service.getCrud({ codigo: $scope.cod }, "/Admision.svc/ReadRecienNac").then(function (d) {
                    var j = JSON.parse(d.data);
                    $scope.json = j;
                    $scope.json.fecha_nac = ssDate.militodate(j.fecha_nac);
                    $scope.hora_nac = ssDate.stringToTime(j.hora_nac);
                    if (ssDate.militodate(j.fecha_muerte) != '01/01/1901') {
                        $scope.fecha_muerte = ssDate.militodate(j.fecha_muerte);
                        $scope.shw3 = true;
                    } else {
                        $scope.fecha_muerte = null;
                        $scope.shw3 = false;
                    }
                    if (j.hora_muerte.length <= 2) {
                        $scope.shw2 = false;
                    } else {
                        $scope.hora_muerte = ssDate.stringToTime(j.hora_muerte);
                        $scope.shw2 = true;
                    }
                    if (j.cod_cau_bas.length >= 2) {
                        $scope.key3.value = j.cod_cau_bas;
                        $scope.shw1 = true;
                    } else {
                        $scope.shw1 = false;
                    }
                    $scope.key1.value = j.codigo;
                    $scope.key2.value = j.cod_diag_rec;
                    $scope.disabledbtn.editar = false;
                    $scope.disabledbtn.eliminar = false;
                });
            }
        };
        $scope.recordRecienNac = {
            id: 'recordCamas',
            component: [{ id: 'formRecienNac-buscar', type: 'btn' }],
            table: 'recien01',
            column: [
                { name: 'codigo', as: 'Codigo', visible: true },
                { name: 'historia', as: 'Historia', visible: true }
            ],
            where: {},
            groupby: false,
            orderby: [],
            title: 'Busqueda de Registros'
        };
        $scope.search_histo = function (item) {
            if (item != undefined) {
                if (item != null) {
                    if (item != '') {
                        $scope.code = item;
                        Service.getCrud({ codigo: $scope.code }, "/Admision.svc/HistoriaRecienNac").then(function (d) {
                            var j = JSON.parse(d.data);
                            $scope.json.historia = j;
                        });

                    }
                }
            }
        }
        $scope.editar = function () {
            $('#brPaciente-clave').prop('disabled', true);
            $('#brPaciente-valor').prop('disabled', true);
            $('#brPaciente-btn').prop('disabled', true);
        }
        $scope.actualizar = function () {
            var defender = $q.defer();
            var promise = defender.promise;
            if ($scope.json.factura == undefined) {
                if ($scope.json.factura == null) {
                    $scope.json.factura = 0;

                }
            }
            if ($scope.json.hora_muerte == undefined) {
                if ($scope.json.hora_muerte == null) {
                    $scope.json.hora_muerte = ' ';

                }
            }
            if ($scope.fecha_muerte == undefined) {
                if ($scope.fecha_muerte == null) {
                    $scope.json.fecha_muerte = new Date('01/01/1901');

                }
            } else {
                $scope.json.fecha_muerte = $scope.fecha_muerte;
            }
            if ($scope.json.cod_cau_bas == undefined) {
                if ($scope.json.cod_cau_bas == null) {
                    $scope.json.cod_cau_bas = ' ';

                }
            }

            if ($scope.hora_muerte != undefined) {
                if ($scope.hora_muerte.length != 0) {
                    $scope.json.hora_muerte = ssDate.dateToString($scope.hora_muerte, 'HH:mm');
                }
            }
            else {
                $scope.json.hora_muerte = '';
            }

            if ($scope.hora_nac.length != 0) {
                $scope.json.hora_nac = ssDate.dateToString($scope.hora_nac, 'HH:mm');
            }
            $timeout(function () {
                try {
                    var rs = false;
                    Service.getCrud({ json: angular.toJson($scope.json), codigo_usuario: $scope.cod_user }, "/Admision.svc/UpdateRecienNac").then(function (d) {
                        rs = JSON.parse(d.data);
                        defender.resolve(rs);
                    });
                } catch (e) {
                    defender.reject(e);
                }
            }, 3000);
            return promise;
        }
        $scope.eliminar = function () {
            var defender = $q.defer();
            var promise = defender.promise;
            $timeout(function () {
                try {
                    var rs = false;
                    Service.getCrud({ codigo: $scope.json.codigo, codigo_usuario: $scope.cod_user }, "/Admision.svc/DeleteRecienNac").then(function (d) {
                        rs = JSON.parse(d.data);
                        defender.resolve(rs);
                    });
                } catch (e) {
                    defered.reject(e);
                }
            }, 3000);

            return promise;
        }
        /*BUSQUEDAS RAPIDAS*/

        $scope.dataBuscador_Paciente = {
            id: 'buscadorPaciente',
            component: [{ id: 'brPaciente', type: '' }],
            table: 'paciente',
            column: [
                { name: 'CODIGO', as: 'Documento', visible: true },
                { name: 'NOMBRE', as: 'Nombre', visible: true },
                { name: 'snombre', as: 'Nombre2', visible: true },
                { name: 'ppellido', as: 'Apellido', visible: true },
                { name: 'sapellido', as: 'Apellido2', visible: true }
            ],
            where: [{ col: ' sexo', filter: " = '2'", join: '' }],
            groupby: false,
            orderby: [],
            title: 'Buscar Madre',
        };
        $scope.resultadoBRPaciente = function (d) {

            var j = JSON.parse(d);
            $scope.json.codigo = j.Documento;
            $scope.search_histo($scope.json.codigo);
        }

        $scope.dataBuscador_Diag = {
            id: 'buscadorDiag',
            component: [{ id: 'brDiag', type: '' }],
            table: 'diagnosticos',
            column: [
                { name: 'CODIGO', as: 'Codigo', visible: true },
                { name: 'NOMBRE', as: 'Nombre', visible: true }
            ],
            where: [],
            groupby: false,
            orderby: [],
            title: 'Buscar Diagnostico',
        };
        $scope.resultadoBRDiag = function (d) {

            var j = JSON.parse(d);
            $scope.json.cod_diag_rec = j.Codigo;

        }

        $scope.dataBuscador_Diag2 = {
            id: 'buscadorDiag2',
            component: [{ id: 'brDiag2', type: '' }],
            table: 'diagnosticos',
            column: [
                { name: 'CODIGO', as: 'Codigo', visible: true },
                { name: 'NOMBRE', as: 'Nombre', visible: true }
            ],
            where: [],
            groupby: false,
            orderby: [],
            title: 'Buscar Dignostico',
        };
        $scope.resultadoBRDiag2 = function (d) {

            var j = JSON.parse(d);
            $scope.json.cod_cau_bas = j.Codigo;

        }

        /*FUCTION*/

        $scope.maxeg = function (i) {
            var item = parseInt(i);
            if (item != null) {
                if (item != undefined) {
                    if (item > 42) {
                        alert('El valor no puede ser superior a 42');
                        $scope.json.edad_gest = '';
                        $('#focus').focus();
                    }

                }
            }

        }
        $scope.maxpeso = function (i) {
            var item = parseInt(i);
            if (item != null) {
                if (item != undefined) {
                    if (item < 1800) {
                        alert('El valor no puede ser inferior a 1800');
                        $scope.json.peso = '';
                        $('#focusp').focus();
                    }

                }
            }

        }


        $scope.disabledbtn = { buscar: false, imprimir: true, nuevo: false, editar: true, eliminar: true };
        $scope.showbtn = { buscar: true, imprimir: true, nuevo: true, editar: true, eliminar: true };



    }
    function EgresoPacController($scope, $q, $timeout, ssDate, Service, Auth) {
        $scope.json = {};
        $scope.cod_user = Auth.getUserObj().codigo;
        $scope.auxuser = Auth.getUserObj().usuario1;

        $scope.profilenivel = angular.toJson({ modulo: 'ADMISIONES', ventana: '05', usuario: $scope.auxuser });
        $scope.disabledbtn = { buscar: false, imprimir: true, nuevo: false, editar: true, eliminar: true };
        $scope.showbtn = { buscar: true, imprimir: true, nuevo: true, editar: true, eliminar: true };


    }
    function AdmisionesController($scope, $q, $timeout, ssDate, Service, Auth) {
        $scope.json = {};
        $scope.cod_user = Auth.getUserObj().codigo;
        $scope.auxuser = Auth.getUserObj().usuario1;

        $scope.profilenivel = angular.toJson({ modulo: 'ADMISIONES', ventana: '03', usuario: $scope.auxuser });
        $scope.disabledbtn = { buscar: false, imprimir: true, nuevo: false, editar: true, eliminar: true };
        $scope.showbtn = { buscar: true, imprimir: true, nuevo: true, editar: true, eliminar: true };
    }
    function UnificacionDocumentoController($scope, $q, $timeout, ssDate, Service, Auth) {
        $scope.json = {};
        $scope.openModal = false;

        //Preload
        Service.getCrud({}, "/Salud.svc/PreloadUnificacion").then(function (d) {
            var t = JSON.parse(d.data)[0];

            $scope.tipo = t;
        });

        //Busqueda Rapida Paciente
        $scope.dataPac = {
            id: 'brp',
            component: [{ id: 'br-pac', type: '' }],
            table: 'paciente',
            column: [
                { name: 'RTRIM(LTRIM(codigo))', as: 'codigo', visible: true },
                { name: 'RTRIM(LTRIM(nombre))', as: 'nombre', visible: true },
                { name: 'RTRIM(LTRIM(snombre))', as: 'snombre', visible: true },
                { name: 'RTRIM(LTRIM(ppellido))', as: 'ppellido', visible: true },
                { name: 'RTRIM(LTRIM(sapellido))', as: 'sapellido', visible: true }

            ],
            where: {},
            groupby: false,
            orderby: [],
            title: 'Busqueda de Paciente',
            required_name: 'Codigo Viejo'

        };
        $scope.rsPaciente = function (d) {
            var j = JSON.parse(d);
            $scope.json.cod_viejo = j.codigo;
            $scope.json.nombreC = j.nombre + " " + j.snombre + " " + j.ppellido + " " + j.sapellido;
        };

        //Ventana Modal Mensajes
        $scope.closeModal = function () {
            $scope.openModal = false;
        }

        $scope.procesar = function () {
            if ($scope.json.cod_viejo != "") {
                if ($scope.json.tipo != "" && $scope.json.tipo != undefined) {
                    if ($scope.json.codigoN != "" && $scope.json.codigoN != undefined) {
                        Service.getCrud({ cod1: $scope.json.cod_viejo, cod2: $scope.json.codigoN, tipo: $scope.json.tipo }, "/Salud.svc/ProcesarDocumento").then(function (d) {
                            var j = JSON.parse(d.data);
                            console.log(j);
                            if (j > 0) {
                                $("#alertColor").css({ 'background-color': 'rgba(78,193,81,1)', 'color': 'white' });
                                $scope.mensaje = 'El documento se actualizo';
                                $scope.openModal = true;
                                $('#closePend').focus();
                            }
                            else {
                                $("#alertColor").css({ 'background-color': 'rgba(236,85,85,1)', 'color': 'white' });
                                $scope.mensaje = 'El se puso actualizar el documento';
                                $scope.openModal = true;
                                $('#closePend').focus();
                            }
                        });
                    } else {
                        $("#alertColor").css({ 'background-color': 'rgba(236,85,85,1)', 'color': 'white' });
                        $scope.mensaje = 'Debe digitar el nuevo numero del documento';
                        $scope.openModal = true;
                        $('#closePend').focus();
                    }
                } else {
                    $("#alertColor").css({ 'background-color': 'rgba(236,85,85,1)', 'color': 'white' });
                    $scope.mensaje = 'Debe Seleccionar el tipo de documento';
                    $scope.openModal = true;
                    $('#closePend').focus();
                }
            } else {
                $("#alertColor").css({ 'background-color': 'rgba(236,85,85,1)', 'color': 'white' });
                $scope.mensaje = 'Debe seleccionar el paciente para actualizar';
                $scope.openModal = true;
                $('#closePend').focus();
            }

        }
        $scope.cancelar = function () {
            $scope.json.cod_viejo = "";
            $scope.json.nombreC = "";
            $('#br-pac-valor').val('');
            $('#br-pac-clave').val('');
        }
        $scope.cerrar = function () {
            var currentButton = angular.element(document.getElementById('salud-system'));
            $timeout(function () {
                $('#salud-system').click();
                currentButton.triggerHandler("click");
            })
        }


    }
    function Cumplimiento_OrdenesController($scope, $q, $timeout, ssDate, Datos, Service, Auth, Paciente) {
        //console.log(Datos);
        $scope.json = {};
        $scope.json2 = {};
        $scope.json3 = {};
        $scope.datapanelpacienteprint = {};
        $scope.mostrar1 = true;
        $scope.mostrar4 = false;
        $scope.mostrar6 = true;
        $('#print_or').attr('disabled', true);
        $scope.json.tipo_consulta = 1;
        $scope.key2 = { value: '' };
        $scope.json.tipo = Datos.info.Parametro;
        $scope.ventana = Datos.info.Ventana;
        $scope.vent = true;
        //console.log(Datos.info.data);
        if($scope.ventana == 1){
            $scope.vent = false;
            $scope.btnorden = false;
            $scope.btnhist = true;
        }else{
            $scope.btnorden = true;
        }
        //console.log(Datos.info);
        $scope.nit = Datos.nit;
        if ($scope.json.tipo == undefined) {
            $scope.json.tipo = 1;
        }
        if ($scope.json.tipo == 2) {
            $scope.sh_imp = true;
        } else {
            $scope.sh_imp = false;
        }

        $scope.json.numero = 0;
        $scope.fecha1 = new Date();
        $scope.fecha2 = new Date();
        $scope.json.codpac = '';
        $scope.json.centro = '';
        $scope.json.ingreso = '';
        $scope.filtro = "1";
        $scope.auxuser = Auth.getUserObj().usuario1;
        $scope.validar_permiso = function () {
            Service.getCrud({ usuario: $scope.auxuser, modulo: 'PERMISOS ESPECIALES', ventana: '09' }, "/Generic.svc/ReadNivel").then(function (d) {
                var j = JSON.parse(d.data);
                //console.log(j);
                if (j == 1111) {
                    $("#prot").attr("disabled", false);
                } else {
                    $("#prot").attr("disabled", true);
                }
            });
        }
        $scope.validar_permiso();
        $scope.filtrar = function (f) {
            if (f == 1) {
                $scope.mostrar1 = true;
                $scope.mostrar2 = false;
                $scope.mostrar3 = false;
                $scope.mostrar4 = false;
                $scope.json.tipo_consulta = 1;
            } else if (f == 2) {
                $scope.mostrar2 = true;
                $scope.mostrar1 = false;
                $scope.mostrar3 = false;
                $scope.mostrar4 = false;
                $scope.json.tipo_consulta = 2;

            } else if (f == 3) {
                $scope.mostrar3 = true;
                $scope.mostrar2 = false;
                $scope.mostrar1 = false;
                $scope.mostrar4 = false;
                $scope.json.tipo_consulta = 3;
            } else if (f == 4) {
                $scope.mostrar4 = true;
                $scope.mostrar2 = false;
                $scope.mostrar1 = false;
                $scope.mostrar3 = false;
                $scope.json.tipo_consulta = 4;
            }
        }
        $scope.cerrarVentana = function () {
            var currentButton = angular.element(document.getElementById('menucerrar'));
            $timeout(function () {
                currentButton.triggerHandler("click");
            });
        }
        $scope.cerrarVentana();
        $scope.openWaiting = false;
        $scope.sh_ordenes = false;
        $scope.orden_show = function () {
            if ($scope.filtro == "1") { $scope.btnorden = true; $scope.btnhist = false; }
            if ($scope.filtro == "2") { $scope.btnorden = false; $scope.btnhist = true; }
        }
        $scope.key2 = { value: '' };
        $scope.$watch('filtro', function (d) {
            $scope.tabla = [];
        });

        Service.getCrud({}, "/Admision.svc/PreloadOrdenesMedicas").then(function (d) {
            var data = JSON.parse(d.data);
            $scope.centros = data[0];

        });
        Service.getCrud({}, "/Generic.svc/DatosEmpresa").then(function (d) {
            var rs = JSON.parse(d.data)[0];
            $scope.sis = rs;
            $scope.nit = $scope.sis.nit;

            Service.getCrud({ nit: $scope.sis.nit }, "/Generic.svc/imgLogo").then(function (d) {
                var rs = JSON.parse(d.data);
                $scope.logo = rs;
                if (rs != null && rs != undefined && rs != '') {
                    $scope.logo = ('data:image/jpeg;base64,' + rs);
                }
            });

        });
        $scope.consultar = function () {
            var rs = [];
            $scope.openWaiting = true;
            $scope.json.fecha1 = $scope.fecha1;
            $scope.json.fecha2 = $scope.fecha2;
            $scope.json.fecha11 = ssDate.dateToString($scope.fecha1, "yyyy/MM/dd");
            $scope.json.fecha22 = ssDate.dateToString($scope.fecha2, "yyyy/MM/dd");
            if ($scope.json.tipo == undefined) {
                $scope.json.tipo = 1;
            }
            if ($scope.json.ingreso.length <= 1) {
                $scope.json.ingreso = 0
            } else {
                $scope.json.ingreso = $scope.json.ingreso;
            }
            if($scope.ventana == 1){
                Service.getCrud({ json: angular.toJson($scope.json)}, "/Admision.svc/ReadReimprimir").then(function (d) {
                    rs = JSON.parse(d.data);
                    $scope.openWaiting = false;
                    if (rs != false) {
                        $scope.tabla = rs;
                        $scope.tabla.forEach(function (item, index) {
                            item.estado = "Realizado";
                        });
                    }
                    if (rs == false || rs.length == 0) {
                        alert("LA CONSULTA NO GENERA SALIDA");
                    }
                });
            }else{
                Service.getCrud({ json: angular.toJson($scope.json), filtro: $scope.filtro }, "/Admision.svc/ReadOrdenesMedicas").then(function (d) {
                    rs = JSON.parse(d.data);
                    $scope.openWaiting = false;
                    if (rs != false) {
                        $scope.tabla = rs;
                    }
                    if (rs == false || rs.length == 0) {
                        alert("LA CONSULTA NO GENERA SALIDA");
                    }
                    if ($scope.filtro == "1") {
                        $scope.tabla.forEach(function (item, index) {                            
                            if (item.estado == undefined) {
                                if (item.usuario_reporte != null) {
                                    if (item.usuario_reporte.trim() != '') {
                                        item.estado = "Realizado";
                                    } else if (item.usuario_reporte.trim() == '') {
                                        item.estado = "Tomado";
                                        if (item.estado2 > 0) {
                                            //item.estado = "Anexado";
                                        }
                                    }
                                } else {
                                    item.estado = "Tomado";
                                }
                            }
                            
                        });
                    } else {
                        $scope.tabla.forEach(function (item, index) {
                            if (item.estado == undefined) {
                                if (item.cod_med_rep != null) {
                                    if (item.cod_med_rep.trim() != '') {
                                        item.estado = "Realizado";
                                    } else if (item.cod_med_rep.trim() == '') {
                                        item.estado = "Tomado";
                                        if (item.estado2 > 0) {
                                            //item.estado = "Anexado";
                                        }
                                    }
                                } else {
                                    item.estado = "Tomado";
                                }
                            }
                        });
                    }
                });
            }

            
        }

        if (Datos.info.data != undefined && Datos.info.data != null) {
            if (Datos.info.data.ventana == 'CEN-HOSP1') {
                $scope.json.tipo_consulta = 3;
                $scope.json.tipo = Datos.info.data.tipo;
                $scope.json.codigo = Datos.info.data.codigopac;
                $scope.json.codpac = Datos.info.data.codigopac;
                $scope.key2.value = Datos.info.data.codigopac;
                $scope.mostrar1 = false;
                $scope.mostrar2 = false;
                $scope.mostrar3 = false;
                $scope.mostrar4 = false;
                $scope.mostrar5 = true;
                $scope.mostrar6 = false;
                $scope.vent = false;
                $scope.ventana == 2;
                $('#prot').hide();
                $('#print_or').hide();
                $('#brp-btn').hide();
                $('#fact').hide();
                $('#ccosto').hide();
                $('#fil').hide();
                $scope.consultar();
            }
        }
        $scope.r = "";
        $scope.data = sessionStorage.getItem('key');
        $scope.$watch('data', function (d) {
        })

        $scope.convert = function (fecha) {
            return ssDate.militodateString(fecha, 'dd-MM-yyyy');
        }
        $scope.showviewModalView = { state: false };
        $scope.dataModalView = {
            url: ''
        }
        $scope.open_orden = function (item, index) {
            //console.log(item);
            if (item.estado != "Tomado" && item.estado != "Realizado") {
                $scope.showviewModalView.state = undefined;
                Datos.info.ventana = 'C_ordenes';
                Datos.info.cod_paciente = item.codigo.trim();
                Datos.info.cod_admin = item.cod_admin;
                Datos.info.medico = item.medico.trim();
                Datos.info.item = item;
                Datos.info.admin = item.admin.trim();
                Datos.info.tipo = $scope.json.tipo;
                Datos.info.numero = item.numero;
                Datos.info.filtro = $scope.filtro;
                if(Datos.info.tipo == '3'){
                    Datos.info.inp = true;
                } else {
                    if (Datos.info.tipo == '1') {
                        Datos.info.inp = true;
                    } else {
                        Datos.info.inp = false;
                    }
                    
                }
                $timeout(function () {
                    $scope.dataModalView.url = 'js/app/Modulo/Salud/Admisiones/FacturacionOrdenesMedicas.html';
                    $scope.showviewModalView.state = true;

                });
                $scope.showviewModalView.state = undefined;
                $scope.dataModalView.url = "";
            } else {
                alert("LA ORDEN YA HA SIDO TOMADA O REALIZADA");
            }
        }

        $scope.estado = function (t) {
          //  console.log(t);
            var aux;
            if (t.estado != undefined) {
                if (t.estado.trim() == '') {
                    aux = "TsinNivel";

                }
                else {
                    if (t.estado.trim() == 'Cumplida') {
                        aux = "Cumplida";
                    }                      
                     if (t.estado.trim() == 'Tomado') {
                        aux = "Tomado";                        
                     }
                    if (t.estado2 > 0) {
                        aux = "Anexado";
                    } 
                    else if (t.estado.trim() == 'Realizado') {
                        aux = "Realizado";
                    }
                                      
                }

            }
            if (t.cod_med_rep != undefined) {
                if (t.cod_med_rep.trim() == '') {
                    aux = "Tomado";
                    if (t.estado2 > 0) {
                        aux = "Anexado";
                    }
                } else {
                    if (t.cod_med_rep.trim() != '') {
                        aux = "Realizado";
                    }
                }
            }
            return aux;
        };

        $scope.print_ord = function () {
            if ($scope.json3 != undefined) {
                if (!jQuery.isEmptyObject($scope.json3)) {
                    console.log($scope.json3);
                    $scope.Mymodalrep = true;
                    $timeout(function (d) {
                        $scope.cargarreport22();
                        $('#myModal22').show();
                    }, 500)
                }
            }
        }

        $scope.seleted = function (item, index) {
            //3console.log(item);
            $scope.json3 = {};
            var estselect;
            Datos.info = {};
            $('#tabla-' + $scope.outseleted).removeClass("seleccion");

            $('#tabla-' + index).addClass("seleccion");

            $scope.outseleted = index;
            Datos.info.item = item;
            $scope.json3 = item;
            $('#print_or').attr('disabled', false);
            //console.log(item);
        }
        $scope.outseleted = '-1';


        $scope.und_atencion = function (item) {
            if (item == 3) {
                return "CONSULTA EXTERNA"
            } else if (item == 2) {
                return "URGENCIA"
            } else if (item == 1) {
                return "HOSPITALIZACION"
            }
        }
        $scope.cargarreport22 = function () {
            $scope.urlreport22 = undefined;
            document.getElementById('reporte22').src = undefined;
            $scope.urlreport22 = Service.getReport('/TomaOrden?title=Orden de Servicio ' + $scope.und_atencion($scope.json3.tipo_histo) + '&historia=' + $scope.json3.historia + '&nit=' + Datos.nit + '&numero=' + $scope.json3.numero);
            document.getElementById('reporte22').src = $scope.urlreport22;
        }
        $scope.closed22 = function () {
            $scope.Mymodalrep = false;
            $('#myModal22').hide();
        }
        $scope.open_rs = function () {
            if (Datos.info.item.numero != undefined && Datos.info.item.numero != null) {
                if (Datos.info.item.estado == "Tomado" || Datos.info.item.estado == undefined || Datos.info.item.estado == "Realizado") {
                    $scope.showviewModalView.state = undefined;
                    Datos.info.tipo = $scope.json.tipo;
                    Datos.info.filtro = $scope.filtro;
                    $timeout(function () {
                        if ($scope.json.tipo == 1) {
                            if($scope.ventana == 1){
                                $scope.dataModalView.url = 'js/app/Modulo/Laboratorio Clinico/Reimprimir.html';
                            }else{
                                $scope.dataModalView.url = 'js/app/Modulo/Laboratorio Clinico/Transcipcion.html';
                            }
                        } else if ($scope.json.tipo == 2) {
                            $scope.dataModalView.url = 'js/app/Modulo/Salud/Admisiones/ResultadoOrdenesDiagnosticas.html';
                        } else if ($scope.json.tipo == 3) {
                            if(Datos.info.item.tipo_histo == 3){
                                Paciente.historia = Datos.info.item.historia;
                                Paciente.codigo = Datos.info.item.codigo;
                                Paciente.cod_admin = Datos.info.item.cod_admin;
                                Paciente.sexo = Datos.info.item.paciente;
                                Paciente.tipo_histo = Datos.info.item.tipo_histo;
                                console.log(Datos.info.item);
                                $scope.dataModalView.url = 'js/app/Modulo/Salud/Admisiones/OrdenesFCE.html';
                            }else{
                                $scope.dataModalView.url = 'js/app/Modulo/Laboratorio Clinico/Orden_Fisioterapia.html';
                            }
                        } else if ($scope.json.tipo == 4) {
                            $scope.dataModalView.url = 'js/app/Modulo/Laboratorio Clinico/resultadocitologia.html';
                        } else if ($scope.json.tipo == 6) {
                            $scope.dataModalView.url = 'js/app/Modulo/Salud/Historias/Histo_Espermograma.html';
                        } else if ($scope.json.tipo == 7) {
                            $scope.dataModalView.url = 'js/app/Modulo/Laboratorio Clinico/Reimprimir.html';
                        }

                        $scope.showviewModalView.state = true;
                    });
                    $scope.showviewModalView.state = undefined;
                    $scope.dataModalView.url = "";
                } else {
                    alert("LA ORDEN NO HA SIDO TOMADA")
                }
            } else {
                alert("DEBE SELECCIONAR UN REGISTRO");
            }
        }
        $scope.imprimir = function (item) {
            $scope.sh_ordenes = true;
            $scope.datapanelpacienteprint.codigo = (item.codigo).trim();
            $scope.datapanelpacienteprint.cod_admin = (item.cod_admin).trim();
            $scope.json3 = {};
            Service.getCrud({ filtro: $scope.filtro, tipo: $scope.json.tipo, numero: item.numero, paciente: item.codigo, id: item.historia }, "/Admision.svc/BuscarOrdenes").then(function (d) {
                var rs = JSON.parse(d.data);

                rs.forEach(function (it, index) {
                    if (item.cod_proc.trim() == it.cod_proc.trim() && item.tarifa.trim() == it.tarifa.trim()) {
                        if (it.cod_pac == undefined) {
                            it.cod_pac = it.codigo;
                            $scope.json3.idweb = it.idweb;
                            $scope.json3.tipo = Datos.info.Parametro;
                            $scope.json3.tipo_histo = it.tipo_histo;
                            $scope.json3.orden = it.numero;
                            $scope.json3.cod_proc = it.cod_proc;
                            $scope.readresult();
                        }
                    }
                })



            })
        }

        $scope.cargarreport3 = function (x) {
            $scope.urlreport = undefined;
            document.getElementById('reporte2').src = undefined;
            if(x.tipo_histo == 1){
                $scope.urlreport = Service.getReport('/ApoyoDiagHosp?historia='+x.historia+'&cod_proc='+x.cod_proc.trim()+'&nit='+$scope.nit+'&numero='+x.numero2);
                document.getElementById('reporte2').src = $scope.urlreport;
            }else if(x.tipo_histo == 2){
                $scope.urlreport = Service.getReport('/ApoyoDiagUrg?historia='+x.historia+'&cod_proc='+x.cod_proc.trim()+'&nit='+$scope.nit+'&numero='+x.numero2);
                 document.getElementById('reporte2').src = $scope.urlreport;
            } else {
                console.log(x);
                $scope.urlreport = Service.getReport('/ApoyoDiagConsExt?historia='+x.historia+'&cod_proc='+x.cod_proc.trim()+'&nit='+$scope.nit+'&numero='+x.numero2);
                document.getElementById('reporte2').src = $scope.urlreport;
            } 
        }
        $scope.imprimir3 = function (x) {
            $scope.Mymodalrepo = true;
            console.log("ENTRO IMPRIMIR!!!");
            $timeout(function(d){
                $scope.cargarreport3(x);
                $('#myModalito').show();
            },500)
        }
        $scope.closed2 = function () {
            $scope.Mymodalrepo = false;
            $('#myModalito').hide();
        }

        $scope.cerrar = function () {
            if (Datos.info.data.ventana == 'CEN-HOSP1') {
                $timeout(function () {
                    var currentButton = angular.element(document.getElementById('cerrar'));
                    $timeout(function () {
                        $('#cerrar').click();
                        currentButton.triggerHandler("cerrar");
                    });
                    Datos.info = {};
                });
            } else {
                $timeout(function () {
                    var currentButton = angular.element(document.getElementById('salud-system'));
                    $timeout(function () {
                        $('#salud-system').click();
                        currentButton.triggerHandler("salud-system");
                    });
                    Datos.info = {};
                });
            }
        }

        /*BUSQUEDA RAPIDA PACIENTE*/
        $scope.dataPaciente = {
            id: 'brPaciente',
            component: [{ id: 'brp', type: '' }],
            table: 'paciente',
            column: [
                { name: 'CODIGO', as: "'No. Documento'", visible: true },
                { name: 'PPELLIDO', as: "'1er Apellido'", visible: true },
                { name: 'SAPELLIDO', as: "'2do Apellido'", visible: true },
                { name: 'NOMBRE', as: "'1er Nombre'", visible: true },
                { name: 'SNOMBRE', as: "'2do Nombre'", visible: true },
                { name: 'nivel_afil', as: "'Nivel'", visible: false },
                { name: 'telefono', as: "'Telefono'", visible: false },
            ],
            where: {},
            groupby: false,
            orderby: ['CODIGO desc', 'PPELLIDO desc'],
            title: 'Buscar pacientes',
            required: false
        };
        $scope.rsPaciente = function (rs) {
            $scope.openModal = false;
            var j = JSON.parse(rs);
            if (!jQuery.isEmptyObject(j)) {
                //$scope.json.codigo = j["No. Documento"];
                $scope.json.codpac = j["No. Documento"];
                $scope.nombrepaciente = j["1er Nombre"] + " " + j["2do Nombre"] + " " + j["1er Apellido"] + " " + j["2do Apellido"];
                //$scope.nivel = j["Nivel"];
                //$scope.tel = j["Telefono"];
                //$scope.json.centro_costo = 'CE';
                //$scope.causa_ext = '13- ENFERMEDAD GENERAL';
            }

        }


        $scope.validarfirma = function () {
            $scope.modelo = {}
            Service.getCrud({ codigo: $scope.json2.medico, cod_pac: '' }, '/Salud.svc/firma_medico1').then(function (d) {
                var rs = JSON.parse(d.data);
                $scope.model = rs[0];
                $scope.especi = rs[1];
                $scope.model1 = rs[2];
                $scope.nombre_medico = $scope.especi[0].nombre;
                $scope.registro_med = $scope.especi[0].registro_medico;
                $scope.espe = $scope.especi[1].nombre;
                $scope.upload = $scope.model.rutafirma;
                if ($scope.upload != null && $scope.upload.trim() != '') {
                    $('#labelfirma').css("margin-top", "100px");
                }
                else {
                    $('#labelfirma').css("margin-top", "30px");
                }
                if ($scope.model.rutafirma != null && $scope.model.rutafirma != undefined && $scope.model.rutafirma != '') {
                    $scope.upload = ('data:image/jpeg;base64,' + $scope.model.rutafirma);
                }
                $scope.openWaiting = false;
                $timeout(function () {
                    $('#menu').addClass("ss-nav-inactive");
                    $('#wrapper').addClass("ss-full-view");
                    window.print();
                    $scope.sh_ordenes = false;
                    $('#menucerrar').click();
                }, 3000);
            });
        }

        $scope.readresult = function () {
            $scope.json2 = {};
            $scope.openWaiting = true;
            $scope.upload = undefined;
            $scope.nombre_medico = undefined;
            $scope.espe = undefined;
            $scope.registro_med = undefined;
            Service.getCrud({ json: angular.toJson($scope.json3) }, "/Admision.svc/ReadrsProtocolo").then(function (d) {
                var rs = JSON.parse(d.data);
                $scope.json2 = rs[0];
                $scope.json2.auxanio = rs[0].año;
                if (rs[0].reporte != undefined) {
                    $scope.json.resultado = rs[0].reporte;
                    $scope.json2.reporte = rs[0].reporte;
                }
                if (rs[0].resultado != undefined) {
                    $scope.json.resultado = rs[0].resultado;
                    $scope.json2.reporte = rs[0].resultado;
                }
                if (rs[0] != false) {
                    $scope.json2.fecha = ssDate.militodateString(rs[0].fecha);

                    if (rs[0].cod_med_rep != undefined) {
                        $scope.json2.medico = rs[0].cod_med_rep.trim();
                    }
                    if (rs[0].cod_med_reporte != undefined) {
                        $scope.json2.medico = rs[0].cod_med_reporte.trim();
                    }
                    $scope.json2.nom_c_costo = rs[1];
                    if ($scope.json2.medico != undefined && $scope.json2.medico != null) {
                        if ($scope.json2.medico.trim() != '') {
                            $scope.validarfirma();
                        } else {
                            $scope.openWaiting = false;
                            //alert("ESTE PROCEDIMIENTO NO TIENE NINGUN RESULTADO TRANSCRITO INTETELO NUEVAMENTE");
                        }
                    }

                }
            });
        }

        $scope.val_disa = function (est) {
            if (est.trim() == "Realizado") {
                return false;
            } else {
                return true;
            }
        }

        $scope.und_atencion = function (item) {
            if (item == 3) {
                return "CONSULTA EXTERNA"
            } else if (item == 2) {
                return "URGENCIA"
            } else if (item == 1) {
                return "HOSPITALIZACION"
            }
        }
        $scope.exit = function () {
            Service.getCrud({codigo: Datos.info.item.codigo, 
                            historia: Datos.info.item.historia, 
                            cod_proc: Datos.info.item.cod_proc,
                            numero: Datos.info.item.numero2,
                            id_web: Datos.info.item.idweb,
                            filtro: $scope.filtro,
                            codigo_usuario: Auth.getUserObj().codigo}, "/Salud2.svc/RealizadoOrd").then(function (d){
                            var rs = JSON.parse(d.data);
                            if(rs == true){
                                $scope.consultar();
                            }
            });  
            
        }
    }
    function F_OrdenesmedicasController($scope, $q, $timeout, ssDate, Datos, Service, Auth, Consultas) {
        console.log("sw: "+Datos.info.inp);
        $scope.swt = Datos.info.inp;
        $scope.codigo = '';
        $scope.codigoesp = '';
        $scope.pagar = '';
        $scope.vlr_t = false;
        $scope.nit = Datos.nit;
        $scope.nivel = '';
        $scope.detalle = '';
        $scope.tel = '';
        $scope.fecha = '';
        $('#procesar').prop('disabled', false);
        $scope.shaut;
        $scope.requ;
        $scope.grilla = [];
        $scope.grilla_servicio = [];
        $scope.anos;
        $scope.fecha = new Date();
        $scope.servi = '';
        $scope.openWaiting = false;
        $scope.emp = '';
        $scope.tarifaa = '';
        $scope.tarifa = '';
        $scope.empres = '';
        $scope.autorizacion = '';
        $scope.admin = '';
        $scope.citas = '';
        $scope.cambioemp = true;
        $scope.causa_ext = '13- ENFERMEDAD GENERAL                            ';
        $scope.cantidad = '1';
        $scope.emp_new = '';
        $scope.emp_old = '';
        $scope.openModal = false;
        $scope.openModalDos = false;
        $scope.openModalPago = false;
        $scope.citas_ant = '';
        $scope.cod_user = Auth.getUserObj().codigo;
        $scope.auxuser = Auth.getUserObj().usuario1;
        $scope.json = {};
        $scope.key = { value: '' };
        $scope.key2 = { value: '' };
        $scope.json3 = {};
        $scope.json3.prestador = "";
        $scope.abrirventana = function () {
            var currentButton = angular.element(document.getElementById('menucerrar'));
            $timeout(function () {
                currentButton.triggerHandler("click");
            });
        }
        $scope.abrirventana();
        $scope.buscaprest_serv = false;

        Service.getCrud({}, "/Salud.svc/PreloadSuministros").then(function (d) {
            $scope.empresa = JSON.parse(d.data)[2];
            $scope.medicos = JSON.parse(d.data)[0];

        });

        $timeout(function () {
            if (Datos.info.ventana == 'C_ordenes') {
                console.log(Datos.info);
                $scope.key2.value = Datos.info.cod_paciente;
                $scope.admin = Datos.info.admin.trim();
                $scope.json.medico = Datos.info.medico.trim();
                $scope.json.nro_aut = Datos.info.numero;
                $scope.json.codigo_med = Datos.info.medico.trim();
                $scope.json.centro_costo = Datos.info.item.c_costo;
                //$scope.findcentro(Datos.info.item.c_costo);
                //$scope.findcentro_piso(Datos.info.item.c_costo);
                $('#brp-clave').prop("disabled", true);
                $('#brp-valor').prop("disabled", true);
                $('#brPaciente-clave').prop('disabled', true);
                $('#brPaciente-valor').prop('disabled', true);
                $('#brPaciente-btn').prop('disabled', true);
                $scope.getadmin();
                Service.getCrud({ filtro: Datos.info.filtro, tipo: Datos.info.tipo, numero: Datos.info.numero, paciente: Datos.info.cod_paciente }, "/Admision.svc/BuscarOrdenes").then(function (d) {
                    var rs = JSON.parse(d.data);
                    rs.forEach(function (item, index) {
                        $scope.json.ano = item.año;
                        item.check = 0;
                        $scope.meta = 1;
                        if (item.tipoestado.trim() == "") {
                            item.admin = item.admin.trim();
                            item.cod_proc = item.cod_proc.trim();
                            item.tarifa = item.tarifa.trim();
                            if (item.grupo == null) {
                                item.grupo = "";
                            }
                        /**/
                            var query4 = "select top 1 valor as nivel from tarifas_variables_servicios where codigo ='" + item.cod_proc + "' and cod_admin in (select top 1 codigo from admin where nombre ='" + item.admin + "')";
                            Consultas.read(query4).then(function (d) {
                                if (d != null) {
                                    var w = d[0].nivel;
                                    if (w != null) {
                                        item.valor = parseInt(w);
                                        
                                    }
                                }                                
                            });
                           // console.log(item);
                            item.cntdad = 1;
                            item.total = item.valor;
                            $scope.grilla.push(item);
                        }
                    })
                });
            }
        }, 500)


        Service.getCrud({ usuario: $scope.auxuser }, "/Salud.svc/ValidarCuadre").then(function (d) {
            var v = JSON.parse(d.data);
            if (v == true) {
                $('#procesar').prop('disabled', false);
            } else {
                $('#procesar').prop('disabled', false);
            }
        });
        Service.getCrud({ usuario: $scope.auxuser, modulo: 'Agenda Medica', ventana: '11' }, "/Generic.svc/ReadNivel").then(function (d) {
            var j = JSON.parse(d.data);
            var c, r, u, de;
            c = j.substr(0, 1);
            r = j.substr(1, 1);
            u = j.substr(2, 1);
            de = j.substr(3, 1);

            if (c == 1) {
                $('#procesar').attr("disabled", false);
                $('#cancelar').attr("disabled", false);
                $('#cambiar').attr("disabled", false);
                $('#procesar').prop('disabled', false);

            } else {
                $('#procesar').attr("disabled", false);
                $('#procesar').prop('disabled', false);
                $('#cancelar').attr("disabled", true);
                $('#cambiar').attr("disabled", true);
            }
        });
        /*-------------------------------BUSQUEDA-----------------------------------------*/
        /*BUSQUEDA RAPIDA PACIENTE*/
        $scope.dataBuscador_Paciente = {
            id: 'buscadorPaciente',
            component: [{ id: 'brPaciente', type: '' }],
            table: 'paciente',
            column: [
                { name: 'CODIGO', as: 'Documento', visible: true },
                { name: 'NOMBRE', as: 'Nombre', visible: true },
                { name: 'snombre', as: 'Nombre2', visible: true },
                { name: 'ppellido', as: 'Apellido', visible: true },
                { name: 'sapellido', as: 'Apellido2', visible: true },
                { name: 'nivel_afil', as: 'Nivel', visible: false },
                { name: 'sexo', as: 'Sexo', visible: false },
                { name: 'tipo_c', as: 'tipoc', visible: true },
                { name: 'telefono', as: 'Telefono', visible: true },
            ],
            where: [],
            groupby: false,
            orderby: [],
            title: 'Buscar Paciente',
            required_name: 'PACIENTE'
        };
        $scope.rsPaciente = function (rs) {
            var j = JSON.parse(rs);
            if (!jQuery.isEmptyObject(j)) {
                $scope.json.codigo = j.Documento;
                $scope.nivel = j.Nivel;
                $scope.tel = j.Telefono;
                $scope.sexo = j.Sexo;
                $scope.nombre = j.Apellido + ' ' + j.Apellido2 + ' ' + j.Nombre + ' ' + j.Nombre2;
            }

        }
        /*BUSQUEDA RAPIDA SERVICIOS*/
        $scope.add_proce = function () {
            var paquete = 0;
            console.log($scope.json);
            console.log($scope.poc);
            Service.getCrud({ codigo: $scope.json.cod_proc, cod_admin: $scope.admin }, "/Salud.svc/ValidarPaquete").then(function (d) {
                var resultado = JSON.parse(d.data);
                if (resultado == false) {
                    if ($scope.json.cod_proc != null && $scope.json.cod_proc != undefined && $scope.json.cod_proc != "") {
                        $scope.grilla_servicio.push({ codigo: $scope.json.cod_proc, nombreserv: $scope.nombreserv, grupo: $scope.json.grupo, valor: $scope.json.total });
                        $('#brs-clave').val('');
                        $('#brs-valor').val('');
                        $scope.json.cod_proc = "";
                    }
                } else {
                    resultado.forEach(function (item, index) {
                        paquete += item.valor;
                        $scope.tarifa = item.tarifa;
                        $scope.json.grupo = item.grupo_fact;
                        $scope.grilla_servicio.push({ codigo: item.codigo, nombreserv: item.nombre, grupo: item.grupo_fact, valor: paquete });
                        $('#brs-clave').val('');
                        $('#brs-valor').val('');
                        $scope.json.total = paquete;
                    });
                    $scope.calc();
                }
            });

        }
        $scope.dataServicios = {
            id: 'brServicios',
            component: [{ id: 'brs', type: '' }],
            table: 'procedimientos AS P',
            column: [
                { name: 'p.CODIGO', as: "Codigo", visible: true },
                { name: 'p.NOMBRE', as: "Nombre", visible: true },
                { name: 'p.TARIFA', as: "Tarifa", visible: true },
                { name: 'p.grupo_fact', as: "Grupo", visible: false }
            ],
            where: {},
            inner_join: [],
            left_join: [],
            groupby: true,
            orderby: [],
            exist: [{ table: 'paquetes', campo: 'codigo,nombre', where1: 'codigo', where2: 'nombre', where3: 'codigo', where4: 'codigo', where5: 'codigo' }],
            title: 'Buscar Servicios'
        };
        $scope.rsServicios = function (rs) {
            var j = JSON.parse(rs);
            if (!jQuery.isEmptyObject(j)) {
                $scope.codigoesp = j["Codigo"];
                $scope.nombreserv = j["Nombre"];
                $scope.tarifa = j["Tarifa"];
                $scope.t = j["Tarifa"];
                $scope.json.cod_proc = $scope.codigoesp;
                $scope.json.grupo = j["Grupo"];
                /*VALOR DE PROCEDIMIENTO*/
                $scope.$watch('json.cod_proc', function (d) {
                    if (d != undefined) {
                        if (d != null) {
                            if (d != '') {
                                $scope.calc();
                            }
                        }
                    }
                });
            }
        }
        $scope.recalcular = function (item) {
            if (item != undefined) {
                item.total = (item.valor * item.cntdad);
            }
        }
        function getCitas() {

            Service.getCrud({ codigo: $scope.json.codigo }, "/Salud.svc/CitasAsignadas").then(function (d) {
                $scope.citas = JSON.parse(d.data);
            });
        }
        $scope.deleteGrilla = function (i) {
            //$scope.grilla.push(grilla[i]);
            $scope.grilla_servicio.splice(i, 1);
        }
        /*BARRA*/
        $scope.disabledbtn = { buscar: true, imprimir: true, nuevo: false, editar: true, eliminar: true };
        $scope.showbtn = { buscar: false, imprimir: false, nuevo: true, editar: false, eliminar: false };

        /*--------------------------CRUDS-------------------------------------------------------------*/


        /*----------ACTUALIZAR-----------------------*/
        $scope.aceptar = function () {
            $scope.openModalPago = false;
            $scope.openModal = true;
        }

        $scope.calcular = function () {
            $scope.cambio = parseInt($scope.pagado) - parseInt($scope.json.total);
        }

        $scope.actualizar = function () {
            if ($scope.json.medico == null) {
                $scope.json.medico = '';
            }
            if ($scope.json.medico == undefined) {
                $scope.json.medico = '';
            }

            var defender = $q.defer();
            var promise = defender.promise;
            if ($scope.vlr_t == true) {
                alert("EL PROCEDIMIENTO: " + $scope.nom_t + " NO TIENE NO ES VALIDO CON LA TARIFA DE LA ADMINISTRADORA Y NO TIENE HOMOLOGO ACTIVO");
                return promise;
            } else {

                if (Datos.info.item.tipo_histo == 2) {
                    $scope.medicos.forEach(function (item, index) {
                        if (item.nombre.trim() == Datos.info.medico.trim()) {
                            $scope.json.codigo_med = item.codigo.trim();
                        }
                    });
                }
                if (Datos.info.item.tipo_histo == 1) {
                    $scope.medicos.forEach(function (item, index) {
                        if (item.nombre.trim() == Datos.info.medico.trim()) {
                            $scope.json.codigo_med = item.codigo.trim();
                        }
                    });
                }

                $scope.json.cod_admin = $scope.adminn;
                if ($scope.discapacitado == undefined) {
                    $scope.discapacitado = 0;
                }
                if ($scope.json.centro_costo == '' || $scope.json.centro_costo == undefined || $scope.json.centro_costo == null) { $scope.json.centro_costo = '0' };
                $scope.json.causa_ext = $scope.causa_ext;
                //$scope.json.ano = '2018';
                if (!$scope.formRecaudo.$valid) {
                    var cr = "Campos requeridos no diligenciados:";
                    $scope.formRecaudo.$error.required.forEach(function (item, index) {
                        cr += "\n - " + item.$name;
                    });
                    alert(cr);
                    defender.resolve(false);
                    return promise;
                }
                else {
                    if (Datos.info.item.tipo_histo == 2) {
                        $scope.fuente_fact(Datos.info.item.historia);
                        if ($scope.piso > 0) {
                            $timeout(function () {
                                try {
                                    $scope.json.recibo = parseInt($scope.json.recibo);
                                    $scope.empresa.forEach(function (item, index) {
                                        if (item.codigo.trim() == $scope.adminn) {
                                            $scope.json.nit = item.nit;
                                        }
                                    })
                                    var rs = false;
                                    if ($scope.json.nro_aut != "" || $scope.json.nro_aut != null || $scope.json.nro_aut != undefined) {
                                        $scope.json.nro_aut = $scope.json.nro_aut + "";
                                    }

                                    if ($scope.json.cod_programapyp == null || $scope.json.cod_programapyp == undefined) {
                                        $scope.json.cod_programapyp = "";
                                    }
                                    if ($scope.json.nro_aut == null || $scope.json.nro_aut == undefined) {
                                        $scope.json.nro_aut = "";
                                    }
                                    if (jQuery.isEmptyObject($scope.grillaforma_pago)) {
                                        $scope.grillaforma_pago = [];
                                    }
                                    Service.getCrud({ head: angular.toJson($scope.json), listproc: angular.toJson($scope.grilla), codigo_usuario: $scope.cod_user, historia: Datos.info.item.historia }, "/Admision.svc/UpdateCitaCumplida_hosp").then(function (d) {
                                        rs = JSON.parse(d.data);
                                        if (rs == true) {
                                            var f = $('#formRecaudo');
                                            $(f).find('input[type=text],textarea,select,input[type=number],input[type=time],input[type=checkbox]').val('');
                                            $(f).find('input[type=checkbox]').attr('checked', false);
                                            $('#brp-clave').prop("disabled", false);
                                            $('#brp-valor').prop("disabled", false);
                                            $('#brPaciente-clave').prop('disabled', false);
                                            $('#brPaciente-valor').prop('disabled', false);
                                            $('#brPaciente-btn').prop('disabled', false);
                                            $scope.grilla = [];
                                            //Datos.info = {};
                                            $scope.json.codigo = '';
                                            if ($scope.json.total > 0) {
                                                $scope.openModalPago = true;
                                            }
                                            else {
                                                $scope.openModal = true;/*
                                                var currentButton = angular.element(document.getElementById('cerrar'));
                                                var currbtn = angular.element(document.getElementById('btncolsultar'));
                                                $timeout(function () {
                                                    currentButton.triggerHandler("click");
                                                    $('#cerrar').click();
                                                    currbtn.triggerHandler("click");
                                                    $('#btncolsultar').click();
                                                });*/
                                                $scope.imprimir2();
                                            }
                                            $scope.openWaiting = false;

                                        } else {
                                            alert("Los Datos insertados Son Incorrectos");
                                            $scope.openWaiting = false;
                                        }

                                    });
                                    defender.resolve(true);
                                } catch (e) {
                                    defender.reject(e);
                                }
                            });
                        } else {
                            $timeout(function () {
                                try {
                                    $scope.json.recibo = parseInt($scope.json.recibo);
                                    $scope.empresa.forEach(function (item, index) {
                                        if (item.codigo.trim() == $scope.adminn) {
                                            $scope.json.nit = item.nit;
                                        }
                                    })
                                    var rs = false;
                                    if ($scope.json.nro_aut != "" || $scope.json.nro_aut != null || $scope.json.nro_aut != undefined) {
                                        $scope.json.nro_aut = $scope.json.nro_aut + "";
                                    }

                                    if ($scope.json.cod_programapyp == null || $scope.json.cod_programapyp == undefined) {
                                        $scope.json.cod_programapyp = "";
                                    }
                                    if ($scope.json.nro_aut == null || $scope.json.nro_aut == undefined) {
                                        $scope.json.nro_aut = "";
                                    }

                                    Service.getCrud({ head: angular.toJson($scope.json), listproc: angular.toJson($scope.grilla), codigo_usuario: $scope.cod_user, historia: Datos.info.item.historia }, "/Admision.svc/UpdateCitaCumplida_Urg").then(function (d) {
                                        rs = JSON.parse(d.data);
                                        if (rs == true) {
                                            var f = $('#formRecaudo');
                                            $(f).find('input[type=text],textarea,select,input[type=number],input[type=time],input[type=checkbox]').val('');
                                            $(f).find('input[type=checkbox]').attr('checked', false);
                                            $('#brp-clave').prop("disabled", false);
                                            $('#brp-valor').prop("disabled", false);
                                            $('#brPaciente-clave').prop('disabled', false);
                                            $('#brPaciente-valor').prop('disabled', false);
                                            $('#brPaciente-btn').prop('disabled', false);
                                            $scope.grilla = [];
                                            //Datos.info = {};
                                            $scope.json.codigo = '';
                                            if ($scope.json.total > 0) {
                                                $scope.openModalPago = true;
                                            }
                                            else {
                                                $scope.openModal = true;/*
                                                var currentButton = angular.element(document.getElementById('cerrar'));
                                                var currbtn = angular.element(document.getElementById('btncolsultar'));
                                                $timeout(function () {
                                                    currentButton.triggerHandler("click");
                                                    $('#cerrar').click();
                                                    currbtn.triggerHandler("click");
                                                    $('#btncolsultar').click();
                                                });*/
                                                $scope.imprimir2();
                                            }
                                            $scope.openWaiting = false;

                                        } else {
                                            alert("Los Datos insertados Son Incorrectos");
                                            $scope.openWaiting = false;
                                        }

                                    });
                                    defender.resolve(true);
                                } catch (e) {
                                    defender.reject(e);
                                }
                            });
                        }

                    } else if (Datos.info.item.tipo_histo == 3) {
                        $timeout(function () {
                            try {
                                $scope.json.recibo = parseInt($scope.json.recibo);
                                $scope.empresa.forEach(function (item, index) {
                                    if (item.codigo.trim() == $scope.adminn) {
                                        $scope.json.nit = item.nit;
                                    }
                                })
                                var rs = false;
                                if ($scope.json.nro_aut != "" || $scope.json.nro_aut != null || $scope.json.nro_aut != undefined) {
                                    $scope.json.nro_aut = $scope.json.nro_aut + "";
                                }

                                if ($scope.json.cod_programapyp == null || $scope.json.cod_programapyp == undefined) {
                                    $scope.json.cod_programapyp = "";
                                }
                                if ($scope.json.nro_aut == null || $scope.json.nro_aut == undefined) {
                                    $scope.json.nro_aut = "";
                                }

                                Service.getCrud({ head: angular.toJson($scope.json), listproc: angular.toJson($scope.grilla), codigo_usuario: $scope.cod_user, historia: Datos.info.item.historia }, "/Admision.svc/UpdateCitaCumplida").then(function (d) {
                                    rs = JSON.parse(d.data);
                                    if (rs == true) {
                                        var f = $('#formRecaudo');
                                        $(f).find('input[type=text],textarea,select,input[type=number],input[type=time],input[type=checkbox]').val('');
                                        $(f).find('input[type=checkbox]').attr('checked', false);
                                        $('#brp-clave').prop("disabled", false);
                                        $('#brp-valor').prop("disabled", false);
                                        $('#brPaciente-clave').prop('disabled', false);
                                        $('#brPaciente-valor').prop('disabled', false);
                                        $('#brPaciente-btn').prop('disabled', false);
                                        $scope.grilla = [];
                                        //Datos.info = {};
                                        $scope.json.codigo = '';
                                        if ($scope.json.total > 0) {
                                            $scope.openModalPago = true;
                                        }
                                       // $scope.cons
                                        else {
                                            $scope.openModal = true;/*
                                            var currentButton = angular.element(document.getElementById('cerrar'));
                                            var currbtn = angular.element(document.getElementById('btncolsultar'));
                                            $timeout(function () {
                                                currentButton.triggerHandler("click");
                                                $('#cerrar').click();
                                                currbtn.triggerHandler("click");
                                                $('#btncolsultar').click();
                                            });*/
                                            $scope.imprimir2();
                                        }
                                        $scope.openWaiting = false;

                                    } else {
                                        alert("Los Datos insertados Son Incorrectos");
                                        $scope.openWaiting = false;
                                    }

                                });
                                defender.resolve(true);
                            } catch (e) {
                                defender.reject(e);
                            }
                        });
                    } else {
                        $timeout(function () {
                            try {
                                $scope.json.recibo = parseInt($scope.json.recibo);
                                $scope.empresa.forEach(function (item, index) {
                                    if (item.codigo.trim() == $scope.adminn) {
                                        $scope.json.nit = item.nit;
                                    }
                                })
                                var rs = false;
                                if ($scope.json.nro_aut != "" || $scope.json.nro_aut != null || $scope.json.nro_aut != undefined) {
                                    $scope.json.nro_aut = $scope.json.nro_aut + "";
                                }

                                if ($scope.json.cod_programapyp == null || $scope.json.cod_programapyp == undefined) {
                                    $scope.json.cod_programapyp = "";
                                }
                                if ($scope.json.nro_aut == null || $scope.json.nro_aut == undefined) {
                                    $scope.json.nro_aut = "";
                                }

                                Service.getCrud({ head: angular.toJson($scope.json), listproc: angular.toJson($scope.grilla), codigo_usuario: $scope.cod_user, historia: Datos.info.item.historia }, "/Admision.svc/UpdateCitaCumplida_hosp").then(function (d) {
                                    rs = JSON.parse(d.data);
                                    if (rs == true) {
                                        var f = $('#formRecaudo');
                                        $(f).find('input[type=text],textarea,select,input[type=number],input[type=time],input[type=checkbox]').val('');
                                        $(f).find('input[type=checkbox]').attr('checked', false);
                                        $('#brp-clave').prop("disabled", false);
                                        $('#brp-valor').prop("disabled", false);
                                        $('#brPaciente-clave').prop('disabled', false);
                                        $('#brPaciente-valor').prop('disabled', false);
                                        $('#brPaciente-btn').prop('disabled', false);
                                        $scope.grilla = [];
                                        //Datos.info = {};
                                        $scope.json.codigo = '';
                                        if ($scope.json.total > 0) {
                                            $scope.openModalPago = true;
                                        }
                                        else {
                                            $scope.openModal = true;/*
                                            var currentButton = angular.element(document.getElementById('cerrar'));
                                            var currbtn = angular.element(document.getElementById('btncolsultar'));
                                            $timeout(function () {
                                                currentButton.triggerHandler("click");
                                                $('#cerrar').click();
                                                currbtn.triggerHandler("click");
                                                $('#btncolsultar').click();
                                            });*/
                                            $scope.imprimir2();
                                        }
                                        $scope.openWaiting = false;

                                    } else {
                                        alert("Los Datos insertados Son Incorrectos");
                                        $scope.openWaiting = false;
                                    }

                                });
                                defender.resolve(true);
                            } catch (e) {
                                defender.reject(e);
                            }
                        });
                    }
                    $scope.openWaiting = true;

                }
                return promise;
            }
        };

        /*COMBOS*/
        Service.getCrud({}, "/Salud.svc/PreloadRecaudo").then(function (d) {
            var data = JSON.parse(d.data);
            $scope.causa = data[0];
            $scope.programa = data[1];
            $scope.centros = data[2];
            $scope.pago = data[3];
            $scope.tipo_cita = data[4];
            $scope.empresa = data[5];

        });

        $scope.buscaradm = function () {
            $scope.empresa.forEach(function (item, ind) {
                if ($scope.admin.trim() == item.nombre.trim()) {
                    $scope.adminn = item.codigo.trim();
                }
            });
        }
        $scope.readprestador = function () {
            var rs = [];
            Service.getCrud({ prestador: $scope.json3.prestador, grupo: $scope.grupo_serv }, "/Admision.svc/Readterceroprestador").then(function (d) {
                rs = JSON.parse(d.data);
                $scope.grillaprest = rs;
            });
        }

        $scope.closeprestadorserv = function () {
            $scope.buscaprest_serv = false;
        }

        /*-------- confirmar cita --------------------*/
        $scope.confirmCita = function (item, i) {
            $scope.buscaradm();
            console.log(item);
            console.log(i);
            console.log(Datos);
            $scope.grupo_serv = item.grupo;
            $scope.readprestador();
            if (item != null) {
                if(Datos.info.tipo == '1'){
                    Service.getCrud({ codigo: item.cod_proc }, "/Admision.svc/GetCantLab").then(function (d) {
                        var x = JSON.parse(d.data);
                        $scope.cantab = x;
                        console.log(x);
                        if ($scope.cantab != null) {
                            if ($scope.cantab > 0) {
                                Service.getCrud({ nivel: parseInt($scope.nivel), codigo: $scope.json.codigo, admin: $scope.adminn }, "/Salud.svc/GetNivelAdmin").then(function (d) {
                                    var x = JSON.parse(d.data);
                                    $scope.json.total = x;
                                });
                                Service.getCrud({ Grupo: item.grupo }, "/Admision.svc/Read_Grupo_Prest").then(function (d) {
                                    var x = JSON.parse(d.data);
                                    if (x.length == 0) {
                                        $scope.grilla.forEach(function (it, ind) {
                                            if (ind == i) {
                                                it.prestador = "";
                                                it.cod_prest = "";
                                            }
                                        })
                                    }
                                    if (x.length > 0) {
                                        if (x.length == 1) {
                                            x.forEach(function (item, index) {
                                                $scope.grilla.forEach(function (it, ind) {
                                                    if (ind == i) {
                                                        Service.getCrud({ codigo: item.id_prestador }, "/Archivo.svc/ReadTercero").then(function (d) {
                                                            var j = JSON.parse(d.data);
                                                            if(j != null){
                                                                it.prestador = j.nombre.trim();
                                                                it.cod_prest = j.codigo.trim();
                                                            }else{
                                                                it.prestador = "";
                                                                it.cod_prest = "";
                                                            }
                                                        });
                                                    }
                                                })
                                            })
                                        } else {
                                            $scope.grupo_serv = item.grupo;
                                            $scope.index = i;
                                            console.log($scope.grillaprest.length);
                                            console.log($scope.grillaprest);
                                            if ($scope.grillaprest.length > 1) {
                                                $scope.buscaprest_serv = true;
                                            } else {
                                                $scope.buscaprest_serv = false;
                                                $scope.grilla.forEach(function (i, ind) {
                                                    $scope.grillaprest.forEach(function (x, y) {
                                                        i.prestador = x.nombre;
                                                        i.cod_prest = x.codigo;
                                                    })
                                                })
                                            }
                                        }
                                    }
                                });
                                $scope.grilla.forEach(function (x, y) {
                                    if (y == i) {
                                        item.check = 1;
    
                                    }
                                })
                                $scope.recordgrilla();
                            } else {
                                var l = confirm("El servicio " + item.servicio + " no tiene parametros normales parametrizados. NO PUEDE SER TOMADO");
                                if (l == true) {
                                    var currentButton2 = angular.element(document.getElementById('restore_' + i));
                                    $timeout(function () {
                                        $('#restore_' + i).click();
                                        currentButton2.triggerHandler("click");
                                    });
                                    currentButton2.triggerHandler("click");
                                    console.log("ENTRO TRUE");
                                } else {
                                    var currentButton2 = angular.element(document.getElementById('restore_' + i));
                                    $timeout(function () {
                                        $('#restore_' + i).click();
                                        currentButton2.triggerHandler("click");
                                    });
                                    currentButton2.triggerHandler("click");
                                    console.log("ENTRO FALSE");
                                }
                            }
                        }
                    });
                }else{
                    Service.getCrud({ nivel: parseInt($scope.nivel), codigo: $scope.json.codigo, admin: $scope.adminn }, "/Salud.svc/GetNivelAdmin").then(function (d) {
                        var x = JSON.parse(d.data);
                        $scope.json.total = x;
                    });
                    Service.getCrud({ Grupo: item.grupo }, "/Admision.svc/Read_Grupo_Prest").then(function (d) {
                        var x = JSON.parse(d.data);
                        if (x.length == 0) {
                            console.log("ENTRO");
                            $scope.grilla.forEach(function (it, ind) {
                                if (ind == i) {
                                    it.prestador = "";
                                    it.cod_prest = "";
                                }
                            })
                        }
                        if (x.length > 0) {
                            if (x.length == 1) {
                                x.forEach(function (item, index) {
                                    console.log("ENTRO222");
                                    $scope.grilla.forEach(function (it, ind) {
                                        if (ind == i) {
                                            Service.getCrud({ codigo: item.id_prestador }, "/Archivo.svc/ReadTercero").then(function (d) {
                                                var j = JSON.parse(d.data);
                                                it.prestador = j.nombre.trim();
                                                it.cod_prest = j.codigo.trim();
                                            });
                                        }
                                    })
                                })
                            } else {
                                $scope.grupo_serv = item.grupo;
                                $scope.index = i;
                                $scope.readprestador();
                                console.log($scope.grillaprest.length);
                                console.log($scope.grillaprest);
                                if ($scope.grillaprest.length > 1) {
                                    $scope.buscaprest_serv = true;
                                } else {
                                    $scope.buscaprest_serv = false;
                                    $scope.grilla.forEach(function (i, ind) {
                                        $scope.grillaprest.forEach(function (x, y) {
                                            i.prestador = x.nombre;
                                            i.cod_prest = x.codigo;
                                        })
                                    })
                                }
                            }
                        }
                    });
                    $scope.grilla.forEach(function (x, y) {
                        if (y == i) {
                            item.check = 1;

                        }
                    })
                    $scope.recordgrilla();
                }
                
            }
        }

        $scope.restoredCita = function (item, i) {
            $scope.grilla.forEach(function (x, y) {
                if (y == i) {
                    item.check = 0;
                    $scope.grilla_servicio.splice(i, 1);
                }
            })
            $scope.recordgrilla();
        }

        

        $scope.click_prest = function (item) {
            $scope.grilla.forEach(function (i, ind) {
                if (ind == $scope.index) {
                    i.prestador = item.nombre;
                    i.cod_prest = item.codigo;
                    $scope.buscaprest_serv = false;
                }
            })

        }

        /*---------------------FUNCIONES--------------------------------------------------------------*/
        $scope.recordgrilla = function () {
            $scope.total = 0;
            $scope.grilla.forEach(function (x, y) {
                if (x.check == 1) {
                    $scope.total += x.total;
                }
            })
        }

        $scope.vlr_serv = function (cod_proc, admin, grupo) {
            var defender = $q.defer();
            var promise = defender.promise;
            Service.getCrud({ codigo: cod_proc, Admin: admin, Grupo: grupo }, "/Admision.svc/Read_Var_Serv2").then(function (d) {
                var x = JSON.parse(d.data);
                $scope.valor_serv = x;
                defender.resolve(x);
            });
            return $scope.valor_serv;
        }

        /*CARGADO DE SERVICIO*/
        $scope.cancelar = function () {
            var f = $('#formRecaudo');
            $(f).find('input[type=text],textarea,select,input[type=number],input[type=date],input[type=time],input[type=checkbox]').val('');
            $(f).find('input[type=checkbox]').attr('checked', false);
            $scope.grilla = [];
            $scope.citas_ant = [];
        }
        $scope.cerrar = function () {
            $timeout(function () {
                var currentButton = angular.element(document.getElementById('cerrar'));
                $timeout(function () {
                    $('#cerrar').click();
                    currentButton.triggerHandler("click");
                });
                currentButton.triggerHandler("click");
                Datos.info = {};
            });
        }
        /*---------------------WATCHS-----------------------------------------------------------------*/

        $scope.funcionauxiliar = function (x) {
            if (x == 1 || x == 2) {
                $('#tabla table tr').each(function () {
                    if ($.trim($(this).text()).length > 0) {
                        $(this).addClass("myclass");
                    }
                });
                $timeout(function () {
                    $("#colorear").addClass("myclass");
                });
            }
        }
        $scope.$watch(('fecha'), function (d) {
            $scope.grilla = [];
            $scope.citas_ant = [];
            $scope.citas_ant = '';
            if (d != '') {
                if (d > new Date()) {
                    alert("NO SE PUEDE CONFIRMAR CITAS CON FECHAS MAYORES A LA ACTUAL");
                    $scope.fecha = new Date();
                } else {
                    if ($scope.json.codigo == null || $scope.json.codigo == '' || $scope.json.codigo == undefined) {
                    } else {
                        $scope.json.fecha_cons = ssDate.dateToString(d, 'dd-MM-yyyy');
                        Service.getCrud({ codigo: $scope.json.codigo, fecha_cons: $scope.json.fecha_cons }, "/Salud.svc/GetCitas_codigo_fecha").then(function (rs) {
                            $scope.grilla = JSON.parse(rs.data);
                        });
                        Service.getCrud({ codigo: $scope.json.codigo, fecha: $scope.json.fecha_cons }, "/Salud.svc/RecordRecaudo").then(function (rs) {
                            $scope.citas_ant = JSON.parse(rs.data);
                        });
                    }
                }

            }
        });
        /*------- AUTORIZACIONES ----------------*/
        $scope.shpyp = false;
        $scope.getadmin = function () {
            if ($scope.admin != undefined || $scope.admin != '') {
                Service.getCrud({ cod_admin: $scope.admin }, "/Salud.svc/getAdmin").then(function (rs) {
                    $scope.j = JSON.parse(rs.data);
                    if ($scope.j != null) {
                        if ($scope.j.autorizacion == 0) {
                            $scope.shaut = false;
                            $scope.json.nro_aut = '';
                        }
                        if ($scope.j.autorizacion == 1) {
                            $scope.shaut = true;
                            $scope.json.nro_aut = '';
                        }
                        if ($scope.j.asistencialpyp == 1 || $scope.j.asistencialpyp == 0) {
                            $scope.shpyp = false;
                        }
                        if ($scope.j.asistencialpyp == 2) {
                            $scope.shpyp = true;
                        }

                    }

                });
            }
        }
        $scope.calc = function () {
            var j;
            if ($scope.tarifa != undefined) {
                Service.getCrud({ empresa: $scope.empres, proce: $scope.json.cod_proc, tarifa: $scope.tarifa }, "/Salud.svc/GetValor_Conf").then(function (d) {
                    j = JSON.parse(d.data)[0];
                    if (j != undefined) {
                        $scope.json.cod_admin = j.Codigo_Emp;
                        $scope.json.nit = j.Nit_Emp;
                        var aux = j.Indicador;
                        if (aux == 1) {
                            var total = 0;
                            var valor = parseInt(j.Valor_Pro);
                            var porc = parseInt(j.Porcentaje_Pro);
                            if (porc <= 0) {
                                total = valor;
                            } else {
                                total = ((valor * (porc / 100)) + (valor));
                            }
                            $scope.json.total = total;
                        }
                        if (j["Indicador"] == 2) {
                            var total = 0;
                            var valor = parseInt(j.Valor_Pro);
                            var porc = parseInt(j.Porcentaje_Pro);
                            if (porc <= 0) {
                                total = valor;
                            } else {
                                total = ((valor) - (valor * (porc / 100)));
                            }
                            $scope.json.total = total;
                        }
                        if (aux == 3) {
                            var total = 0;
                            var valor = j.Valor_Pro;
                            var porc = j.Porcentaje_Pro;
                            if (porc <= 0) {
                                total = valor;
                            } else {
                                total = valor;
                            }
                            $scope.json.total = total;
                        }
                    }
                    else {
                        $scope.json.nit = "";
                        $scope.json.cod_admin = $scope.admin;
                    }
                });
            }
        };
        /*MODAL*/
        $scope.open = function () {
            $scope.openModal = true;

        }
        $scope.close = function () {
            $scope.openModal = false;
        }
        $scope.open2 = function () {
            $scope.openModalDos = true;
        }
        $scope.close2 = function () {
            $scope.openModalDos = false;

        }

        $scope.fuente_fact = function (x) {
            var defender = $q.defer();
            var promise = defender.promise;
            $scope.consulta_piso = "select count(*) as cant from urgencia012018 where fuente='1' and factura='1' and historia='" + x + "'";
            $scope.x = Consultas.read($scope.consulta_piso);
            $timeout(function () {
                if ($scope.x != undefined) {
                    var z = ($scope.x.$$state.value)[0];
                    if (z == null || z == undefined) {
                        $scope.piso = 0;
                    }
                    else {
                        $scope.piso = parseInt(z.cant);
                    }
                    defender.resolve(z);
                } else {
                    $scope.piso = 0;
                }
            }, 200);
            return promise;
        }

        $scope.und_atencion = function (item) {
            if (item == 3) {
                return "CONSULTA EXTERNA"
            } else if (item == 2) {
                return "URGENCIA"
            } else if (item == 1) {
                return "HOSPITALIZACION"
            }
        }
        $scope.cargarreport2 = function () {
            $scope.nit = Datos.nit;
            $scope.urlreport = Service.getReport('/TomaOrden?title=Orden de Servicio '+$scope.und_atencion(Datos.info.item.tipo_histo)+'&historia=' + Datos.info.item.historia + '&nit=' + $scope.nit + '&numero=' + Datos.info.item.numero);
            document.getElementById('reporte').src = $scope.urlreport;
        }
        $scope.imprimir2 = function () {
            $timeout(function (d) {
                $scope.myModalrep = true;
                $scope.cargarreport2();
                $('#myModal').show();
            }, 500)
        }
        $scope.closed = function () {
            $scope.myModalrep = false;
            $('#myModal').hide();
            var currentButton = angular.element(document.getElementById('cerrar'));
            var currbtn = angular.element(document.getElementById('btncolsultar'));
            $timeout(function () {
                currentButton.triggerHandler("click");
                $('#cerrar').click();
                currbtn.triggerHandler("click");
                $('#btncolsultar').click();
            });
            Datos.info = {};
        }

    }
    function ResultOrdenesDiagController($scope, $q, $timeout, ssDate, Datos, Service, Auth) {
        $scope.json = {};
        $scope.cod_user = Auth.getUserObj().codigo;
        $scope.user = Auth.getUserObj().usuario1;
        $scope.auxuser = Auth.getUserObj().usuario1;
        $scope.tabla = [];
        $scope.alerta = false;
        $scope.ds_proc = false;
        $scope.ds_actu = true;
        $scope.act = false;
        console.log(Datos.info.item);
        $scope.edt = true;
        $scope.sh_ordenes_diag = false;
        $scope.msgalert = undefined;
        $scope.json.filtro = Datos.info.filtro;
        $scope.json2 = {};
        $scope.nit = Datos.nit;
        $timeout(function () {
            Service.getCrud({ filtro: Datos.info.filtro, tipo: Datos.info.tipo, numero: Datos.info.item.numero, paciente: Datos.info.item.codigo, id: Datos.info.item.historia }, "/Admision.svc/BuscarOrdenes").then(function (d) {
                var rs = JSON.parse(d.data);
                if(rs[0]=='?'){
                    alert(rs);
                }
                else{
                    rs.forEach(function (item, index) {
                        if (item.cod_pac == undefined) {
                            item.cod_pac = item.codigo;
                        }
                        /*if (item.reporte != "") {
                            item.tipoestado = "Realizado";
                        }*/
                        $scope.tabla.push(item);
                    })
                    
                }                
            })
        }, 500)
        /*PANEL PACIENTE*/
        $scope.dataPanelPaciente = { codigo: Datos.info.item.codigo, cod_admin: Datos.info.item.cod_admin };
        $scope.datapanelpacienteprint = { codigo: Datos.info.item.codigo, cod_admin: Datos.info.item.cod_admin };

        $scope.profilenivel = angular.toJson({ modulo: 'CONSULTA EXTERNA', ventana: '03', usuario: $scope.auxuser });

        $scope.disabledbtn = { buscar: false, imprimir: true, nuevo: false, editar: true, eliminar: true };
        $scope.showbtn = { buscar: true, imprimir: true, nuevo: true, editar: true, eliminar: true };

        Service.getCrud({}, "/Admision.svc/PreloadIngreso").then(function (d) {
            $scope.medicos = JSON.parse(d.data)[9];
        });

        Service.getCrud({}, "/Generic.svc/DatosEmpresa").then(function (d) {
            var rs = JSON.parse(d.data)[0];
            $scope.sis = rs;
            $scope.nit = $scope.sis.nit;

            Service.getCrud({ nit: $scope.sis.nit }, "/Generic.svc/imgLogo").then(function (d) {
                var rs = JSON.parse(d.data);
                $scope.logo = rs;
                if (rs != null && rs != undefined && rs != '') {
                    $scope.logo = ('data:image/jpeg;base64,' + rs);
                }
            });

        });

        Service.getCrud({ usuario: $scope.auxuser, modulo: 'PERMISOS ESPECIALES', ventana: '13' }, "/Generic.svc/ReadNivel").then(function (d) {
            var j = JSON.parse(d.data);
            if (j == 1111) {
                $scope.ds_actu = false;
            } else {
                $scope.ds_actu = true;
            }
        });

        $scope.databuscadorprotocolos = {
            id: 'databuscadorprotocolos',
            component: [{ id: 'buscar_protocolos', type: 'btn' }],
            table: 'protocolosrx',
            column: [
                { name: 'rtrim(ltrim(codigo))', as: 'Codigo', visible: true },
                { name: 'rtrim(ltrim(nombre))', as: 'Nombre', visible: true }
            ],
            where: [],
            groupby: false,
            orderby: [],
            title: 'Buscar Protocolo',
        }
        $scope.rsprotocolos = function (d) {
            var j = JSON.parse(d);
            var codigo = j.Codigo;
            if (codigo != undefined) {
                Service.getCrud({ codigo: codigo }, "/Admision.svc/readprotocolorx").then(function (d) {
                    $scope.json.resultado = d.data;
                });
            }
        };

        $scope.convert = function (fecha) {
            return ssDate.militodateString(fecha, 'dd-MM-yyyy');
        }

        $scope.select = function (item, index) {
            //$scope.json2 = {};
            Datos.info = {};
            //$scope.json2 = item;
            $scope.alerta = false;
            $scope.json.idweb = item.idweb;
            $scope.json.tipo = Datos.info.Parametro;
            $scope.json.tipo_histo = item.tipo_histo;
            $scope.json.orden = item.numero;
            $scope.json.cod_proc = item.cod_proc;
            console.log(item);
            var estselect;

            $('#grilla-' + $scope.outseleted).removeClass("seleccion");

            $('#grilla-' + index).addClass("seleccion");

            $scope.outseleted = index;
            Datos.info.item = item;
            Service.getCrud({ json: angular.toJson($scope.json) }, "/Admision.svc/ReadrsProtocolo").then(function (d) {
                var rs = JSON.parse(d.data);
                //console.log(rs);
                if (rs[0].reporte != undefined) {
                    if (rs[0].reporte.trim() != '') {
                        $scope.json.medico = rs[0].cod_med_rep.trim();
                        $scope.json.resultado = rs[0].reporte;
                        $scope.ds_proc = true;
                        $scope.ds_actu=false;
                        $('#ds_act').prop('disabled',true);
                        $('#ds_act').attr('disabled',true);
                    } else {
                        $scope.ds_proc = false;
                        /*
                        $('#ds_act').prop('disabled',true);
                        $('#ds_act').attr('disabled',true);*/
                        //$scope.ds_actu=true;
                        
                    }
                }
                if (rs[0].resultado != undefined) {
                    if (rs[0].resultado.trim() != '') {
                        $scope.json.medico = rs[0].usuario_reporte.trim();
                        $scope.json.resultado = rs[0].resultado;
                        $scope.ds_proc = true;
                        $scope.ds_actu=false;
                        $('#ds_act').prop('disabled',true);
                        $('#ds_act').attr('disabled',true);
                        
                    } else {
                        $scope.ds_proc = false;
                        //$scope.ds_actu=true;
                        /*
                        $('#ds_act').prop('disabled',true);
                        $('#ds_act').attr('disabled',true);*/
                    }
                }
            });
        }

        $scope.edit = function(){
            $('#ds_act').prop('disabled',false);
            $('#ds_act').attr('disabled',false);
            $scope.act = true;
            $scope.edt = false;
        }

        $scope.cerrar = function () {
            $timeout(function () {
                var currentButton = angular.element(document.getElementById('cerrar'));
                $timeout(function () {
                    $('#cerrar').click();
                    currentButton.triggerHandler("click");
                });
                currentButton.triggerHandler("click");
                Datos.info = {};
            });
        }
        $scope.outseleted = '-1';

        /*CRUD*/
        $scope.guardar = function () {
            var defender = $q.defer();
            var promise = defender.promise;
            if (!$scope.formrsdiag.$valid) {
                var cr = "Campos requeridos no diligenciados:";
                $scope.formrsdiag.$error.required.forEach(function (item, index) {
                    cr += "\n - " + item.$name;
                });
                alert(cr);
                defender.resolve(false);
                return promise;
            } else {
                if ($scope.json.idweb != undefined && $scope.json.idweb != '' && $scope.json.idweb != null) {
                    $timeout(function () {
                        try {
                            var rs = false;
                            $scope.openWaiting = true;
                            Service.getCrud({ json: angular.toJson($scope.json), codigo_usuario: $scope.cod_user }, "/Admision.svc/CreateRsOrdenesDiag").then(function (d) {
                                rs = JSON.parse(d.data);
                                if(rs[0]=='?'){
                                    alert(rs);
                                    $scope.openWaiting = false;
                                }else{
                                    $scope.openWaiting = false;
                                    $scope.msgalert = "TRANSCRIPCION DE RESULTADO EXITOSO!"
                                    $scope.alerta = true;
                                    $scope.ds_proc = true;   
                                    //$scope.ds_actu=true;  
                                    $('#ds_act').prop('disabled',true);
                                    $('#ds_act').attr('disabled',true);
                                    $scope.act = false;
                                    $scope.edt = true;                               
                                    $scope.json.resultado = "";
                                    var currentButton = angular.element(document.getElementById('btncolsultar'));
                                    $timeout(function () {
                                        currentButton.triggerHandler("click");
                                        $('#btncolsultar').click();
                                    });
                                    defender.resolve(rs);
                                }
                            });
                        } catch (e) {
                            defender.reject(e);
                        }
                    });
                } else {
                    alert("DEBE SELECCIONAR UN NUMERO DE ORDEN");
                }
            }
            return promise;
        };

        $scope.est = function (t) {
            t.estado = t.tipoestado;

            var aux;
            if (t.estado != undefined) {
                if (t.estado.trim() == '') {
                    aux = "TsinNivel";

                }
                else {
                    if (t.estado.trim() == 'Cumplida') {
                        aux = "Cumplida";
                    }
                    else if (t.estado.trim() == 'Tomado') {
                        aux = "Tomado";
                    }
                    else if (t.estado.trim() == 'Realizado') {
                        aux = "Realizado";
                    }
                }

            }
            if (t.cod_med_rep != undefined) {
                if (t.cod_med_rep.trim() == '') {
                    aux = "Tomado";

                } else {
                    if (t.cod_med_rep.trim() != '') {
                        aux = "Realizado";
                    }
                }
            }
            return aux;
        };

        $scope.validarfirma = function () {
            $scope.modelo = {}
            Service.getCrud({ codigo: $scope.json2.medico, cod_pac: '' }, '/Salud.svc/firma_medico1').then(function (d) {
                var rs = JSON.parse(d.data);
                $scope.model = rs[0];
                $scope.especi = rs[1];
                $scope.model1 = rs[2];
                $scope.nombre_medico = $scope.especi[0].nombre;
                $scope.registro_med = $scope.especi[0].registro_medico;
                $scope.espe = $scope.especi[1].nombre;
                $scope.upload = $scope.model.rutafirma;
                if ($scope.upload != null && $scope.upload.trim() != '') {
                    $('#labelfirma').css("margin-top", "100px");
                }
                else {
                    $('#labelfirma').css("margin-top", "30px");
                }
                if ($scope.model.rutafirma != null && $scope.model.rutafirma != undefined && $scope.model.rutafirma != '') {
                    $scope.upload = ('data:image/jpeg;base64,' + $scope.model.rutafirma);
                }
                $scope.openWaiting = false;
                $timeout(function () {
                    $('#menu').addClass("ss-nav-inactive");
                    $('#wrapper').addClass("ss-full-view");
                    window.print();
                    $scope.sh_ordenes_diag = false;
                }, 3000);
            });
        }

        $scope.readresult = function () {
            $scope.json2 = {};
            $scope.openWaiting = true;
            $scope.upload = undefined;
            $scope.nombre_medico = undefined;
            $scope.espe = undefined;
            $scope.registro_med = undefined;
            Service.getCrud({ json: angular.toJson($scope.json) }, "/Admision.svc/ReadrsProtocolo").then(function (d) {
                var rs = JSON.parse(d.data);
                console.log(rs);
                $scope.json.medico = (rs[0].usuario_reporte).trim();
                /*
                $scope.json2 = rs[0];
                $scope.json2.auxanio = rs[0].año;
                if (rs[0].reporte != undefined) {
                    $scope.json.resultado = rs[0].reporte;
                    $scope.json2.reporte = rs[0].reporte;
                }
                if (rs[0].resultado != undefined) {
                    $scope.json.resultado = rs[0].resultado;
                    $scope.json2.reporte = rs[0].resultado;
                }
                if (rs[0] != false) {
                    $scope.json2.fecha = ssDate.militodateString(rs[0].fecha);

                    if (rs[0].cod_med_rep != undefined) {
                        $scope.json2.medico = rs[0].cod_med_rep.trim();
                    }
                    if (rs[0].cod_med_reporte != undefined) {
                        $scope.json2.medico = rs[0].cod_med_reporte.trim();
                    }
                    $scope.json2.nom_c_costo = rs[1];
                    if ($scope.json2.medico != undefined && $scope.json2.medico != null) {
                        if ($scope.json2.medico.trim() != '') {
                        } else {
                            $scope.openWaiting = false;
                            alert("ESTE PROCEDIMIENTO NO TIENE NINGUN RESULTADO TRANSCRITO INTETELO NUEVAMENTE");
                        }
                    }

                }*/
            });
        }

        $scope.imprimir = function () {
            $scope.sh_ordenes_diag = true;
            $scope.readresult();
        }

        $scope.cargarreport3 = function () {
            $scope.urlreport = undefined;
            document.getElementById('reporte2').src = undefined;
            if(Datos.info.item.tipo_histo == 1){
                $scope.urlreport = Service.getReport('/ApoyoDiagHosp?historia='+Datos.info.item.historia+'&cod_proc='+Datos.info.item.cod_proc.trim()+'&nit='+$scope.nit+'&numero='+Datos.info.item.numero);
                document.getElementById('reporte2').src = $scope.urlreport;
            }else if(Datos.info.item.tipo_histo == 2){
                $scope.urlreport = Service.getReport('/ApoyoDiagUrg?historia='+Datos.info.item.historia+'&cod_proc='+Datos.info.item.cod_proc.trim()+'&nit='+$scope.nit+'&numero='+Datos.info.item.numero);
                 document.getElementById('reporte2').src = $scope.urlreport;
            }else{
                $scope.urlreport = Service.getReport('/ApoyoDiagConsExt?historia='+Datos.info.item.historia+'&cod_proc='+Datos.info.item.cod_proc.trim()+'&nit='+$scope.nit+'&numero='+Datos.info.item.numero);
                document.getElementById('reporte2').src = $scope.urlreport;
            }  
        }
        $scope.imprimir3 = function(){
            $timeout(function (d) {
                $scope.myModalRep = true;
                $scope.cargarreport3();
                $('#myModalito').show();
            },500)
        }
        $scope.closed2 = function () {
            $scope.myModalRep = false;
            $('#myModalito').hide();
        }

    }
    function CajaController($scope, $q, $timeout, ssDate, Service, Auth, Datos) {
        $scope.json = {};
        $scope.cod_user = Auth.getUserObj().codigo;
        $scope.auxuser = Auth.getUserObj().usuario1;
        $scope.profilenivel = angular.toJson({ modulo: 'TESORERIA', ventana: '01', usuario: $scope.auxuser });
        $scope.disabledbtn = { buscar: false, imprimir: true, nuevo: false, editar: true, eliminar: true };
        $scope.showbtn = { buscar: true, imprimir: true, nuevo: true, editar: true, eliminar: true };
        $scope.key = { value: '' };
        $scope.grillaforma_pago =[];
        $scope.swbuscar = true;
        $scope.sh_edt = false;
        $scope.openModalPago = false;
        $scope.dataAtencion = {
            id: 'brAtencion',
            component: [{ id: 'brAtencion', type: '' }],
            inner_join: [{ table: 'paciente  as p', on: 'p.codigo=c.codigo', join: '' }],
            where: {},
            groupby: false,
            orderby: [' c.historia desc '],
            title: 'Buscar atencion',
            required_name: 'Atencion'
        };
        $scope.$watch('json.tipo_historia', function (d) {
            if (d != undefined) {
                if (d == 1) {
                    $scope.dataAtencion.table = 'hospitalizacion01 as c ';
                    $scope.dataAtencion.column = [];
                    $scope.dataAtencion.column.push(
                        { name: 'c.año', as: "Año", visible: true },
                        { name: 'c.historia', as: "Atencion", visible: true },
                        { name: 'c.tipo_disc', as: 'Centro_costo', visible: false },
                        { name: 'c.id', as: "Id", visible: false },
                        { name: 'p.codigo', as: 'Codigo', visible: true },
                        { name: 'p.nombre', as: 'Nombre', visible: true },
                        { name: 'p.ppellido', as: 'Apellido', visible: true },
                        { name: 'c.nit', as: 'Nit', visible: false }
                    );
                }
                if (d == 2) {
                    var year = new Date().getFullYear;
                    $scope.dataAtencion.table = 'urgencia01' + year + " as c";
                    $scope.dataAtencion.column = [];
                    $scope.dataAtencion.column.push(
                        { name: 'c.año', as: "Año", visible: true },
                        { name: 'c.historia', as: "Atencion", visible: true },
                        { name: 'c.c_costo', as: 'Centro_costo', visible: false },
                        { name: 'c.id', as: "Id", visible: false },
                        { name: 'p.codigo', as: 'Codigo', visible: true },
                        { name: 'p.nombre', as: 'Nombre', visible: true },
                        { name: 'p.ppellido', as: 'Apellido', visible: true },
                        { name: 'c.nit', as: 'Nit', visible: false }
                    );
                }
                if (d == 3) {
                    $scope.dataAtencion.table = 'consulta01 as c';
                    $scope.dataAtencion.column = [];
                    $scope.dataAtencion.column.push(
                        { name: 'c.año', as: "Año", visible: true },
                        { name: 'c.historia', as: "Atencion", visible: true },
                        { name: 'c.centro_costo', as: 'Centro_costo', visible: false },
                        { name: 'c.id', as: "Id", visible: false },
                        { name: 'p.codigo', as: 'Codigo', visible: true },
                        { name: 'p.nombre', as: 'Nombre', visible: true },
                        { name: 'p.ppellido', as: 'Apellido', visible: true },
                        { name: 'c.nit', as: 'Nit', visible: false }
                    );
                }
            }
        });
        $scope.rsAtencion = function (rs) {
            var j = JSON.parse(rs);
            console.log(j);
            $scope.json.historia = j.Atencion;
            $scope.year = j.Año;
            $scope.json.id = j.Id;
            //$scope.json.centro_costo = j.Centro_costo;
            if ($scope.swbuscar != false) {
                $scope.consultarcaja01($scope.json.historia, $scope.year);
            }
            $scope.traerinfo($scope.json.historia, $scope.year);
        }
        $scope.deleteforma_pago = function (x) {
            console.log(x);
            $scope.grillaforma_pago.splice(x, 1);
        }
        $scope.inicializar = function () {
            $timeout(function () {
                $('#brPaciente-valor').css('width', '50px');
            }, 500)
        }
        $scope.inicializar();
        $scope.traerinfo = function (h, a) {
            var rs ;
            try {
                if (h != null && h != undefined) {
                    Service.getCrud({ historia: h, year: a, tipo_histo: $scope.json.tipo_historia }, "/Salud2.svc/traerinfoc01").then(function (d) {
                         rs = JSON.parse(d.data);
                        if (rs[0] == '?') {
                            alert(rs);
                        }
                        if (rs != null && rs != []) {
                            $scope.json.codigo = rs.Codigo;
                            $scope.json.paciente = rs.Nombre;
                            $scope.json.admin = rs.Admin;
                            $scope.json.empresa = rs.Cod_admin;
                            console.log(rs);
                            //$scope.json.total = rs.Coopago; 
                        }
                    });
                }
            } catch (e) {
                console.log(e);
            }
        }
        $scope.consultarcaja01 = function (h, a) {
            try {
                if (h != null && h != undefined) {
                    Service.getCrud({ historia: h, year: a }, "/Salud2.svc/consultarcaja01").then(function (d) {
                        var rs = JSON.parse(d.data);
                        console.log(rs);
                        if (rs[0] == '?') {
                            alert(rs)
                        } else if (rs == true) {
                            alert("ESTE INGRESO YA ESTA RECAUDADO!");
                        }

                    });
                }
            } catch (e) {
                console.log(e);
            }
        }
        $scope.cambiarformapago = function (fp) {
            Service.getCrud({ fp: fp }, "/Salud.svc/CambiarFormapago").then(function (d) {
                var rs = JSON.parse(d.data);
                $scope.json.fuente = rs.fuente;
                $scope.json.numero = rs.consecutivo;
            })
        }
        $scope.calcular = function () {
            $scope.cambio = (parseInt($scope.json.total) - parseInt($scope.json.valorcoopago)).toString();
        }
        $scope.add_fp = function () {
            if ($scope.json.total != null && $scope.json.total != undefined && $scope.json.total != 0) {
                if ($scope.json.valorcoopago != null && $scope.json.valorcoopago != undefined && $scope.json.valorcoopago != "") {
                    if ($scope.json.forma_pago != null && $scope.json.forma_pago != undefined && $scope.json.forma_pago != "") {
                        var sumador = 0;
                        $scope.grillaforma_pago.push({ fuente: $scope.json.fuente, numero: $scope.json.numero, forma_pago: $scope.json.forma_pago, valorcoopago: $scope.json.valorcoopago })
                        $scope.grillaforma_pago.forEach(function (item, index) {
                            sumador += parseFloat(item.valorcoopago);
                        })
                        $scope.json.valorcoopago = parseFloat($scope.json.total) - parseFloat(sumador);
                        $scope.calcular();
                    } else {
                        alert("Seleccione forma de pago")
                    }
                } else {
                    alert("Seleccione valor");
                }
            } else {
                alert("Seleccione el valor del coopago")
            }
        }
        Service.getCrud({}, "/Salud.svc/PreloadRecaudo").then(function (d) {
            var data = JSON.parse(d.data);
            if (d.data[1] == '?') {
                alert(d.data);
            }
            $scope.formapago = data[6];
        });
        function Unidades(num) {

            switch (num) {
                case 1: return "UN";
                case 2: return "DOS";
                case 3: return "TRES";
                case 4: return "CUATRO";
                case 5: return "CINCO";
                case 6: return "SEIS";
                case 7: return "SIETE";
                case 8: return "OCHO";
                case 9: return "NUEVE";
            }

            return "";
        }//Unidades()
        function Decenas(num) {

            var decena = Math.floor(num / 10);
            var unidad = num - (decena * 10);

            switch (decena) {
                case 1:
                    switch (unidad) {
                        case 0: return "DIEZ";
                        case 1: return "ONCE";
                        case 2: return "DOCE";
                        case 3: return "TRECE";
                        case 4: return "CATORCE";
                        case 5: return "QUINCE";
                        default: return "DIECI" + Unidades(unidad);
                    }
                case 2:
                    switch (unidad) {
                        case 0: return "VEINTE";
                        default: return "VEINTI" + Unidades(unidad);
                    }
                case 3: return DecenasY("TREINTA", unidad);
                case 4: return DecenasY("CUARENTA", unidad);
                case 5: return DecenasY("CINCUENTA", unidad);
                case 6: return DecenasY("SESENTA", unidad);
                case 7: return DecenasY("SETENTA", unidad);
                case 8: return DecenasY("OCHENTA", unidad);
                case 9: return DecenasY("NOVENTA", unidad);
                case 0: return Unidades(unidad);
            }
        }//Unidades()
        function DecenasY(strSin, numUnidades) {
            if (numUnidades > 0)
                return strSin + " Y " + Unidades(numUnidades)

            return strSin;
        }//DecenasY()
        function Centenas(num) {
            var centenas = Math.floor(num / 100);
            var decenas = num - (centenas * 100);

            switch (centenas) {
                case 1:
                    if (decenas > 0)
                        return "CIENTO " + Decenas(decenas);
                    return "CIEN";
                case 2: return "DOSCIENTOS " + Decenas(decenas);
                case 3: return "TRESCIENTOS " + Decenas(decenas);
                case 4: return "CUATROCIENTOS " + Decenas(decenas);
                case 5: return "QUINIENTOS " + Decenas(decenas);
                case 6: return "SEISCIENTOS " + Decenas(decenas);
                case 7: return "SETECIENTOS " + Decenas(decenas);
                case 8: return "OCHOCIENTOS " + Decenas(decenas);
                case 9: return "NOVECIENTOS " + Decenas(decenas);
            }

            return Decenas(decenas);
        }//Centenas()
        function Seccion(num, divisor, strSingular, strPlural) {
            var cientos = Math.floor(num / divisor)
            var resto = num - (cientos * divisor)

            var letras = "";

            if (cientos > 0)
                if (cientos > 1)
                    letras = Centenas(cientos) + " " + strPlural;
                else
                    letras = strSingular;

            if (resto > 0)
                letras += "";

            return letras;
        }//Seccion()
        function Miles(num) {
            var divisor = 1000;
            var cientos = Math.floor(num / divisor)
            var resto = num - (cientos * divisor)

            var strMiles = Seccion(num, divisor, "UN MIL", "MIL");
            var strCentenas = Centenas(resto);

            if (strMiles == "")
                return strCentenas;

            return strMiles + " " + strCentenas;
        }//Miles()
        function Millones(num) {
            var divisor = 1000000;
            var cientos = Math.floor(num / divisor)
            var resto = num - (cientos * divisor)

            var strMillones = Seccion(num, divisor, "UN MILLON DE", "MILLONES DE");
            var strMiles = Miles(resto);

            if (strMillones == "")
                return strMiles;

            return strMillones + " " + strMiles;
        }//Millones()
        $scope.numtoleters = function (num) {
            var data = {
                numero: num,
                enteros: Math.floor(num),
                centavos: (((Math.round(num * 100)) - (Math.floor(num) * 100))),
                letrasCentavos: "",
                letrasMonedaPlural: 'PESOS',//"PESOS", 'Dólares', 'Bolívares', 'etcs'
                letrasMonedaSingular: 'PESO', //"PESO", 'Dólar', 'Bolivar', 'etc'

                letrasMonedaCentavoPlural: "CENTAVOS",
                letrasMonedaCentavoSingular: "CENTAVO"
            };

            if (data.centavos > 0) {
                data.letrasCentavos = "CON " + (function () {
                    if (data.centavos == 1)
                        return Millones(data.centavos) + " " + data.letrasMonedaCentavoSingular;
                    else
                        return Millones(data.centavos) + " " + data.letrasMonedaCentavoPlural;
                })();
            };

            if (data.enteros == 0)
                return "CERO " + data.letrasMonedaPlural + " " + data.letrasCentavos;
            if (data.enteros == 1)
                return Millones(data.enteros) + " " + data.letrasMonedaSingular + " " + data.letrasCentavos;
            else
                return Millones(data.enteros) + " " + data.letrasMonedaPlural + " " + data.letrasCentavos;
        }//NumeroALetras()

        $scope.asignarfecha1 = function () {
            var now = new Date();

            var day = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1)).slice(-2);

            var today = now.getFullYear() + "-" + (month) + "-" + (day);

            $('#fecha_ing').val(today);
        }
        $scope.nuevo = function () {
            $scope.cancelar().then(function (s) {
                $scope.fecha = new Date();
                $scope.grillaforma_pago = [];
            });

        }
        $scope.cancelar = function () {
            var defender = $q.defer();
            var promise = defender.promise;
            $scope.fecha = undefined;
            $scope.fecha = null;
            $scope.swbuscar = true;
            $scope.json = {};
            $scope.json.tipo_pac = 1;
            $scope.json.tipo_historia = 3;
            $scope.json.detalle = '';
            $scope.cambio = '';
            $scope.pagado = '';
            $scope.asignarfecha1();
            defender.resolve(true);
            return promise;
        }
        $scope.guardar = function () {
            var defender = $q.defer();
            var promise = defender.promise;
            $scope.json.pagado = $scope.json.total;
            if (!$scope.caja.$valid) {
                var cr = "Campos requeridos no diligenciados:";
                if ($scope.caja.$error.required) {
                    $scope.caja.$error.required.forEach(function (item, index) {
                        cr += "\n - " + item.$name;
                    });
                    alert(cr);
                } else {
                    alert("Los Datos No Fueron Diligenciados Correctamente");
                }
                defender.resolve(false);

                return promise;
            } else {
                $timeout(function () {
                    try {
                        var rs = false;
                        Service.getCrud({ json: angular.toJson($scope.json), codigo_usuario: $scope.auxuser, year: $scope.year, listformapago: angular.toJson($scope.grillaforma_pago) }, "/Salud2.svc/CreateCaja01").then(function (d) {
                            rs = JSON.parse(d.data);
                            if (rs[0] == '?') {
                                alert(rs);
                            }
                            if (rs == "cuenta") {
                                alert("La forma pago no registra una cuenta credito asociada");
                            }
                            if (rs == "cuentadb") {
                                alert("La forma pago no registra una cuenta debito asociada");
                            }
                            defender.resolve(rs);
                            $scope.disabledbtn.imprimir = false;

                        });
                    } catch (e) {
                        //console.log(e);
                        defender.reject(e);
                    }
                });
            }
            return promise;
        }
        $scope.actualizar = function () {
            var defender = $q.defer();
            var promise = defender.promise;
            $scope.json.pagado = $scope.json.total;
            if (!$scope.caja.$valid) {
                var cr = "Campos requeridos no diligenciados:";
                if ($scope.caja.$error.required) {
                    $scope.caja.$error.required.forEach(function (item, index) {
                        cr += "\n - " + item.$name;
                    });
                    alert(cr);
                } else {
                    alert("Los Datos No Fueron Diligenciados Correctamente");
                }
                defender.resolve(false);

                return promise;
            } else {
                $timeout(function () {
                    try {
                        var rs = false;
                        $scope.json.fecha = ssDate.militodate($scope.json.fecha);
                        Service.getCrud({ json: angular.toJson($scope.json), numero: $scope.json.numero, codigo_usuario: $scope.auxuser, year: $scope.year, listformapago: angular.toJson($scope.grillaforma_pago) }, "/Salud2.svc/UpdateCaja01").then(function (d) {
                            rs = JSON.parse(d.data);
                            if (rs[0] == '?') {
                                alert(rs);
                            }
                            if (rs == "cuenta") {
                                alert("La forma pago no registra una cuenta credito asociada");
                            }
                            if (rs == "cuentadb") {
                                alert("La forma pago no registra una cuenta debito asociada");
                            }
                            defender.resolve(rs);
                            $scope.disabledbtn.imprimir = false;

                        });
                    } catch (e) {
                        //console.log(e);
                        defender.reject(e);
                    }
                });
            }
            return promise;
        }
        $scope.elim = function () {
            $scope.openModalDetalle = false;
            var defender = $q.defer();
            var promise = defender.promise;
            Service.getCrud({ fuente: $scope.json.fuente, numero: $scope.json.numero, codigo_usuario: $scope.cod_user }, "/Salud2.svc/DeleteCaja01").then(function (d) {
                var rs = JSON.parse(d.data);
                if(rs == true){
                    var currbtn = angular.element(document.getElementById('caja-borrar'));
                    $timeout(function () {
                        currbtn.triggerHandler("click");
                        $('#caja-borrar').click();
                    });
                    $scope.grillaforma_pago = [];
                }
                defender.resolve(rs);
                $scope.disabledbtn.imprimir = true;
            });
            return promise;
        }
        $scope.eliminar = function(){
            $scope.openModalPago = false;
            var defender = $q.defer();
            var promise = defender.promise;
            defender.resolve(true);
            return promise;
        }
        $scope.editar = function () {
            $scope.sh_edt = true;
        }
        $scope.preeliminar = function(){
            //alert("ELIMINAR O CANCELAR");
            $scope.openModalPago = true;
        }
        $scope.imprimir = function () {
            $('#menu').addClass("ss-nav-inactive");
            $('#wrapper').addClass("ss-full-view");
            window.print();
        }
        $scope.anular = function(){
            $scope.openModalPago = false;
            $scope.openModalDetalle = true;
            $('#det2').focus();
        }
        $scope.p = function(e){
            console.log(e);
            if(e.keyCode == 13){
                $scope.openModalDetalle = false;
                var defender = $q.defer();
                var promise = defender.promise;
                var rs = false;
                $scope.json.fecha = ssDate.militodate($scope.json.fecha);
                Service.getCrud({ json: angular.toJson($scope.json), codigo_usuario: $scope.auxuser, detalle2: $scope.json.detalle2 }, "/Salud2.svc/UpdateAnuladoCaja").then(function (d) {
                    rs = JSON.parse(d.data);
                    if (rs[0] == '?') {
                        alert(rs);
                    }
                    if(rs == true){
                        var currbtn = angular.element(document.getElementById('caja-borrar'));
                        $timeout(function () {
                            currbtn.triggerHandler("click");
                            $('#caja-borrar').click();
                        });
                        $scope.grillaforma_pago = [];
                    }
                    defender.resolve(rs);
                });
                return promise;
            }
        }
        $scope.dataCaja01 = {
            id: 'brCitas',
            component: [{ id: 'caja-buscar', type: 'btn' }],
            table: 'caja01 as c',
            column: [
                { name: 'c.historia', as: 'Historia', visible: true },
                { name: 'c.año', as: 'Año', visible: true },
                { name: 'c.fuente', as: 'Fuente', visible: true },
                { name: 'c.numero', as: 'Numero', visible: true },
                { name: 'p.codigo', as: 'Codigo', visible: true },
                { name: 'p.nombre', as: 'Nombre', visible: true },
                { name: 'p.ppellido', as: 'Apellido', visible: true },
                { name: 'c.fecha', as: 'Fecha', visible: true },
            ],
            inner_join: [],
            left_join: [{ table: 'paciente  as p', on: 'p.codigo=c.codigo', join: '' }],
            where: [
            ],
            orderby: ['c.historia desc'],
            groupby: false,
            title: 'Buscar caja'

        };
        $scope.rsCaja01 = function (v) {
            var rss = JSON.parse(v);
            if (rss.Historia != '') {
                $scope.grillaforma_pago = [];
                $scope.sh_edt = false;
                Service.getCrud({ fuente: rss.Fuente, numero: rss.Numero }, "/Salud2.svc/ReadCaja01").then(function (d) {
                    var rs = JSON.parse(d.data);
                    if (rs[0] == '?') {
                        alert(rs);
                    } else if (rs != null && rs != []) {
                        $scope.key.value = rs.historia+"";
                        $scope.json = rs;
                        $scope.json.total = rs.total;
                        $('#brAtencion-clave').val(rs.historia+"");
                        $('#brAtencion-valor').val(rs.año+"");
                        Service.getCrud({ fuente: rs.fuente, numero: rs.numero, idweb: rs.id_web }, "/Salud2.svc/ReadTipoFormaP").then(function (d) {
                            var rs3 = JSON.parse(d.data);
                            if(rs3[0] == '?'){
                                alert(rs3);
                            }else if (rs3 != null && rs != []){
                                rs3.forEach(function(item,index){
                                    item.valorcoopago = item.valor;
                                    $scope.grillaforma_pago.push(item);
                                    $scope.json.forma_pago = item.forma_pago.trim();
                                    $scope.json.valorcoopago = item.valor;
                                });
                            }
                        });
                        if (rs.fecha != null) {
                            $scope.fecha = ssDate.militodate(rs.fecha);
                        }
                        console.log(rs);
                        $scope.json.tipo_rec = rs.tipo_rec.trim();
                        $scope.json.valorcoopago = rs.pagado+"";
                        $scope.swbuscar = false;
                        $scope.traerinfo(rs.historia, rs.año);
                        $scope.cambio ='0';
                        if ($scope.json.codigo == '888888') {
                            $scope.json.total = '0';
                            $scope.json.valorcoopago = '0';
                        }
                    }
                    $scope.disabledbtn.editar = false;
                    $scope.disabledbtn.imprimir = false;
                    $scope.disabledbtn.eliminar = false;

                    //$scope.key10.value = $scope.json.codigo_Med;

                });
            }
        };
        $scope.close = function(){
            $scope.openModalPago = false;
        }

    }
    function RecaudoCitaController($scope, $q, $timeout, ssDate, Service, Auth, Datos, Consultas) {
        $scope.voucher = {};
        $scope.codigo = '';
        $scope.codigoesp = '';
        $scope.grillaforma_pago = [];
        $scope.hora_aux = [];
        $scope.pagar = '';
        $scope.nivel = '';
        $scope.detalle = '';
        $scope.key3 = { value: '' };
        $scope.tel = '';
        $scope.fecha = '';
        $scope.shaut;
        $scope.requ;
        $scope.grilla = [];
        $scope.grilla_conf = [];
        $scope.grilla_servicio = [];
        $scope.anos;
        $scope.fecha = new Date();
        $scope.servi = '';
        $scope.openWaiting = false;
        $scope.grillagruposprestador=[];
        $scope.emp = '';
        $scope.tarifaa = '';
        $scope.tarifa = '';
        $scope.empres = '';
        $scope.autorizacion = '';
        $scope.admin = '';
        $scope.citas = '';
        $scope.cambioemp = true;
        $scope.cantidad = '1';
        $scope.emp_new = '';
        $scope.emp_old = '';
        $scope.buscaprest_serv=false;
        $scope.imprimirrecibocaja = false;
        $scope.imprimirgrilla = false;
        $scope.openModal = false;
        $scope.openModalDos = false;
        $scope.openModalPago = false;
        $('#costo_disab').prop("disabled", true);
        $scope.citas_ant = '';
        $scope.cod_user = Auth.getUserObj().usuario1;
        $scope.auxuser = Auth.getUserObj().usuario1;
        $scope.json = {};
        $scope.json2 = {};
        $scope.json.cod_diag = '';
        $scope.json.nom_diag = '';
        $scope.openModalPend = false;
        $scope.imprimirvoucher = false;
        $scope.causa_ext = '13- ENFERMEDAD GENERAL';
        

        $scope.key = { value: '' };
        $scope.key2 = { value: '' };
        $scope.abrirventana = function () {
            var currentButton = angular.element(document.getElementById('menucerrar'));
            $timeout(function () {
                currentButton.triggerHandler("click");
            });
        }
        $scope.abrirventana();
        $scope.datapanelpacienteprint = { codigo: '', cod_admin: '' };
        $scope.json.total = 0;
        Service.getCrud({ usuario: $scope.auxuser, modulo: 'Agenda Medica', ventana: '11' }, "/Generic.svc/ReadNivel").then(function (d) {
            var j = JSON.parse(d.data);
            var c, r, u, d;
            c = j.substr(0, 1);
            r = j.substr(1, 1);
            u = j.substr(2, 1);
            d = j.substr(3, 1);

            if (c == 1) {
                $('#procesar').attr("disabled", false);
                $('#cancelar').attr("disabled", false);
                $('#cambiar').attr("disabled", false);

            } else {
                $('#procesar').attr("disabled", true);
                $('#cancelar').attr("disabled", true);
                $('#cambiar').attr("disabled", true);
            }
        });
        Service.getCrud({ usuario: $scope.auxuser, modulo: 'PERMISOS ESPECIALES', ventana: '13' }, "/Generic.svc/ReadNivel").then(function (d) {
            var j = JSON.parse(d.data);
            var c, r, u, d;
            c = j.substr(0, 1);
            r = j.substr(1, 1);
            u = j.substr(2, 1);
            d = j.substr(3, 1);

            if (c == 1) {
                $('#costo_disab').attr("disabled", false);

            } else {
                $('#costo_disab').attr("disabled", true);
            }
        });
        $scope.imprimir1 = false;
        $scope.imprimir2 = false;
        $scope.fecha_nac = function (pac) {
            if (pac != undefined) {
                var x = "select convert(varchar,fecha_nac,111) as Fecha_Nac  from usuarios where docume ='" + pac + "'";
                Consultas.read(x).then(function (d) {
                    var j = d[0];
                    if (j != undefined) {
                        $scope.f1 = j.Fecha_Nac.split(" ");
                        $scope.fech = $scope.f1[0];
                        $scope.f2 = $scope.fech.split("/");
                        $scope.f3 = $scope.f2[0] + $scope.f2[1] + $scope.f2[2];
                        var z = "EXEC [dbo].[calcularedad] @fecha ='" + $scope.f3 + "'";
                        Consultas.read(z).then(function (d) {
                            var a = d[0];
                            if (a != undefined) {
                                var s = "select tipo_afi as Afi,parentesco as Paren from usuarios where docume ='" + pac + "'";
                                Consultas.read(s).then(function (d) {
                                    var f = d[0];
                                    if (f != undefined) {
                                        if (f.Afi.trim() == '2' && parseInt(a.edad) > 24 && parseInt(f.Paren) == 2) {
                                            alert("FAVOR ACERCARSE AL AREA DE AFILIACIONES POR EDAD MAYOR A 25 AÑOS");
                                            $scope.json.codigo = '';
                                            $scope.codpac = '';
                                            $scope.nombrepaciente = '';
                                            $scope.grilla = [];
                                            $scope.citas_ant = [];
                                            $('#brp-clave').val('');
                                            $('#brp-valor').val('');
                                            $scope.closePac();
                                        }
                                    }
                                });
                            }
                        });
                    } else {
                        var y = "select convert(varchar,fecha_nac,111) as Fecha_Nac  from paciente where codigo ='" + pac + "'";
                        Consultas.read(y).then(function (d) {
                            var c = d[0];
                            if (c != undefined) {
                                $scope.f1 = c.Fecha_Nac.split(" ");
                                $scope.fech = $scope.f1[0];
                                $scope.f2 = $scope.fech.split("/");
                                $scope.f3 = $scope.f2[0] + $scope.f2[1] + $scope.f2[2];
                                var l = "EXEC [dbo].[calcularedad] @fecha ='" + $scope.f3 + "'";
                                Consultas.read(l).then(function (d) {
                                    var n = d[0];
                                    if (n != undefined) {
                                        var s = "select contribucion as Afi  from paciente where codigo ='" + pac + "'";
                                        Consultas.read(s).then(function (d) {
                                            var f = d[0];
                                            if (f != undefined) {
                                                if (f.Afi.trim() == '2' && parseInt(n.edad) > 24) {
                                                    alert("FAVOR ACERCARSE AL AREA DE AFILIACIONES POR EDAD MAYOR A 25 AÑOS");
                                                    $scope.json.codigo = '';
                                                    $scope.codpac = '';
                                                    $scope.nombrepaciente = '';
                                                    $scope.grilla = [];
                                                    $scope.citas_ant = [];
                                                    $('#brp-clave').val('');
                                                    $('#brp-valor').val('');
                                                    $scope.closePac();
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                });
            }
        }
        $scope.show_fac_venta_pivi = false;
        $scope.imprimirrecibocaja = false;
        $scope.openModalPac_update = false;
        $scope.openModalpyp = false;
        Service.getCrud({}, "/Salud.svc/Nom_Sistema").then(function (d) {
            $scope.sistema = JSON.parse(d.data);
        });
        /*-------------------------------BUSQUEDA-----------------------------------------*/
        /*BUSQUEDA RAPIDA PACIENTE*/
        $scope.dataPaciente = {
            id: 'brPaciente',
            component: [{ id: 'brp', type: '' }],
            table: 'paciente',
            column: [
                { name: 'CODIGO', as: "'No. Documento'", visible: true },                                
                { name: 'PPELLIDO', as: "'1er Apellido'", visible: true },
                { name: 'SAPELLIDO', as: "'2do Apellido'", visible: true },
                { name: 'NOMBRE', as: "'1er Nombre'", visible: true },
                { name: 'SNOMBRE', as: "'2do Nombre'", visible: true },
                { name: 'nivel_afil', as: "'Nivel'", visible: false },
                { name: 'telefono', as: "'Telefono'", visible: false },
            ],
            where: {},
            groupby: false,
            orderby: ['CODIGO desc', 'PPELLIDO desc'],
            title: 'Buscar pacientes',
            required: false
        };
        $scope.rsPaciente = function (rs) {
            $scope.openModal = false;
            $scope.imprimir1 = false;
            $scope.imprimir2 = false;
            $scope.json.sisfact = 1;            
            $scope.admitir = 1;        
            $scope.grilla_servicio = [];
            $scope.documento1='';
            $scope.documento2='';
            $scope.json.forma_pago = '';
            $scope.voucher.Medico = "";
            var j = JSON.parse(rs);
            if (!jQuery.isEmptyObject(j)) {
                $scope.grillaforma_pago = [];
                $scope.json.codigo = j["No. Documento"];
                $scope.auxiliarcodigo = j["No. Documento"];
                $scope.codpac = j["No. Documento"];
                $scope.nombrepaciente = j["1er Nombre"] + " " + j["2do Nombre"] + " " + j["1er Apellido"] + " " + j["2do Apellido"];
                $scope.nivel = j["Nivel"];
                $scope.tel = j["Telefono"];
                $scope.causa_ext = '13- ENFERMEDAD GENERAL';
                if ($scope.nombrepaciente != undefined && Datos.info.estado != true) {
                    $scope.openModalPac_update = true;
                    if ($scope.sistema.nit.trim() == '800204153') {
                        console.log("entro");
                        $scope.open_Pac_update();
                    }
                }
                $scope.json.tipo_diag = 1;
                Datos.info = {};
            }

        }
        /*BUSQUEDA RAPIDA SERVICIOS*/
        $scope.dataServicios = {
            id: 'brServicios',
            component: [{ id: 'brs', type: '' }],
            table: 'procedimientos AS P',
            column: [
                { name: 'p.CODIGO', as: "Codigo", visible: true },
                { name: 'p.NOMBRE', as: "Nombre", visible: true },
                { name: 'p.TARIFA', as: "Tarifa", visible: true },
                { name: 'p.grupo_fact', as: "Grupo", visible: false },
                { name: 'p.coopago', as: 'Coopago', visible: false }
            ],
            where: {},
            inner_join: [],
            left_join: [],
            groupby: true,
            orderby: [],
            exist: [{ table: 'paquetes', campo: 'codigo,nombre', where1: 'codigo', where2: 'nombre', where3: 'codigo', where4: 'codigo', where5: 'codigo' }],
            title: 'Buscar Servicios'
        };
        $scope.rsServicios = function (rs) {
            var j = JSON.parse(rs);
            if (!jQuery.isEmptyObject(j)) {
                console.log(j);
                $scope.coopaux = j["Coopago"];
                $scope.codigoesp = j["Codigo"];
                $scope.poc = j["Codigo"];
                $scope.nombreserv = j["Nombre"];
                $scope.tarifa = j["Tarifa"];
                $scope.t = j["Tarifa"];
                $scope.json.cod_proc = $scope.codigoesp;
                $scope.json.grupo = j["Grupo"];
                console.log($scope.json);
                console.log($scope.poc);
                if ($scope.json.cod_proc != null) {
                    $scope.calc();
                }
            }
        }
        $scope.add_proce = function () {
            console.log($scope.json);
            console.log($scope.poc);            
            var paquete = 0;
            if ($scope.j.asistencialpyp == 2) {
                if ($scope.json.cod_programapyp != undefined && $scope.json.cod_programapyp != null) {
                    if ($scope.json.cod_programapyp.trim() != '') {
                        Service.getCrud({ cod_proc: $scope.codigoesp, cod_admin: $scope.admin, cod_programa: $scope.json.cod_programapyp }, "/Admision.svc/validar_matrizpyp").then(function (d) {
                            var rs = JSON.parse(d.data);
                            if (rs == false) {
                                //alert("EL PROCEDIMIENTO NO ESTA DISPONIBLE PARA ESTE PROGRAMA PYP");
                                $scope.openModalpyp = true;

                            } else {
                                $scope.savechangepyp();
                                $timeout(function () {                                    
                                    Service.getCrud({ codigo: $scope.json.cod_proc, cod_admin: $scope.admin }, "/Salud.svc/ValidarPaquete").then(function (d) {
                                        var resultado = JSON.parse(d.data);
                                        if (resultado == false) {
                                            if ($scope.json.cod_proc != null && $scope.json.cod_proc != undefined && $scope.json.cod_proc != "") {
                                                console.log("WTF" + $scope.cantidad);
                                                $scope.grilla_servicio.push({ codigo: $scope.json.cod_proc, nombreserv: $scope.nombreserv, grupo: $scope.json.grupo, valor: $scope.json.total, cantidad: $scope.cantidad, codproc: $scope.json.cod_proc  });
                                                $scope.procesoprestador();
                                                $('#brs-clave').val('');
                                                $('#brs-valor').val('');
                                                //$scope.json.cod_proc = "";
                                            }
                                        } else {
                                            resultado.forEach(function (item, index) {
                                                paquete += item.valor;
                                                console.log(paquete);
                                                $scope.tarifa = item.tarifa;
                                                $scope.json.grupo = item.grupo_fact;
                                                $scope.grilla_servicio.push({ codigo: item.codigo, nombreserv: item.nombre, grupo: item.grupo_fact, valor: item.valor, cantidad: $scope.cantidad, codproc: $scope.json.cod_proc });
                                                $('#brs-clave').val('');
                                                $('#brs-valor').val('');
                                                $scope.json.total = paquete;
                                            });
                                            $scope.calc();
                                        }
                                    });
                                    console.log($scope.grilla_servicio);
                                }, 500);
                            }
                        });
                    } else {
                        alert("SELECCIONE EL PROGRAMA DE PYP");
                        $('#prog_pyp').focus();
                    }
                }
            } else {                
                Service.getCrud({ codigo: $scope.json.cod_proc, cod_admin: $scope.admin }, "/Salud.svc/ValidarPaquete").then(function (d) {
                    var resultado = JSON.parse(d.data);
                    if (resultado == false) {
                        if ($scope.json.cod_proc != null && $scope.json.cod_proc != undefined && $scope.json.cod_proc != "") {
                            $scope.grilla_servicio.push({ codigo: $scope.json.cod_proc, nombreserv: $scope.nombreserv, grupo: $scope.json.grupo, valor: $scope.json.total, cantidad: $scope.cantidad, codproc: $scope.json.cod_proc });
                            $scope.procesoprestador();
                            $('#brs-clave').val('');
                            $('#brs-valor').val('');
                           // $scope.json.cod_proc = "";
                        }
                    } else {
                        resultado.forEach(function (item, index) {
                            paquete += item.valor;
                            console.log(paquete);
                            $scope.tarifa = item.tarifa;
                            $scope.json.grupo = item.grupo_fact;
                            $scope.grilla_servicio.push({ codigo: item.codigo, nombreserv: item.nombre, grupo: item.grupo_fact, valor: item.valor, codproc: $scope.json.cod_proc });
                            $('#brs-clave').val('');
                            $('#brs-valor').val('');
                            $scope.json.total = paquete;
                        });
                        $scope.calc();
                    }
                });
            }
            if ($scope.coopaux == '3' || $scope.coopaux == 3) {
                console.log("entro aqui 3");

                $scope.json.coopago = 0;
            }
            if ($scope.coopaux == '1' || $scope.coopaux == 1) {
                console.log("entro aqui 1");
                $scope.json.coopago = $scope.json.coopago;
                //$scope.calcularcoopago();
            }
            if ($scope.coopaux == '2' || $scope.coopaux == 2) {
                console.log("entro aqui 2");
                //console.log("entro");
                $scope.calcularcoopago();
                //$scope.json.total = 5200
            }
        }


        $scope.closeprestadorserv = function () {
            $scope.buscaprest_serv = false;
        }
        $scope.clickprestador = function (item) {
            console.log(item);
            if (item != null) {
                $scope.buscaprest_serv = false;
                if (item.unifact == '1' || item.unifact == 1) {
                    $scope.json.prestador = item.codigo;
                    $scope.json.nombreprestador = item.nombre;
                    if (item.tipo_desc == '1' || item.tipo_desc == 1) {
                        var sub = parseFloat($scope.json.total) * (parseFloat(item.porc) / 100);
                        $scope.json.costo = parseFloat($scope.json.valor) + sub;                        
                    } else if (item.tipo_desc == '2' || item.tipo_desc == 2) {
                        var sub = parseFloat($scope.json.total) * (parseFloat(item.porc) / 100);
                        $scope.json.costo = parseFloat($scope.json.total) - sub;
                    } else if (item.tipo_desc == '3' || item.tipo_desc == 3) {
                        $scope.json.costo = $scope.json.total;
                    } else {
                        alert("No se pudo calcular el costo");
                    }
                } else {
                    if ($scope.tarifa.trim() == item.tarifa.trim()) {
                        console.log("TARIFAS IGUALES");
                        $scope.json.prestador = item.codigo;
                        $scope.json.nombreprestador = item.nombre;
                        if (item.tipo_desc == '1' || item.tipo_desc == 1) {
                            var sub = parseFloat($scope.json.total) * (parseFloat(item.porc) / 100);
                            $scope.json.costo = parseFloat($scope.json.valor) + sub;
                        } else if (item.tipo_desc == '2' || item.tipo_desc == 2) {
                            var sub = parseFloat($scope.json.total) * (parseFloat(item.porc) / 100);
                            $scope.json.costo = parseFloat($scope.json.total) - sub;
                        } else if (item.tipo_desc == '3' || item.tipo_desc == 3) {
                            $scope.json.costo = $scope.json.total;
                        } else {
                            alert("No se pudo calcular el costo");
                        }
                    } else {
                        var queryequivalentes = "select tarifa2,codigo2 from equivalentes where codigo1='" + $scope.json.cod_proc + "' and tarifa1='" + $scope.tarifa + "' and tarifa2='" + item.tarifa + "'";
                        Consultas.read(queryequivalentes).then(function (d) {
                            console.log(d);
                            if (d.length > 0) {
                                console.log("si tiene");
                                var queryproc = "select codigo,nombre,valor,tarifa from procedimientos where tarifa='" + d[0].tarifa2 + "' and codigo='" + d[0].codigo2 + "'";
                                Consultas.read(queryproc).then(function (p) {
                                    console.log(p);
                                    if (p.length > 0) {
                                        console.log("existe proc");
                                        $scope.json2.prestador = item.codigo;
                                        $scope.json2.nombreprestador = item.nombre;
                                        $scope.json.prestador = item.codigo;
                                        if (item.tipo_desc == '1' || item.tipo_desc == 1) {
                                            var sub = parseFloat(p[0].valor) * (parseFloat(item.porc_tar) / 100);
                                            $scope.json.costo = parseFloat($scope.json.total) + sub;
                                        } else if (item.tipo_desc == '2' || item.tipo_desc == 2) {
                                            var sub = parseFloat(p[0].valor) * (parseFloat(item.porc_tar) / 100);
                                            $scope.json.costo = parseFloat($scope.json.total) - sub;
                                        } else if (item.tipo_desc == '3' || item.tipo_desc == 3) {
                                            $scope.json.costo = parseFloat(p[0].valor);
                                        } else {
                                            alert("No se pudo calcular el costo");
                                        }
                                    }else {
                                        console.log("no existe proc");
                                        alert("El servicio no tiene homologo, seleccionar un servicio");                                        
                                    }
                                })
                            } else {
                                console.log("no teine nd");
                                alert("La tarifa de compra es diferente a la tarifa de venta, seleccionar un servicio");                                
                            }
                        })
                    }
                }
            }
        }

        $scope.procesoprestador = function () {
            Service.getCrud({ Grupo: $scope.json.grupo }, "/Admision.svc/Read_Grupo_Prest").then(function (d) {
                var x = JSON.parse(d.data);
                console.log(x);
                var item={};
                if (x.length > 0) {
                    if (x.length == 1) {
                        x.forEach(function (item, index) {
                            $scope.json.prestador = item.id_prestador;   
                            $scope.json.nombreprestador = item.nombre;                         
                        })
                        var queryp="select * from convterceros where codigo='"+$scope.json.prestador+"'";                        
                        Consultas.read(queryp).then(function(d){
                            item =d[0];         
                            console.log(item);                                                        
                        if(d.length>0){
                            if (item.unifact == '1' || item.unifact == 1) {
                                console.log("existe proc");
                                $scope.json.prestador = item.codigo;
                                $scope.json.nombreprestador = item.nombre;
                                if (item.tipo_desc == '1' || item.tipo_desc == 1) {
                                    var sub = parseFloat($scope.json.total) * (parseFloat(item.porc_tar) / 100);
                                    $scope.json.costo = parseFloat($scope.json.valor) + sub;
                                } else if (item.tipo_desc == '2' || item.tipo_desc == 2) {
                                    var sub = parseFloat($scope.json.total) * (parseFloat(item.porc_tar) / 100);
                                    $scope.json.costo = parseFloat($scope.json.total) - sub;
                                } else if (item.tipo_desc == '3' || item.tipo_desc == 3) {
                                    $scope.json.costo = $scope.json.total;
                                } else {
                                    alert("No se pudo calcular el costo");
                                }
                                console.log($scope.json.costo);
                            } else {
                                if ($scope.tarifa.trim() == item.tarifa.trim()) {
                                    console.log("TARIFAS IGUALES");
                                    $scope.json.prestador = item.codigo;
                                    $scope.json.nombreprestador = item.nombre;          
                                    if (item.tipo_desc == '1' || item.tipo_desc == 1) {
                                        var sub = parseFloat($scope.json.total) * (parseFloat(item.porc_tar) / 100);
                                        $scope.json.costo = parseFloat($scope.json.total) + sub;
                                    } else if (item.tipo_desc == '2' || item.tipo_desc == 2) {
                                        var sub = parseFloat($scope.json.total) * (parseFloat(item.porc_tar) / 100);
                                        $scope.json.costo = parseFloat($scope.json.total) - sub;
                                    } else if (item.tipo_desc == '3' || item.tipo_desc == 3) {
                                        $scope.json.costo = $scope.json.total;
                                    } else {
                                        alert("No se pudo calcular el costo");
                                    }                         
                                } else {
                                    console.log("TARIFAS DIFERENTES");                                    
                                    var queryequivalentes = "select tarifa2,codigo2 from equivalentes where codigo1='" + $scope.json.cod_proc + "' and tarifa1='" + $scope.tarifa + "' and tarifa2='" + item.tarifa + "'";
                                    Consultas.read(queryequivalentes).then(function (d) {
                                        console.log(d);
                                        if (d.length > 0) {
                                            console.log("si tiene");
                                            var queryproc = "select codigo,nombre,valor,tarifa from procedimientos where tarifa='" + d[0].tarifa2 + "' and codigo='" + d[0].codigo2 + "'";
                                            Consultas.read(queryproc).then(function (p) {
                                                console.log(p);
                                                if (p.length > 0) {
                                                    console.log("existe proc");
                                                    $scope.json.prestador = item.codigo;
                                                    $scope.json.nombreprestador = item.nombre;
                                                    console.log(item.tipo_desc);
                                                    if (item.tipo_desc == '1' || item.tipo_desc == 1) {                                                        
                                                        var sub = parseFloat(p[0].valor) * (parseFloat(item.porc_tar) / 100);
                                                        $scope.json.costo = parseFloat($scope.json.total) + sub;
                                                    } else if (item.tipo_desc == '2' || item.tipo_desc == 2) {
                                                        var sub = parseFloat(p[0].valor) * (parseFloat(item.porc_tar) / 100);
                                                        $scope.json.costo = parseFloat($scope.json.total) - sub;
                                                    } else if (item.tipo_desc == '3' || item.tipo_desc == 3) {
                                                        $scope.json.costo = parseFloat(p[0].valor);
                                                    } else {
                                                        alert("No se pudo calcular el costo");
                                                    }


                                                } else {
                                                    console.log("no existe proc");
                                                    alert("El servicio no tiene homologo, seleccionar un servicio");
                                                    $scope.porc_equivalente = item.porc_tar;
                                                    $scope.tipo_desc_equivalente = item.tipo_desc;
                                                    $timeout(function () {
                                                        $scope.json.prestador = item.codigo;
                                                        $scope.json.nombreprestador = item.nombre;
                                                        //$('#buscar_pro2').click();
                                                    }, 300)
                                                }
                                            })
                                        } else {
                                            console.log("no teine nd");
                                            alert("La tarifa de compra es diferente a la tarifa de venta, seleccionar un servicio");
                                            $scope.porc_equivalente = item.porc_tar;
                                            $scope.tipo_desc_equivalente = item.tipo_desc;
                                            $timeout(function () {
                                                $scope.json2.prestador = item.codigo;
                                                $scope.json2.nombreprestador = item.nombre;
                                                $('#buscar_pro2').click();
                                            }, 300)
                                        }
                                    })
                                }
                            }
                        }else{
                            
                        }                            
                        })                                                
                    } else {
                        console.log("entre aqui");
                        //$scope.readprestador();
                        var querytercero = "";
                        querytercero += "select";
                        querytercero += " g.id_prestador as codigo, ";
                        querytercero += " g.convenio as convenio ,";
                        querytercero += " g.grupo as grupo,";
                        querytercero += " c.nombre as nombre,";
                        querytercero += " c.porc_tar as porc,";
                        querytercero += " c.tarifa as tarifa,";
                        querytercero += " c.unifact as unifact, ";
                        querytercero += " c.tipo_desc as tipo_desc ";
                        querytercero += " from gruposprestador g ";
                        querytercero += " inner join convterceros c on c.codigo=g.id_prestador";
                        querytercero += " where g.grupo='" + x[0].grupo + "'";
                        querytercero += "";
                        querytercero += "";
                        Consultas.read(querytercero).then(function (d) {
                            $scope.grillagruposprestador = d;
                        })
                        $scope.buscaprest_serv = true;
                    }
                }
            });
            
        }

        $scope.closePrest = function () {
            $scope.openModalPrestador = false;
        }

        function getCitas() {

            Service.getCrud({ codigo: $scope.json.codigo }, "/Salud.svc/CitasAsignadas").then(function (d) {
                $scope.citas = JSON.parse(d.data);
            });
        }
        $scope.calcularcoopago = function () {
            var rs = 0;
            console.log($scope.nivel);
            if ($scope.nivel == 1 || $scope.nivel == '1') {
                var querynivel = "select sub_n1 from admin where codigo='" + $scope.admin + "'";
                Consultas.read(querynivel).then(function (d) {
                    var porc = d[0];
                    rs = $scope.json.total * (parseFloat(porc.sub_n1) / 100);
                    console.log(rs);
                    $scope.json.coopago = rs;
                    
                    //rs=$scope.json.total*
                })
            }
            if ($scope.nivel == 2 || $scope.nivel == '2') {
                var querynivel = "select sub_n2 from admin where codigo='" + $scope.admin + "'";
                Consultas.read(querynivel).then(function (d) {
                    var porc = d[0];
                    rs = $scope.json.total * (parseFloat(porc.sub_n2) / 100);
                    $scope.json.coopago = rs;
                    
                })
            }
            if ($scope.nivel == 3 || $scope.nivel == '3') {
                var querynivel = "select sub_n3 from admin where codigo='" + $scope.admin + "'";
                Consultas.read(querynivel).then(function (d) {
                    var porc = d[0];
                    rs = $scope.json.total * (parseFloat(porc.sub_n3) / 100);
                    $scope.json.coopago = rs;                    
                })
            }
            return rs
        }
        $scope.deleteGrilla = function (i) {
            //$scope.grilla.push(grilla[i]);
            $scope.grilla_servicio.splice(i, 1);
        }

        $scope.deleteforma_pago = function (x) {
            console.log(x);
            $scope.grillaforma_pago.splice(x, 1);
        }

        $scope.limpiarservicio = function () {
            $scope.grilla_servicio = [];
            $scope.grilla = [];
            $scope.grilla = '';
            Service.getCrud({ codigo: $scope.json.codigo, fecha_cons: $scope.fecha }, "/Salud.svc/GetCitas_codigo_fecha").then(function (rs) {
                $scope.grilla = JSON.parse(rs.data);
            });
            $timeout(function () {
                $('#brs-clave').val('');
                $('#brs-valor').val('');
                $scope.json.cod_proc = undefined;
                $scope.json.grupo = undefined;
                $scope.json.total = '';
                $scope.json.cod_programapyp = undefined;
                $scope.admin = '';
                $scope.tarifa = '';
                $scope.detalle = '';

            }, 50);
        }
        /*BARRA*/
        $scope.disabledbtn = { buscar: true, imprimir: true, nuevo: false, editar: true, eliminar: true };
        $scope.showbtn = { buscar: false, imprimir: false, nuevo: true, editar: false, eliminar: false };

        /*--------------------------CRUDS-------------------------------------------------------------*/

        /*BUSQUEDAS RAPIDAS*/
        /*Diagnosticos 1*/
        $scope.datadiag1 = {
            id: 'brDiagnostico1',
            component: [{ id: 'brm-diag1', type: '' }],
            table: 'diagnosticos',
            column: [
                { name: 'RTRIM(LTRIM(CODIGO))', as: 'Codigo', visible: true },
                { name: 'RTRIM(LTRIM(NOMBRE))', as: 'Nombre', visible: true }
            ],
            where: {},
            groupby: false,
            orderby: ['CODIGO DESC', 'NOMBRE DESC'],
            title: 'Busqueda de Diagnosticos'
        };
        $scope.rsdiag1 = function (d) {
            var diag1 = JSON.parse(d);
            if ($scope.json.codigo != undefined && $scope.json.codigo != '') {
                if (diag1.Codigo != undefined) {
                    var myArray = diag1.Codigo.split(/(\d+)/);
                    if (myArray[0] == "S" && ($scope.causa_ext.trim() == '15- OTRA' || $scope.causa_ext.trim() == '13- ENFERMEDAD GENERAL')) {
                        $("#alertColor").css({ 'background-color': 'rgba(236,85,85,1)', 'color': 'white' });
                        $scope.mensaje = 'LA CAUSA EXTERNA NO PERTENECE AL DIAGNOSTICO';
                        $timeout(function () {
                            $('#brm-diag1-clave').val('');
                            $('#brm-diag1-valor').val('');
                            $scope.key3 = '';
                        }, 50);
                        $scope.json.cod_diag = '';
                        $scope.json.nom_diag = '';
                        $('#closePend').focus();
                        $scope.openModalPend = true;
                    } else {

                        $scope.json.cod_diag = diag1.Codigo;
                        $scope.json.nom_diag = diag1.Nombre;
                    }
                    if (myArray[0] == "T") {
                        if (parseInt(myArray[1]) <= 784 && ($scope.causa_ext.trim() == '15- OTRA' || $scope.causa_ext.trim() == '13- ENFERMEDAD GENERAL')) {
                            $("#alertColor").css({ 'background-color': 'rgba(236,85,85,1)', 'color': 'white' });
                            $scope.mensaje = 'LA CAUSA EXTERNA NO PERTENECE AL DIAGNOSTICO';
                            $timeout(function () {
                                $('#brm-diag1-clave').val('');
                                $('#brm-diag1-valor').val('');
                            }, 50);
                            $scope.json.cod_diag = '';
                            $scope.json.nom_diag = '';
                            $('#closePend').focus();
                            $scope.openModalPend = true;
                        }
                    }
                    if ($scope.json.cod_diag != '') {
                        $scope.validardiag();
                    }

                }
            } else {
                alert("DEBE SELECCIONAR EL PACIENTE");
                $timeout(function () {
                    $('#brm-diag1-clave').val('');
                    $('#brm-diag1-valor').val('');
                    $scope.key3 = '';
                }, 50);
                $scope.json.cod_diag = '';
                $scope.json.nom_diag = '';
            }
        };

        $scope.val_causa_ext = function () {
            if ($scope.json.cod_diag != undefined && $scope.json.cod_diag != '') {
                var myArray1 = $scope.json.cod_diag.split(/(\d+)/);
                if (myArray1[0] == "S" && ($scope.causa_ext.trim() == '15- OTRA' || $scope.causa_ext.trim() == '13- ENFERMEDAD GENERAL')) {
                    $("#alertColor").css({ 'background-color': 'rgba(236,85,85,1)', 'color': 'white' });
                    $scope.mensaje = 'LA CAUSA EXTERNA NO PERTENECE AL DIAGNOSTICO';
                    $timeout(function () {
                        $('#brm-diag1-clave').val('');
                        $('#brm-diag1-valor').val('');
                    }, 50);
                    $scope.json.cod_diag = '';
                    $scope.json.nom_diag = '';
                    $('#closePend').focus();
                    $scope.openModalPend = true;
                }
                if (myArray1[0] == "T") {
                    if (parseInt(myArray1[1]) <= 784 && ($scope.causa_ext.trim() == '15- OTRA' || $scope.causa_ext.trim() == '13- ENFERMEDAD GENERAL')) {
                        $("#alertColor").css({ 'background-color': 'rgba(236,85,85,1)', 'color': 'white' });
                        $scope.mensaje = 'LA CAUSA EXTERNA NO PERTENECE AL DIAGNOSTICO';
                        $timeout(function () {
                            $('#brm-diag1-clave').val('');
                            $('#brm-diag1-valor').val('');
                        }, 50);
                        $scope.json.cod_diag_rel1 = '';
                        $scope.json.nom_diag_rel1 = '';
                        $('#closePend').focus();
                        $scope.openModalPend = true;
                    }
                }
            }
        }

        $scope.validardiag = function () {
            Service.getCrud({ cod_pac: $scope.json.codigo, cod_diag: $scope.json.cod_diag }, "/Generic.svc/ValidarDiag").then(function (d) {
                var rs = JSON.parse(d.data);
                $("#alertColor").css({ 'background-color': 'rgba(236,85,85,1)', 'color': 'white' });
                $scope.mensaje = '';
                if (rs == 'NO ESTA ACTO') {
                    $scope.mensaje = 'EL PACIENTE NO SE ENCUENTRA ACTO PARA ESTE DIAGNOSTICO';
                    $scope.openModalPend = true;
                    $('#closePend').focus();
                    $timeout(function () {
                        $('#brm-diag1-clave').val('');
                        $('#brm-diag1-valor').val('');
                        $scope.key3 = '';
                        $scope.json.cod_diag = '';
                        $scope.json.nom_diag = '';
                    }, 50);
                }
                //if (rs == '1') {
                //    $scope.mensaje = 'ESTE DIAGNOSTICO NO PUEDE SER APLICADO AL PACIENTE DEBIDO AL SEXO';
                //    $scope.openModalPend = true;
                //    $('#closePend').focus();
                //    $timeout(function () {
                //        $('#brm-diag1-clave').val('');
                //        $('#brm-diag1-valor').val('');
                //        $scope.key3 = '';
                //        $scope.json.cod_diag = '';
                //        $scope.json.nom_diag = '';
                //    }, 50);
                //} else if (rs != x && rs != '1') {
                //    $scope.mensaje = 'ESTE DIAGNOSTICO NO PUEDE SER APLICADO AL PACIENTE DEBIDO SU EDAD';
                //    $scope.openModalPend = true;
                //    $('#closePend').focus();
                //    $timeout(function () {
                //        $('#brm-diag1-clave').val('');
                //        $('#brm-diag1-valor').val('');
                //        $scope.key3 = '';
                //        $scope.json.cod_diag = '';
                //        $scope.json.nom_diag = '';
                //    }, 50);
                //}
            });
        }

        $scope.closePend = function () {
            $scope.openModalPend = false;
        }

        /*----------ACTUALIZAR-----------------------*/
        $scope.add_fp = function () { 
            if ($scope.json.coopago != null && $scope.json.coopago != undefined && $scope.json.coopago != 0) { 
                if ($scope.json.valorcoopago != null && $scope.json.valorcoopago != undefined && $scope.json.valorcoopago != "") { 
                    if ($scope.json.forma_pago != null && $scope.json.forma_pago != undefined && $scope.json.forma_pago != "") { 
                        var sumador = 0; 
                        $scope.grillaforma_pago.push({ fuente: $scope.documento1, numero: $scope.documento2, forma_pago: $scope.json.forma_pago, valorcoopago: $scope.json.valorcoopago }) 
                        $scope.grillaforma_pago.forEach(function (item, index) { 
                            sumador += parseFloat(item.valorcoopago); 
                        }) 
                        $scope.json.valorcoopago = parseFloat($scope.json.coopago) - parseFloat(sumador); 
                    } else { 
                        alert("Seleccione forma de pago") 
                    } 
                } else { 
                    alert("Seleccione valor"); 
                } 
            } else { 
                alert("Seleccione el valor del coopago") 
            } 
        } 

        $scope.aceptar = function () {
            var sumador = 0;
            if (!jQuery.isEmptyObject($scope.grillaforma_pago)) {
                $scope.grillaforma_pago.forEach(function (item, index) {
                    sumador += parseFloat(item.valorcoopago);
                })
                if ($scope.json.coopago != sumador) {
                    alert("El valor de las formas de pago no concuerda con el coopago");
                } else if ($scope.json.coopago > 0) {
                    $scope.openModalPago = false;
                    $scope.actualizar2();
                }
            } else if ($scope.json.coopago == 0 || $scope.json.coopago == undefined || $scope.json.coopago == '') {
                $scope.json.coopago = 0;
                $scope.openModalPago = false;
                $scope.actualizar();
            }
            else if ($scope.json.coopago == null || $scope.json.coopago == "" || $scope.json.coopago == undefined) {
                alert("Digite un valor en el coopago, 0 si no genera coopago");
            }
            else {
                alert("Seleccione la forma de pago ");
            }
        }
        $scope.CerrarRecaudo = function () {
            $scope.openModalPago = false;
        }

        $scope.cambiarformapago = function (fp) {
            Service.getCrud({ fp: fp }, "/Salud.svc/CambiarFormapago").then(function (d) {
                var rs = JSON.parse(d.data);
                $scope.documento1 = rs.fuente;
                $scope.documento2 = rs.consecutivo;
            })
        }

        $scope.calcular = function () {
            $scope.cambio = parseInt($scope.pagado) - parseInt($scope.json.coopago);
        }

        $scope.validarccostocontrol = function () {
            var rs;
            $timeout(function () {
                $scope.grilla_servicio.forEach(function (item, index) {
                    Service.getCrud({ consulta02: item.grupo, consulta01: $scope.json.centro_costo }, "/Salud.svc/Validarccostocontrol").then(function (d) {
                        rs = JSON.parse(d.data);
                    });
                });
            });
            return rs;
        }
        $scope.validarccostocontrol2 = function () {
            if ($scope.json.centro_costo != undefined) {
                if ($scope.json.centro_costo.trim != '') {
                    Service.getCrud({ consulta02: $scope.json.grupo, consulta01: $scope.json.centro_costo }, "/Salud.svc/Validarccostocontrol").then(function (d) {
                        var rs = JSON.parse(d.data);
                        if (rs == false) {
                            alert("EL PROCEDIMIENTO NO EXISTE PARA ESTE CENTRO DE COSTO");
                            $timeout(function () {
                                $('#brs-clave').val('');
                                $('#brs-valor').val('');
                                $scope.key = '';
                                $scope.json.cod_proc = '';
                                $scope.json.grupo = '';
                                $scope.json.total = '';
                            }, 50);
                        }
                    });
                }
            }
        }
        $scope.validarserviciopyp = function () {
            if ($scope.j.asistencialpyp == 2) {
                if ($scope.json.cod_programapyp != undefined && $scope.json.cod_programapyp != null) {
                    if ($scope.json.cod_programapyp.trim() != '') {
                        Service.getCrud({ cod_proc: $scope.codigoesp, cod_admin: $scope.admin, cod_programa: $scope.json.cod_programapyp }, "/Admision.svc/validar_matrizpyp").then(function (d) {
                            var rs = JSON.parse(d.data);
                            if (rs == false) {
                                //alert("EL PROCEDIMIENTO NO ESTA DISPONIBLE PARA ESTE PROGRAMA PYP");
                                $scope.openModalpyp = true;

                            } else {
                                $scope.savechangepyp();

                            }
                        });
                    } else {
                        alert("SELECCIONE EL PROGRAMA DE PYP");
                        $scope.limpiarservicio();
                    }
                }
            }
        }


        $scope.validar = function () {
            console.log($scope.json);
            if ($scope.tarifa != undefined) {
                if ($scope.json.cod_proc != undefined) {
                    if ($scope.json.cod_proc.length > 0) {
                        Service.getCrud({ codigo: $scope.json.cod_proc, tarifa: $scope.tarifa, Grupo: $scope.json.grupo }, "/Admision.svc/Read_nom_proc").then(function (d) {
                            var j = JSON.parse(d.data);
                            console.log(j);
                            if (j == null) {
                                alert('PROCEDIMENTO NO EXISTE PARA ESTA TARIFA');
                                $scope.json.cod_proc = '';
                                $scope.json.grupo = '';
                                $scope.json.total = '';
                                $timeout(function () {
                                    $('#brs-clave').val('');
                                    $('#brs-valor').val('');
                                    $scope.key = '';
                                }, 50);
                            } else {
                                Service.getCrud({ codigo: $scope.json.cod_proc, Cod_Admin: $scope.admin, Grupo: $scope.json.grupo, Servicio: $scope.servicio }, "/Admision.svc/Read_Var_Serv").then(function (d) {
                                    var x = JSON.parse(d.data);
                                    $scope.cantidad = 1;
                                    if (x[1] == 1) {
                                        $scope.json.total = x[0];
                                        if ($scope.tipo_desc == 1) {
                                            if ($scope.unifact == 0) {
                                                console.log("aqui2");
                                                $scope.json.total = parseInt(j.valor + (j.valor * (parseInt($scope.porc_tar) / 100)));
                                            }
                                            if ($scope.unifact == 1) {
                                                console.log("aqui3");
                                                $scope.json.total = parseInt($scope.json.total + ($scope.json.total * (parseInt($scope.porc_tar) / 100)));
                                            }
                                        }
                                        if ($scope.tipo_desc == 2) {
                                            if ($scope.unifact == 0) {
                                                console.log("aqui4");
                                                $scope.json.total = parseInt(j.valor - (j.valor * (parseInt($scope.porc_tar) / 100)));
                                            }
                                            if ($scope.unifact == 1) {
                                                console.log("aqu5");
                                                $scope.json.total = parseInt($scope.json.total - ($scope.json.total * (parseInt($scope.porc_tar) / 100)));
                                            }
                                        }
                                        if ($scope.tipo_desc == 3) {
                                            if ($scope.unifact == 0) {
                                                console.log("aqu6");
                                                $scope.json.total = j.valor;
                                            }
                                            if ($scope.unifact == 1) {
                                                console.log("aqui7");
                                                $scope.json.total = $scope.json.total;
                                            }
                                        }
                                    }
                                    if (x[1] == 2) {
                                        $scope.porcentaje = x[0];
                                        if ($scope.servicio != undefined) {
                                            if ($scope.servicio == 1) {
                                                console.log("xxx");
                                                $scope.json.total = parseInt(j.valor + (j.valor * (parseInt($scope.porcentaje) / 100)));
                                            }
                                            if ($scope.servicio == 2) {
                                                console.log("xxx1");
                                                $scope.json.total = parseInt(j.valor - (j.valor * (parseInt($scope.porcentaje) / 100)));
                                            }
                                            if ($scope.servicio == 3) {
                                                console.log("xxx2");
                                                $scope.json.total = j.valor;
                                            }
                                            if ($scope.tipo_desc == 1) {
                                                if ($scope.unifact == 0) {
                                                    console.log("xxx3");
                                                    $scope.json.total = parseInt(j.valor + (j.valor * (parseInt($scope.porc_tar) / 100)));
                                                }
                                                if ($scope.unifact == 1) {
                                                    console.log("xx4");
                                                    $scope.json.total = parseInt($scope.json.total + ($scope.json.total * (parseInt($scope.porc_tar) / 100)));
                                                }
                                            }
                                            if ($scope.tipo_desc == 2) {
                                                if ($scope.unifact == 0) {
                                                    console.log("xxx5");
                                                    $scope.json.total = parseInt(j.valor - (j.valor * (parseInt($scope.porc_tar) / 100)));
                                                }
                                                if ($scope.unifact == 1) {

                                                    console.log("xxx6");
                                                    $scope.json.total = parseInt($scope.json.total - ($scope.json.total * (parseInt($scope.porc_tar) / 100)));
                                                }
                                            }
                                            if ($scope.tipo_desc == 3) {
                                                if ($scope.unifact == 0) {
                                                    console.log("xxx7");
                                                    $scope.json.total = j.valor;
                                                }
                                                if ($scope.unifact == 1) {
                                                    console.log("xxx8");
                                                    $scope.json.total = json.total;
                                                }
                                            }
                                        }
                                    }
                                    if (x[1] == 0) {
                                        if ($scope.servicio != undefined) {
                                            if ($scope.servicio == 1) {
                                                console.log("xxx10");
                                                $scope.json.total = parseInt(j.valor + (j.valor * (parseInt($scope.porcentaje) / 100)));
                                            }
                                            if ($scope.servicio == 2) {
                                                console.log("xxx9");
                                                $scope.json.total = parseInt(j.valor - (j.valor * (parseInt($scope.porcentaje) / 100)));
                                            }
                                            if ($scope.servicio == 3) {
                                                console.log("xxx11");
                                                $scope.json.total = j.valor;
                                            }
                                            if ($scope.tipo_desc == 1) {
                                                if ($scope.unifact == 0) {
                                                    console.log("xxx12");
                                                    $scope.json.total = parseInt(j.valor + (j.valor * (parseInt($scope.porc_tar) / 100)));
                                                }
                                                if ($scope.unifact == 1) {
                                                    console.log("xxx13");
                                                    $scope.json.total = parseInt($scope.json2.valor + ($scope.json2.valor * (parseInt($scope.porc_tar) / 100)));
                                                }
                                            }
                                            if ($scope.tipo_desc == 2) {
                                                if ($scope.unifact == 0) {
                                                    console.log("xxx14");
                                                    $scope.json.total = parseInt(j.valor - (j.valor * (parseInt($scope.porc_tar) / 100)));
                                                }
                                                if ($scope.unifact == 1) {
                                                    console.log("xxx15");
                                                    $scope.json.total = parseInt($scope.json.total - ($scope.json.total * (parseInt($scope.porc_tar) / 100)));
                                                }
                                            }
                                            if ($scope.tipo_desc == 3) {
                                                if ($scope.unifact == 0) {
                                                    console.log("xxx16");
                                                    $scope.json.total = j.valor;
                                                }
                                                if ($scope.unifact == 1) {
                                                    console.log("xxx17");
                                                    $scope.json.total = $scope.json.total;
                                                }
                                            }
                                        }
                                    }
                                    $scope.unifact = undefined;
                                    $scope.tipo_desc = undefined;
                                    $scope.porc_tar = undefined;
                                });
                                $scope.validarserviciopyp();
                            }
                        });
                    }
                }

            } else {
                alert('TARIFA NO ESPECIFICADA');
            }
        }

        function Unidades(num) {

            switch (num) {
                case 1: return "UN";
                case 2: return "DOS";
                case 3: return "TRES";
                case 4: return "CUATRO";
                case 5: return "CINCO";
                case 6: return "SEIS";
                case 7: return "SIETE";
                case 8: return "OCHO";
                case 9: return "NUEVE";
            }

            return "";
        }//Unidades()

        function Decenas(num) {

            var decena = Math.floor(num / 10);
            var unidad = num - (decena * 10);

            switch (decena) {
                case 1:
                    switch (unidad) {
                        case 0: return "DIEZ";
                        case 1: return "ONCE";
                        case 2: return "DOCE";
                        case 3: return "TRECE";
                        case 4: return "CATORCE";
                        case 5: return "QUINCE";
                        default: return "DIECI" + Unidades(unidad);
                    }
                case 2:
                    switch (unidad) {
                        case 0: return "VEINTE";
                        default: return "VEINTI" + Unidades(unidad);
                    }
                case 3: return DecenasY("TREINTA", unidad);
                case 4: return DecenasY("CUARENTA", unidad);
                case 5: return DecenasY("CINCUENTA", unidad);
                case 6: return DecenasY("SESENTA", unidad);
                case 7: return DecenasY("SETENTA", unidad);
                case 8: return DecenasY("OCHENTA", unidad);
                case 9: return DecenasY("NOVENTA", unidad);
                case 0: return Unidades(unidad);
            }
        }//Unidades()

        function DecenasY(strSin, numUnidades) {
            if (numUnidades > 0)
                return strSin + " Y " + Unidades(numUnidades)

            return strSin;
        }//DecenasY()

        function Centenas(num) {
            var centenas = Math.floor(num / 100);
            var decenas = num - (centenas * 100);

            switch (centenas) {
                case 1:
                    if (decenas > 0)
                        return "CIENTO " + Decenas(decenas);
                    return "CIEN";
                case 2: return "DOSCIENTOS " + Decenas(decenas);
                case 3: return "TRESCIENTOS " + Decenas(decenas);
                case 4: return "CUATROCIENTOS " + Decenas(decenas);
                case 5: return "QUINIENTOS " + Decenas(decenas);
                case 6: return "SEISCIENTOS " + Decenas(decenas);
                case 7: return "SETECIENTOS " + Decenas(decenas);
                case 8: return "OCHOCIENTOS " + Decenas(decenas);
                case 9: return "NOVECIENTOS " + Decenas(decenas);
            }

            return Decenas(decenas);
        }//Centenas()

        function Seccion(num, divisor, strSingular, strPlural) {
            var cientos = Math.floor(num / divisor)
            var resto = num - (cientos * divisor)

            var letras = "";

            if (cientos > 0)
                if (cientos > 1)
                    letras = Centenas(cientos) + " " + strPlural;
                else
                    letras = strSingular;

            if (resto > 0)
                letras += "";

            return letras;
        }//Seccion()

        function Miles(num) {
            var divisor = 1000;
            var cientos = Math.floor(num / divisor)
            var resto = num - (cientos * divisor)

            var strMiles = Seccion(num, divisor, "UN MIL", "MIL");
            var strCentenas = Centenas(resto);

            if (strMiles == "")
                return strCentenas;

            return strMiles + " " + strCentenas;
        }//Miles()

        function Millones(num) {
            var divisor = 1000000;
            var cientos = Math.floor(num / divisor)
            var resto = num - (cientos * divisor)

            var strMillones = Seccion(num, divisor, "UN MILLON DE", "MILLONES DE");
            var strMiles = Miles(resto);

            if (strMillones == "")
                return strMiles;

            return strMillones + " " + strMiles;
        }//Millones()

        $scope.numtoleters = function (num) {
            var data = {
                numero: num,
                enteros: Math.floor(num),
                centavos: (((Math.round(num * 100)) - (Math.floor(num) * 100))),
                letrasCentavos: "",
                letrasMonedaPlural: 'PESOS',//"PESOS", 'Dólares', 'Bolívares', 'etcs'
                letrasMonedaSingular: 'PESO', //"PESO", 'Dólar', 'Bolivar', 'etc'

                letrasMonedaCentavoPlural: "CENTAVOS",
                letrasMonedaCentavoSingular: "CENTAVO"
            };

            if (data.centavos > 0) {
                data.letrasCentavos = "CON " + (function () {
                    if (data.centavos == 1)
                        return Millones(data.centavos) + " " + data.letrasMonedaCentavoSingular;
                    else
                        return Millones(data.centavos) + " " + data.letrasMonedaCentavoPlural;
                })();
            };

            if (data.enteros == 0)
                return "CERO " + data.letrasMonedaPlural + " " + data.letrasCentavos;
            if (data.enteros == 1)
                return Millones(data.enteros) + " " + data.letrasMonedaSingular + " " + data.letrasCentavos;
            else
                return Millones(data.enteros) + " " + data.letrasMonedaPlural + " " + data.letrasCentavos;
        }//NumeroALetras()

        $scope.actualizar2 = function () {
            $scope.json.costo=Math.round($scope.json.costo * 100) / 100;
            console.log($scope.json.costo);
            console.log("aquidavid");
            if($scope.json.coopago!=null || $scope.json.coopago!=undefined || $scope.json.coopago !=""){
                $scope.json.coopago=parseInt($scope.json.coopago);
            }
            $scope.json.cod_admin = $scope.admin;
            if ($scope.discapacitado == undefined) {
                $scope.discapacitado = 0;
            }
            if ($scope.prioridad == undefined) {
                $scope.prioridad = 0;
            }
            if (jQuery.isEmptyObject($scope.grilla_servicio)) {
                alert("Porfavor Ingrese Por Lo Menos 1 Servicio a La Tabla");
            } else {
                if ($scope.json.centro_costo == '' || $scope.json.centro_costo == undefined || $scope.json.centro_costo == null) { $scope.json.centro_costo = '0' };
                if ($scope.json.cod_programapyp == '' || $scope.json.cod_programapyp == undefined || $scope.json.cod_programapyp == null) { $scope.json.cod_programapyp = '0' };
                $scope.json.causa_ext = $scope.causa_ext;
                $scope.json.ano = '2017';
                $scope.json.coopago = parseInt($scope.json.coopago);
                var defender = $q.defer();
                var promise = defender.promise;
                if (!$scope.formRecaudo.$valid) {
                    var cr = "Campos requeridos no diligenciados:";
                    $scope.formRecaudo.$error.required.forEach(function (item, index) {
                        cr += "\n - " + item.$name;
                    });
                    alert(cr);
                    defender.resolve(false);
                    return promise;
                }
                else {
                    $scope.openWaiting = true;
                    $timeout(function () {
                        try {
                            Service.getCrud({ consulta02: $scope.json.grupo, consulta01: $scope.json.centro_costo }, "/Salud.svc/Validarccostocontrol").then(function (d) {
                                var rs2 = JSON.parse(d.data);
                                if (rs2 > 0) {
                                    $scope.json.recibo = parseInt($scope.json.recibo);
                                    var rs = false;
                                    if (jQuery.isEmptyObject($scope.grillaforma_pago)) {
                                        $scope.grillaforma_pago = [];
                                    }
                                    Service.getCrud({
                                        head: angular.toJson($scope.json), list: angular.toJson($scope.hora_aux), listproc: angular.toJson($scope.grilla_servicio), codigo_usuario: $scope.cod_user,
                                        discapacitado: $scope.discapacitado, prioridad: $scope.prioridad, admitir: $scope.admitir, listformapago: angular.toJson($scope.grillaforma_pago),cantidad:$scope.cantidad
                                    }, "/Salud.svc/UpdateCitaCumplida").then(function (d) {
                                        console.log(JSON.parse(d.data));
                                        rs = JSON.parse(d.data)[0];
                                        var rs3 = JSON.parse(d.data)[1];
                                        $scope.json.facturafinal = rs3;
                                        if (rs > 0) {
                                            $scope.json.factura=rs;                                            
                                            $scope.imprimir1 = true;
                                            if (rs3 > 0) {
                                                $scope.imprimir2 = true;
                                            }
                                            $scope.datapanelpacienteprint.codigo = ($scope.json.codigo).trim();
                                            $scope.datapanelpacienteprint.cod_admin = ($scope.json.cod_admin).trim();
                                            var f = $('#formRecaudo');
                                            $(f).find('input[type=text],textarea,select,input[type=number],input[type=time],input[type=checkbox]').val('');
                                            $(f).find('input[type=checkbox]').attr('checked', false);
                                            $scope.grilla = [];
                                            $scope.grilla_conf = [];
                                            $scope.citas = {};
                                            $scope.openWaiting = false;
                                            $scope.openModal = true;
                                            console.log("DAVID");
                                            if ($scope.nit_empresa.trim() == '900645074' || $scope.nit_empresa.trim() == '890101994') {
                                                $scope.btnimprimirvoucherFact();
                                                console.log('caja')
                                            }
                                            else {
                                                $scope.imprimir();
                                            }
                                            
                                            $scope.imprimirgrilla = false;
                                            $('#brp-clave').focus();
                                            $scope.json.codigo = '';
                                            $scope.json.centro_costo = '';
                                            $scope.causa_ext = '';
                                            $timeout(function () {
                                                $('#brm-diag1-clave').val('');
                                                $('#brm-diag1-valor').val('');
                                            }, 50);
                                            $scope.json.cod_diag = '';
                                            $scope.json.nom_diag = '';
                                        } else {
                                            alert("Los Datos insertados Son Incorrectos");
                                            $scope.openWaiting = false;
                                        }
                                    });
                                }
                                else {
                                    alert("El Grupo de este servicio no esta habilitado o configurado en este centro de costo");
                                    $scope.openWaiting = false;
                                }
                                defender.resolve(true);
                            });
                        } catch (e) {
                            defender.reject(e);
                        }
                    });
                }
                return promise;


            }
        };

        $scope.autoexist = function () {
            if ($scope.json.nro_aut != undefined) {
                if ($scope.json.nro_aut != null) {
                    if ($scope.json.nro_aut != '') {
                        Service.getCrud({ autorizacion: $scope.json.nro_aut, nit: $scope.json.nit }, "/Admision.svc/ExistAutorizacion").then(function (d) {
                            var j = JSON.parse(d.data);
                            if (j.length > 0) {
                                if (j != "AUTORIZACION VALIDA") {
                                    alert("Nro. de Autorizacion ya existe en " + j);
                                    $scope.json.nro_aut = '';
                                    $('#nro_aut').focus();
                                }
                            }
                        });
                    }
                }
            }

        }

        $scope.actualizar = function () {
            $scope.json.costo=Math.round($scope.json.costo * 100) / 100;
            console.log($scope.json.costo);            
            if($scope.json.coopago!=null || $scope.json.coopago!=undefined || $scope.json.coopago !=""){
                $scope.json.coopago=parseInt($scope.json.coopago);
            }
            console.log("aqui2");
            if ($scope.json.coopago <= 0) {
                $scope.json.cod_admin = $scope.admin;
                if ($scope.discapacitado == undefined) {
                    $scope.discapacitado = 0;
                }
                if ($scope.prioridad == undefined) {
                    $scope.prioridad = 0;
                }
                if (jQuery.isEmptyObject($scope.grilla_servicio)) {
                    alert("Porfavor Ingrese Por Lo Menos 1 Servicio a La Tabla");
                } else {
                    if ($scope.json.centro_costo == '' || $scope.json.centro_costo == undefined || $scope.json.centro_costo == null) { $scope.json.centro_costo = '0' };
                    if ($scope.json.cod_programapyp == '' || $scope.json.cod_programapyp == undefined || $scope.json.cod_programapyp == null) { $scope.json.cod_programapyp = '0' };
                    $scope.json.causa_ext = $scope.causa_ext;
                    $scope.json.ano = '2017';
                    $scope.json.coopago = parseInt($scope.json.coopago);
                    var defender = $q.defer();
                    var promise = defender.promise;
                    if (!$scope.formRecaudo.$valid) {
                        var cr = "Campos requeridos no diligenciados:";
                        $scope.formRecaudo.$error.required.forEach(function (item, index) {
                            cr += "\n - " + item.$name;
                        });
                        alert(cr);
                        defender.resolve(false);
                        return promise;
                    }
                    else {
                        $scope.openWaiting = true;
                        $timeout(function () {
                            try {
                                Service.getCrud({ consulta02: $scope.json.grupo, consulta01: $scope.json.centro_costo }, "/Salud.svc/Validarccostocontrol").then(function (d) {
                                    var rs2 = JSON.parse(d.data);
                                    if (rs2 == true) {
                                        if ($scope.j.asistencialpyp == 2) {
                                            if ($scope.json.cod_programapyp != undefined && $scope.json.cod_programapyp != null) {
                                                if ($scope.json.cod_programapyp.trim() != '') {
                                                    Service.getCrud({ cod_proc: $scope.codigoesp, cod_admin: $scope.admin, cod_programa: $scope.json.cod_programapyp }, "/Admision.svc/validar_matrizpyp").then(function (d) {
                                                        var rs = JSON.parse(d.data);
                                                        if (rs == true) {
                                                            $scope.json.recibo = parseInt($scope.json.recibo);
                                                            if (jQuery.isEmptyObject($scope.grillaforma_pago)) {
                                                                $scope.grillaforma_pago = [];
                                                            }
                                                            Service.getCrud({ head: angular.toJson($scope.json), list: angular.toJson($scope.hora_aux), listproc: angular.toJson($scope.grilla_servicio), codigo_usuario: $scope.cod_user, discapacitado: $scope.discapacitado, prioridad: $scope.prioridad, admitir: $scope.admitir, listformapago: angular.toJson($scope.grillaforma_pago), cantidad: $scope.cantidad }, "/Salud.svc/UpdateCitaCumplida").then(function (d) {
                                                                var rs = JSON.parse(d.data)[0];
                                                                console.log(rs);
                                                                var rs3 = JSON.parse(d.data)[1];
                                                                if (rs3 == 22) {
                                                                    alert("Error De Transaccion, la factura no se genero correctamente");
                                                                    $scope.openWaiting = false;
                                                                    $('#brp-clave').focus();
                                                                }
                                                                else {
                                                                    $scope.json.facturafinal = rs3;
                                                                    if (rs > 0) {
                                                                        $scope.json.factura=rs;                                                                        
                                                                        console.log("ESTOY ENTRANDO AAQUI");
                                                                        $scope.imprimir1 = true;
                                                                        console.log("NIIIIIIITTTTTTTTTTTTT-"+ $scope.nit_empresa);
                                                                        $timeout(function () {  
                                                                            if($scope.nit_empresa=='890101994'){                                                    
                                                                                $scope.btnimprimirvoucher();
                                                                            }    
                                                                            if ($scope.nit_empresa == '900645074') {
                                                                                $scope.btnimprimirvoucherFact();
                                                                            }   
                                                                        });
                                                                        if (rs3 > 0) {
                                                                            $scope.imprimir2 = true;
                                                                        }
                                                                        $scope.datapanelpacienteprint.codigo = ($scope.json.codigo).trim();
                                                                        $scope.datapanelpacienteprint.cod_admin = ($scope.json.cod_admin).trim();
                                                                        var f = $('#formRecaudo');
                                                                        $(f).find('input[type=text],textarea,select,input[type=number],input[type=time],input[type=checkbox]').val('');
                                                                        $(f).find('input[type=checkbox]').attr('checked', false);
                                                                        $scope.grilla = [];
                                                                        $scope.grilla_conf = [];
                                                                        $scope.citas = {};
                                                                        $scope.json.codigo = '';
                                                                        $scope.json.centro_costo = '';
                                                                        $scope.causa_ext = '';
                                                                        $scope.json.sisfact = undefined;
                                                                        $scope.json.centro_costo = undefined;
                                                                        $scope.admin = undefined;
                                                                        $scope.json.cod_programapyp = undefined;                                                                        
                                                                        $timeout(function () {
                                                                            $('#brm-diag1-clave').val('');
                                                                            $('#brm-diag1-valor').val('');
                                                                        }, 50);
                                                                        
                                                                        $scope.json.cod_diag = '';
                                                                        $scope.json.nom_diag = '';
                                                                        $('#brp-clave').focus();
                                                                        if ($scope.json.coopago > 0) {
                                                                            $scope.openModalPago = true;
                                                                        }
                                                                        else {
                                                                            $scope.openModal = true;
                                                                        }
                                                                        $scope.openWaiting = false;
                                                                    } else {
                                                                        alert("Los Datos insertados Son Incorrectos");
                                                                        $scope.openWaiting = false;
                                                                    }
                                                                }


                                                            });
                                                        } else {
                                                            alert("EL PROCEDIMIENTO NO ESTA DISPONIBLE PARA ESTE PROGRAMA PYP");
                                                            $scope.openWaiting = false;
                                                        }
                                                    });
                                                } else {
                                                    alert("SELECCIONE EL PROGRAMA DE PYP");
                                                    $scope.openWaiting = false;
                                                }
                                            }
                                        }
                                        else {
                                            $scope.servicio_procesar();
                                        }
                                    }
                                    else {
                                        alert("El Grupo de este servicio no esta habilitado o configurado en este centro de costo");
                                        $scope.openWaiting = false;
                                    }
                                    defender.resolve(true);
                                });
                            } catch (e) {
                                defender.reject(e);
                            }
                        });
                    }
                    return promise;
                }
            }
            else {
                if (jQuery.isEmptyObject($scope.grilla_servicio)) {
                    alert("Porfavor Ingrese Por Lo Menos 1 Servicio a La Tabla");
                } else {
                    $scope.json.valorcoopago = $scope.json.coopago;
                    $scope.openModalPago = true;
                }

            }
        };

        
        $scope.servicio_procesar = function () {
            console.log("aquidavidservicio_procesar");
            $scope.json.costo=Math.round($scope.json.costo * 100) / 100;
            console.log($scope.json.costo);
            console.log($scope.cantidad);
            $scope.json.recibo = parseInt($scope.json.recibo);
            if (jQuery.isEmptyObject($scope.grillaforma_pago)) {
                $scope.grillaforma_pago = [];
            }
            Service.getCrud({ head: angular.toJson($scope.json), list: angular.toJson($scope.hora_aux), listproc: angular.toJson($scope.grilla_servicio), codigo_usuario: $scope.cod_user, discapacitado: $scope.discapacitado, prioridad: $scope.prioridad, admitir: $scope.admitir, listformapago: angular.toJson($scope.grillaforma_pago), cantidad: $scope.cantidad }, "/Salud.svc/UpdateCitaCumplida").then(function (d) {
                var rs = JSON.parse(d.data)[0];
                console.log(rs)
                var rs3 = JSON.parse(d.data)[1];
                if (rs3 == 22) {
                    alert("Error De Transaccion, la factura no se genero correctamente");
                    $scope.openWaiting = false;
                    $('#brp-clave').focus();
                }
                else {
                    $scope.json.facturafinal = rs3;
                    if (rs > 0) {
                        $scope.json.factura=rs;                                            
                        $scope.imprimir1 = true;
                        if (rs3 > 0) {
                            $scope.imprimir2 = true;
                        }
                        console.log("ESTOY ENTRANDO AAQUI");
                        $scope.imprimir1 = true;
                        console.log("NIIIIIIITTTTTTTTTTTTT-"+ $scope.nit_empresa);
                        $timeout(function () {  
                            if($scope.nit_empresa=='890101994'){                                                    
                                $scope.btnimprimirvoucher();
                            } 
                            if ($scope.nit_empresa.trim() == '900645074') {
                                $scope.btnimprimirvoucherFact();
                            } 
                        });
                        $scope.datapanelpacienteprint.codigo = ($scope.json.codigo).trim();
                        $scope.datapanelpacienteprint.cod_admin = ($scope.json.cod_admin).trim();
                       

                        var f = $('#formRecaudo');
                        $(f).find('input[type=text],textarea,select,input[type=number],input[type=time],input[type=checkbox]').val('');
                        $(f).find('input[type=checkbox]').attr('checked', false);
                        $scope.grilla = [];
                        $scope.grilla_conf = [];
                        $scope.citas = {};
                        $scope.json.codigo = '';
                        $scope.json.centro_costo = '';
                        $scope.json.total = undefined;
                        $scope.cantidad = undefined;
                        $scope.causa_ext = '';
                        $scope.json.sisfact = undefined;
                        $scope.json.centro_costo = undefined;
                        $scope.admin = undefined;
                        $timeout(function () {
                            $('#brm-diag1-clave').val('');
                            $('#brm-diag1-valor').val('');
                        }, 50);
                        $scope.json.cod_diag = '';
                        $scope.json.nom_diag = '';
                        $('#brp-clave').focus();
                        if ($scope.json.coopago > 0) {
                            $scope.openModalPago = true;
                        }
                        else {
                            $scope.openModal = true;
                        }
                        $scope.openWaiting = false;
                    } else {
                        alert("Los Datos insertados Son Incorrectos");
                        $scope.openWaiting = false;
                    }
                }


            });
        }

        $scope.$watch('json.sisfact', function (d) {
            if (d == undefined) {
                $scope.json.sisfact = 1;
            }
        })

        /*COMBOS*/
        $scope.convertempresa = function (x) {
            var y
            if (x != null) {
                $scope.empresa.forEach(function (item, index) {
                    if (item.codigo.trim() == x.trim()) {
                        y = item.nombre;
                    }
                });
            }
            return y;

        }
        $scope.convertdate = function (fecha) {
            return ssDate.dateToString(fecha, 'dd-MM-yyyy');
        }

        Service.getCrud({}, "/Salud.svc/PreloadRecaudo").then(function (d) {
            var data = JSON.parse(d.data);

            if (d.data[1] == '?') {
                alert(d.data);
            }
            $scope.causa = data[0];
            $scope.programa = data[1];
            $scope.centros = data[2];
            $scope.pago = data[3];
            $scope.tipo_cita = data[4];
            $scope.empresa = data[5];
            $scope.empresa.forEach(function (item, index) {
                if (item.estado != 2 || item.vigente != 1) {
                    $scope.empresa.splice(index, 1);
                }
            });
            $scope.formapago = data[6];
        });
        
        Service.getCrud({}, "/Admision.svc/PreloadIngreso").then(function (d) {
            $scope.prefijo = JSON.parse(d.data)[3];
            $scope.tipo_diag = JSON.parse(d.data)[5];
            $scope.json.tipo_diag = '1';
        });
        Service.getCrud({}, "/Salud.svc/traernumerodefactura").then(function (d) {
            var data = JSON.parse(d.data);
            $scope.json.factura = parseInt(data) + 1;
        });

        /*-------- confirmar cita --------------------*/
        $scope.confirmCita = function (i) {
            $scope.hora_aux = [];
            //$scope.key = $scope.grilla[i].codigo_proce;
            $scope.json.codigo_med = $scope.grilla[i].cod_med;
            $scope.json.cod_programapyp = $scope.grilla[i].pyp;
            $scope.json.tipo_diag = "1";
            console.log($scope.causa);
            console.log($scope.centros);
            if (Datos.nit.trim() == '900994767') {
                $scope.causa_ext = '13- ENFERMEDAD GENERAL                            ';
                $scope.json.centro_costo = 'CE        ';
                console.log("ENTRAR");
            }
            $scope.adminn = $scope.grilla[i].cod_adm;
            $scope.hora_aux.push({ hora: $scope.grilla[i].hora, medico: $scope.grilla[i].cod_med});
            $scope.grilla_conf.push({ hora: $scope.grilla[i].hora });
            $scope.calc();
            Service.getCrud({ nivel: parseInt($scope.nivel), codigo: $scope.json.codigo, admin: $scope.adminn }, "/Salud.svc/GetNivelAdmin").then(function (d) {
                var x = JSON.parse(d.data);
                $scope.json.coopago = x;
            });
        }
        $scope.restoredCita = function (i) {
            console.log($scope.grilla_conf);
            var res = $scope.grilla[i];
            $scope.grilla_conf.forEach(function (item, index) {
                if (item.fecha == res.fecha && item.hora == res.hora) {
                    $scope.grilla_conf.splice(index, 1);
                    $scope.hora_aux.splice(index, 1);
                }
            });
            $scope.limpiarservicio();
        }

        /*---------------------FUNCIONES--------------------------------------------------------------*/

        /*CARGADO DE SERVICIO*/
        $scope.setco = function (item) {
            console.log(item);
            $scope.key.value = '';
            $scope.detalle = '';
            $scope.json.cod_proc = '';
            $scope.empres = '';
            $scope.hours = '';
            $scope.codigo_medi = '';
            $scope.rest = '';
            $scope.admin = '';
            $scope.json.cod_programapyp = '';
            if (item != null) {
                $scope.nombremedico = item.medico;
                $scope.horareporte = item.hora;
                $scope.detalle = item.detalle;
                $scope.numero_his = item.numero;
                $scope.ctradminaux = 0;
                $scope.empresa.forEach(function (it, index) {
                    if ($scope.ctradminaux == 0) {
                        if (it.codigo == item.cod_adm) {
                            $scope.ctradminaux = 1;
                            if (it.vigente != 1 || it.estado != 2) {
                                $scope.ctradminaux = 2;
                            }
                        }
                    }
                })
                if ($scope.ctradminaux == 0 || $scope.ctradminaux == 1) {
                    $scope.admin = item.cod_adm.trim();
                    $scope.empres = item.empresa;
                    if ($scope.admin != undefined) {
                        if ($scope.admin != '') {
                            Service.getCrud({ codigo: $scope.admin }, "/Salud.svc/TarifaAdmin").then(function (rs) {
                                $scope.dataServicios.inner_join = [];
                                $scope.dataServicios.inner_join.push({ table: "admin", on: "p.tarifa = '" + JSON.parse(rs.data).tarifa.trim() + "'" });
                                //$scope.dataServicios.inner_join[0].on = "p.tarifa = '" + JSON.parse(rs.data).trim() + "'";
                                if (item.codigo_proce != '') {
                                    console.log(item.codigo_proce);
                                    $scope.json.cod_proc = item.codigo_proce;
                                    $scope.key.value = item.codigo_proce;
                                    $scope.coopagoaux = item.coopago;
                                    $scope.tf = JSON.parse(rs.data).tarifa.trim();
                                    if ($('#brs-clave').val() == '') {
                                        Service.getCrud({ pack: $scope.json.cod_proc, tarifa: $scope.tf}, "/Salud.svc/BuscarPaquete").then(function (d) {
                                            var pack = JSON.parse(d.data);
                                            $('#brs-clave').val(pack.codigo);
                                            $('#brs-valor').val(pack.nombre);
                                        });
                                    }
                                } else {
                                    Service.getCrud({ codigo_med: item.cod_med }, "/Salud.svc/ProceMed").then(function (d) {
                                        console.log(d.data);
                                        var j = JSON.parse(d.data);
                                        if (item.convenio_cita = '2- CONTROL') {
                                            $scope.json.cod_proc = j.tarifacontrol;
                                            $scope.key.value = j.tarifacontrol;
                                        } else {
                                            $scope.json.cod_proc = j.tarifa;
                                            $scope.key.value = j.tarifa;
                                        }
                                    });
                                }
                            });
                        }
                    }
                    $scope.hours = item.hora;
                    $scope.codigo_medi = item.cod_med;
                    $scope.rest = item.cod_adm;
                    $scope.json.cod_programapyp = item.pyp;
                } else {
                    alert("La administradora no se encuentra vigente, por favor cambiar administradora");
                    $scope.admin = item.cod_adm.trim();
                    $scope.empres = item.empresa;
                    if ($scope.admin != undefined) {
                        if ($scope.admin != '') {
                            Service.getCrud({ codigo: $scope.admin }, "/Salud.svc/TarifaAdmin").then(function (rs) {
                                $scope.dataServicios.inner_join = [];
                                $scope.dataServicios.inner_join.push({ table: "admin", on: "p.tarifa = '" + JSON.parse(rs.data).tarifa.trim() + "'" });
                                //$scope.dataServicios.inner_join[0].on = "p.tarifa = '" + JSON.parse(rs.data).trim() + "'";
                                if (item.codigo_proce != '') {
                                    $scope.json.cod_proc = item.codigo_proce;
                                    $scope.key.value = item.codigo_proce;
                                    $scope.coopagoaux = item.coopago;
                                    console.log($scope.coopagoaux);
                                    $scope.tf = JSON.parse(rs.data).tarifa.trim();
                                    if ($('#brs-clave').val() == '') {
                                        Service.getCrud({ pack: $scope.json.cod_proc, tarifa: $scope.tf }, "/Salud.svc/BuscarPaquete").then(function (d) {
                                            var pack = JSON.parse(d.data);
                                            $('#brs-clave').val(pack.codigo);
                                            $('#brs-valor').val(pack.nombre);
                                        });
                                    }
                                } else {
                                    Service.getCrud({ codigo_med: item.cod_med }, "/Salud.svc/ProceMed").then(function (d) {
                                        console.log(d.data);
                                        var j = JSON.parse(d.data);
                                        if (item.convenio_cita = '2- CONTROL') {
                                            $scope.json.cod_proc = j.tarifacontrol;
                                            $scope.key.value = j.tarifacontrol;
                                        } else {
                                            $scope.json.cod_proc = j.tarifa;
                                            $scope.key.value = j.tarifa;
                                        }
                                    });
                                }
                            });
                        }
                    }
                    $scope.hours = item.hora;
                    $scope.codigo_medi = item.cod_med;
                    $scope.rest = item.cod_adm;
                    $scope.json.cod_programapyp = item.pyp;
                }


            }
        }
        $scope.cancelar = function () {
            var f = $('#formRecaudo');
            $(f).find('input[type=text],textarea,select,input[type=number],input[type=date],input[type=time],input[type=checkbox]').val('');
            $(f).find('input[type=checkbox]').attr('checked', false);
            $scope.grilla = [];
            $scope.citas_ant = [];
        }
        $scope.cerrar = function () {
            var currentButton = angular.element(document.getElementById('salud-system'));
            $timeout(function () {
                var currentButton = angular.element(document.getElementById('salud-system'));
                $timeout(function () {
                    $('#salud-system').click();
                    currentButton.triggerHandler("click");
                });
                currentButton.triggerHandler("click");
            });
        }
        $scope.cambiar = function () {
            if ($scope.admin != null) {
                if ($scope.admin != '') {
                    if ($scope.admin != undefined) {
                        $scope.cambioemp = false;
                    } else {
                        alert('Debe consultar una cita');
                    }
                } else {
                    alert('Debe consultar una cita');
                }
            } else {
                alert('Debe consultar una cita');
            }




        }
        /*---------------------WATCHS-----------------------------------------------------------------*/
        /*----------Cuota----------------------------*/
        $scope.$watch('cuota', function (d) {
            if (d != undefined) {
                $scope.pagar = '$' + (d);

                $scope.$watch('incumplidas', function (c) {
                    if (c == undefined) {
                        $scope.pagar = '$' + (d);
                    } else {
                        var cuot = parseInt(d);
                        var inc = parseInt(c);
                        $scope.pagar = '$' + (cuot + inc);
                    }
                });
            };


        });
        /*----------BUSCADOR DE CITAS----------------*/
        $scope.$watch(('json.codigo'), function (d) {
            $scope.grilla = [];
            $scope.grilla = '';
            $scope.citas_ant = [];
            $scope.citas_ant = '';
            if (d != '' || d != null || d != undefined) {
                if (d == undefined) { } else {
                    if (Datos.nit.trim() == "802012036") {
                        $scope.fecha_nac(d);
                    }
                    $scope.json.fecha_cons = ssDate.dateToString($scope.fecha, 'yyyy-MM-dd');
                    //$scope.json.fecha_cons = $scope.fecha;
                    Service.getCrud({ codigo: d, fecha_cons: $scope.fecha }, "/Salud.svc/GetCitas_codigo_fecha").then(function (rs) {
                        $scope.grilla = JSON.parse(rs.data);
                        $scope.grilla.forEach(function (item, index) {
                            $scope.grilla.telefono = item.telefono.trim();
                            var f = new Date();
                            var cad = f.getHours() + ":" + f.getMinutes();
                            var x = ssDate.comparetohour(item.hora, cad);
                            $scope.funcionauxiliar(x);
                        });
                    });
                    Service.getCrud({ codigo: d, fecha: $scope.json.fecha_cons }, "/Salud.svc/RecordRecaudo").then(function (rs) {
                        $scope.citas_ant = JSON.parse(rs.data);
                    });

                }

            }
        });
        $scope.funcionauxiliar = function (x) {
            if (x == 1 || x == 2) {
                $('#tabla table tr').each(function () {
                    if ($.trim($(this).text()).length > 0) {
                        $(this).addClass("myclass");
                    }
                });
                $timeout(function () {
                    $("#colorear").addClass("myclass");
                });
            }
        }
        $scope.$watch(('fecha'), function (d) {
            $scope.grilla = [];
            $scope.grilla = '';
            $scope.citas_ant = [];
            $scope.citas_ant = '';
            if (d != '') {
                if (d > new Date()) {
                    alert("NO SE PUEDE CONFIRMAR CITAS CON FECHAS MAYORES A LA ACTUAL");
                    $scope.fecha = new Date();
                } else {
                    if ($scope.json.codigo == null || $scope.json.codigo == '' || $scope.json.codigo == undefined) {
                    } else {
                        //$scope.json.fecha_cons = ssDate.dateToString(d, 'dd-MM-yyyy');
                        Service.getCrud({ codigo: $scope.json.codigo, fecha_cons: $scope.fecha }, "/Salud.svc/GetCitas_codigo_fecha").then(function (rs) {
                            $scope.grilla = JSON.parse(rs.data);
                        });
                        Service.getCrud({ codigo: $scope.json.codigo, fecha: $scope.fecha }, "/Salud.svc/RecordRecaudo").then(function (rs) {
                            $scope.citas_ant = JSON.parse(rs.data);
                        });
                    }
                }

            }
        });
        /*------- AUTORIZACIONES ----------------*/
        $scope.shpyp = false;
        $scope.$watch(('admin'), function (d1, d2) {
            if (d1 != '' && d1 != null) {
                if (d2 != '' && d2 != null) {
                    if (d1 != d2) {
                        if (d1.length != 0 && d2.length != 0) {
                            $scope.open2();
                            $scope.emp_new = d1;
                            $scope.emp_old = d2;
                        };

                    }
                }
            }

        });
        $scope.change = function () {
            var rs = false;
            $scope.json.fecha_cons = ssDate.dateToString($scope.fecha, 'yyyy-MM-dd');
            Service.getCrud({ codigo: $scope.json.codigo, fecha: $scope.json.fecha_cons, adm_new: $scope.emp_new, adm_old: $scope.emp_old, hora: $scope.hours, cod_med: $scope.codigo_medi }, "/Salud.svc/ChangeAdmin").then(function (d) {
                rs = JSON.parse(d.data);
                $scope.grilla_servicio = [];
                if (rs == true) {
                    $scope.grilla = [];
                    $scope.grilla = '';
                    Service.getCrud({ codigo: $scope.json.codigo, fecha_cons: $scope.json.fecha_cons }, "/Salud.svc/GetCitas_codigo_fecha").then(function (rs) {
                        $scope.grilla = JSON.parse(rs.data);
                    });
                }
            });
            $scope.close2();
            $scope.cambioemp = true;
        }
        $scope.cancel_change = function () {
            $scope.cambioemp = true;
            $scope.admin = $scope.rest;
            $scope.close2();
        }
        $scope.$watch(('admin'), function (d) {
            if (d != undefined && d != '') {
                $scope.emp = d;
                Service.getCrud({ cod_admin: $scope.emp }, "/Salud.svc/getAdmin").then(function (rs) {
                    $scope.j = JSON.parse(rs.data);
                    if ($scope.j != null) {
                        $scope.servicio = $scope.j.servicios;
                        $scope.tarifa = $scope.j.tarifa;
                        $scope.porcentaje = $scope.j.vlr_p_serv;
                        $scope.json.nit = $scope.j.nit;
                        if ($scope.j.autorizacion == 0) {
                            $scope.shaut = false;
                            $scope.json.nro_aut = '';
                        }
                        if ($scope.j.autorizacion == 1) {
                            $scope.shaut = true;
                            $scope.json.nro_aut = '';
                        }
                        if ($scope.j.autorizacion == 2) {
                            $scope.shaut = true;
                            $scope.json.nro_aut = '';
                        }
                        if ($scope.j.asistencialpyp == 1 || $scope.j.asistencialpyp == 0) {
                            $scope.shpyp = false;
                        }
                        if ($scope.j.asistencialpyp == 2) {
                            $scope.shpyp = true;
                        }

                    }

                });
            }


        });
        $scope.calc = function () {
            var j;
            console.log($scope.json);
            if ($scope.tarifa != undefined) {
                $scope.validar();
                if ($scope.json.total == null) {
                    Service.getCrud({ empresa: $scope.empres, proce: $scope.json.cod_proc, tarifa: $scope.tarifa }, "/Salud.svc/GetValor_Conf").then(function (d) {
                        j = JSON.parse(d.data)[0];
                        if (j != undefined) {
                            $scope.json.cod_admin = j.Codigo_Emp;
                            $scope.json.nit = j.Nit_Emp;
                            var aux = j.Indicador;
                            if (aux == 1) {
                                var total = 0;
                                var valor = parseInt(j.Valor_Pro);
                                var porc = parseInt(j.Porcentaje_Pro);
                                if (porc <= 0) {
                                    total = valor;
                                } else {
                                    total = ((valor * (porc / 100)) + (valor));
                                }
                                $scope.json.total = total;
                            }
                            if (j["Indicador"] == 2) {
                                var total = 0;
                                var valor = parseInt(j.Valor_Pro);
                                var porc = parseInt(j.Porcentaje_Pro);
                                if (porc <= 0) {
                                    total = valor;
                                } else {
                                    total = ((valor) - (valor * (porc / 100)));
                                }
                                $scope.json.total = total;
                            }
                            if (aux == 3) {
                                var total = 0;
                                var valor = j.Valor_Pro;
                                var porc = j.Porcentaje_Pro;
                                if (porc <= 0) {
                                    total = valor;
                                } else {
                                    total = valor;
                                }
                                $scope.json.total = total;
                            }
                        }
                        else {
                            $scope.json.nit = "";
                            $scope.json.cod_admin = $scope.admin;
                        }
                    });
                }
            }
        };
        /*MODAL*/
        $scope.open = function () {
            $scope.openModal = true;

        }
        $scope.close = function () {
            $scope.openModal = false;
        }
        $scope.open2 = function () {
            $scope.openModalDos = true;
        }
        $scope.close2 = function () {
            $scope.openModalDos = false;

        }

        $scope.closepyp = function () {
            $scope.openModalpyp = false;

        }

        $scope.change_pyp = function () {
            $scope.closepyp();
            //$scope.json.cod_programapyp = undefined;
            $('#prog_pyp').focus();


        }

        $scope.savechangepyp = function () {
            if ($scope.json.cod_programapyp != undefined) {
                Service.getCrud({ codigo: $scope.json.codigo, fecha: $scope.json.fecha_cons, pyp_new: $scope.json.cod_programapyp, hora: $scope.hours, cod_med: $scope.codigo_medi }, "/Salud.svc/ChangePyP").then(function (d) {
                    var rs = JSON.parse(d.data);
                    if ($scope.grilla_servicio.length > 0) {
                        $scope.grilla_servicio = [];
                    }
                    //if (rs == true) {
                    //    $scope.grilla = [];
                    //    $scope.grilla = '';
                    //    $scope.json.cod_programapyp = undefined;
                    //    Service.getCrud({ codigo: $scope.json.codigo, fecha_cons: $scope.json.fecha_cons }, "/Salud.svc/GetCitas_codigo_fecha").then(function (rs) {
                    //        $scope.grilla = JSON.parse(rs.data);
                    //    });
                    //}
                    //$scope.add_proce();
                });
            }
        }


        $scope.imprimir = function () {
            $scope.imprimirgrilla = false;
            $scope.imprimirrecibocaja = true;
            $scope.show_fac_venta_pivi = false;
            $('#menu').addClass("ss-nav-inactive");
            $('#wrapper').addClass("ss-full-view");
            window.print();
        }
        $scope.bttnprintfactura = function () {
            $scope.imprimirgrilla = true;
            $scope.imprimirrecibocaja = false;
            $scope.show_fac_venta_pivi = false;
            $scope.imprimirvoucher = false;
            $timeout(function () {
                $('#menu').addClass("ss-nav-inactive");
                $('#wrapper').addClass("ss-full-view");
                window.print();
            }, 1000)
        }
        $scope.btnimprimirfact = function () {
            $scope.imprimirgrilla = false;
            $scope.imprimirrecibocaja = false;
            $scope.show_fac_venta_pivi = true;
            $timeout(function () {
                $('#menu').addClass("ss-nav-inactive");
                $('#wrapper').addClass("ss-full-view");
                window.print();
            }, 1000)
        }

        Service.getCrud({}, "/Salud.svc/Nom_Sistema").then(function (d) {
            var a = JSON.parse(d.data);
            $scope.nit_empresa = a.nit.trim();
            console.log(a)
        });

        $scope.btnimprimirfact = function () {
            console.log("nit " + $scope.json.nit);
            if ($scope.nit_empresa.trim() == "900556028") {
                $scope.show_footer_biobac = true;
            }
            //NIT DE  PIVIJAY
            if ($scope.nit_empresa.trim() == "819002025") {
                $scope.imprimirgrilla = false;
                $scope.imprimirrecibocaja = false;
                $scope.show_fac_venta_pivi = true;
                var defender = $q.defer();
                var promise = defender.promise;
                $timeout(function () {
                    try {
                        var rs2 = false;
                        var fecha_aux = new Date();
                        var f = new Date();
                        Service.getCrud({ sisfact: $scope.json.sisfact, factura: $scope.json.facturafinal }, "/Salud.svc/ReportFactura2").then(function (d) {
                            rs2 = JSON.parse(d.data);
                            $scope.numeroatencion = rs2[3].historia;
                            if (rs2[3].usuario != null && rs2[3].usuario.trim() != "") {
                                $scope.usuarioproceso = rs2[3].usuarioproceso.toUpperCase();
                            }
                            $scope.json.fecha_venc = ssDate.militodate(rs2[3].fecha_venc);
                            $scope.json.fecha_venc = ssDate.dateToString($scope.json.fecha_venc, "yyyy-MM-dd");
                            $scope.json.fecha_prec = ssDate.militodate(rs2[3].fecha_prec);
                            $scope.json.fecha_prec = ssDate.dateToString($scope.json.fecha_prec, "yyyy-MM-dd");
                            $scope.prefi = rs2[2].prefijo;
                            $scope.json.admin_nombre = rs2[4].nombre;
                            $scope.json.admin_nit = rs2[1].nit;
                            $scope.json.nombre = rs2[1].nombre;
                            $scope.json.nit = rs2[1].nit;
                            $scope.json.convenio = rs2[1].convenio;
                            $scope.json.direccion = rs2[1].direccion;
                            $scope.json.telefono = rs2[1].telefono;
                            $scope.ReportFac = [];
                            $scope.tama = rs2.length;
                            defender.resolve(rs2);
                            $scope.totalresumen = 0;
                            $scope.subtotal = 0;
                            $scope.totalcoopago = 0;
                            $scope.totaldescuento = 0;
                            $scope.nfecha_sal = ssDate.dateToString($scope.json.fecha, "yyyy/MM/dd");
                            $scope.nfecha = ssDate.dateToString($scope.json.fecha_sal, "yyyy/MM/dd");
                            rs2[0].forEach(function (item, index) {
                                ////console.log(item);
                                $scope.ReportFac.push(item);
                                $scope.subtotal += item.total;
                                $scope.totalcoopago += item.coopagorips;
                                $scope.totaldescuento += item.dsto;
                            });
                            $scope.json.vlr_fact = $scope.subtotal - $scope.totalcoopago;
                            $scope.num_en_letras = $scope.numtoleters($scope.json.vlr_fact);
                            console.log($scope.tama)
                            if ($scope.tama > 0) {
                                var currentButton = angular.element(document.getElementById('menucerrar'));
                                $timeout(function () {
                                    currentButton.triggerHandler("click");
                                    $('#menucerrar').click();
                                });
                                $('#menu').addClass("ss-nav-inactive");
                                $('#wrapper').addClass("ss-full-view");
                                $scope.close();
                                $timeout(function () {
                                    window.print();
                                });
                            } else {
                                alert("LA CONSULTA NO GENERA SALIDA");
                                $scope.show_fac_venta = false;
                            }
                        });
                    } catch (e) {
                        defender.reject(e);
                    }
                });
                return promise;
            } else {
                $scope.show_fac_venta_pivi = false;
                $scope.show_detalladas_ord = false;
                $scope.show_detalladas_ord = false;
                $scope.verpublico = false;
                if ($scope.json.facturafinal != 0 && $scope.json.facturafinal != null && $scope.json.facturafinal != undefined && $scope.json.facturafinal != '') {
                    $scope.verprest = false;
                    $scope.show_fac_venta = true;
                    var defender = $q.defer();
                    var promise = defender.promise;
                    $timeout(function () {
                        try {
                            var rs2 = false;
                            var fecha_aux = new Date();
                            var f = new Date();
                            if (typeof $scope.fecha2 != "string") {
                                var fech = f.setDate($scope.fecha2.getDate() + parseInt($scope.dias));
                                $scope.json.fecha_venc = new Date(fech);
                                $scope.json.fecha_venc = ssDate.dateToString($scope.json.fecha_venc, "yyyy/MM/dd");
                                $scope.fecha2 = ssDate.dateToString($scope.fecha2, "yyyy/MM/dd");
                            }
                            console.log("antes del service");
                            Service.getCrud({ fuente: $scope.json.fuente, factura: $scope.json.facturafinal }, "/Admision.svc/ReportFactura2").then(function (d) {
                                rs2 = JSON.parse(d.data);
                                console.log(rs[3]);
                                $scope.json.fecha_venc = ssDate.dateToString(rs[3].fecha_venc, "yyyy/MM/dd");
                                $scope.json.fecha_prec = ssDate.dateToString(rs[3].fecha_prec, "yyyy/MM/dd");
                                $scope.prefi = rs2[2].prefijo;
                                $scope.json.admin_nombre = rs2[3].nombre;
                                $scope.json.admin_nit = rs2[3].codigo;
                                $scope.json.nombre = rs2[1].nombre;
                                $scope.json.nit = rs2[1].nit;
                                $scope.json.direccion = rs2[1].direccion;
                                $scope.json.telefono = rs2[1].telefono;
                                $scope.ReportFac = [];
                                $scope.tama = rs2.length;
                                defender.resolve(rs2);
                                $scope.totalresumen = 0;
                                $scope.subtotal = 0;
                                $scope.totalcoopago = 0;
                                $scope.totaldescuento = 0;
                                rs2[0].forEach(function (item, index) {
                                    ////console.log(item);
                                    $scope.ReportFac.push(item);
                                    $scope.subtotal += item.total;
                                    $scope.totalcoopago += item.coopagorips;
                                    $scope.totaldescuento += item.dsto;
                                });
                                $scope.json.vlr_fact = $scope.subtotal - $scope.totalcoopago;
                                $scope.num_en_letras = $scope.numtoleters($scope.json.vlr_fact);
                                console.log($scope.tama)
                                if ($scope.tama > 0) {
                                    var currentButton = angular.element(document.getElementById('menucerrar'));
                                    $timeout(function () {
                                        currentButton.triggerHandler("click");
                                        $('#menucerrar').click();
                                    });
                                    $('#menu').addClass("ss-nav-inactive");
                                    $('#wrapper').addClass("ss-full-view");
                                    $scope.close();
                                    $timeout(function () {
                                        window.print();
                                    });
                                } else {
                                    alert("LA CONSULTA NO GENERA SALIDA");
                                    $scope.show_fac_venta = false;
                                }
                            });
                        } catch (e) {
                            defender.reject(e);
                        }
                    });
                    return promise;
                } else {
                    alert("NO EXISTE FACTURA");
                }
            }
        }

        $scope.open_Pac_update = function () {
            //$scope.showviewModalView.state = undefined;
            $timeout(function () {
                var currentButton = angular.element(document.getElementById('Paciente-0'));
                currentButton.triggerHandler("click");
                Datos.info.ingreso_ventana = "CC";
                Datos.info.accion = "editar";
                Datos.info.cod_paci = $scope.json.codigo;
                //$scope.dataModalView.url = 'js/app/Modulo/Consulta Externa/Pacientes2.html';
                //$scope.showviewModalView.state = true;
                //$scope.openModalPac = false;
            });

        }
        if (Datos.info.ingreso_ventana == "paciente") {
            $timeout(function () {
                $('#formIngresoConsExt-nuevo').click();
            }, 2000)
            $timeout(function () {
                $scope.key2.value = Datos.info.cod_paci;
                //$scope.rsPaciente($scope.json.codigo);
            }, 2200)
        }
        $scope.closePac = function () {
            $scope.openModalPac = false;
            $scope.openModalPac_update = false;
        }
        $scope.btnimprimirvoucher = function () {
            $scope.openWaiting = true;
            $scope.imprimirgrilla = false;
            $scope.imprimirrecibocaja = false;
            $scope.show_fac_venta_pivi = false;
            $scope.show_footer_biobac = false;
            $scope.imprimirvoucher = true;
            $scope.imprimirvoucherfact = false;
            if ($scope.auxiliarcodigo != undefined && $scope.auxiliarcodigo != undefined && $scope.auxiliarcodigo != "") {
                if ($scope.fecha != undefined && $scope.fecha != undefined && $scope.fecha != "") {
                    if ($scope.hora_aux[0].hora != undefined && $scope.hora_aux[0].hora != undefined && $scope.hora_aux[0].hora != "") {
                        Service.getCrud({ codigo: $scope.auxiliarcodigo, fecha: $scope.fecha, hora: $scope.hora_aux[0].hora }, "/Salud.svc/Imprimirvoucher").then(function (d) {
                            var rs = JSON.parse(d.data);
                            if (rs[0] == '?') {
                                alert(rs);
                                $scope.openWaiting = false;
                            } else {
                                $scope.voucher = rs;
                                console.log($scope.voucher);
                                $scope.json.historia = rs.Atencion;
                                $timeout(function () {
                                    $scope.openWaiting = false;
                                    window.print();
                                }, 800)

                            }
                        });
                    }
                }
            }
        }

        $scope.btnimprimirvoucherFact = function () {
            console.log("Q RARO");
            $scope.openWaiting = true;
            $scope.imprimirgrilla = false;
            $scope.imprimirrecibocaja = false;
            $scope.show_fac_venta_pivi = false;
            $scope.show_footer_biobac = false;
            $scope.imprimirvoucher = false;
            $scope.imprimirvoucherfact = true;
            if ($scope.auxiliarcodigo != undefined && $scope.auxiliarcodigo != undefined && $scope.auxiliarcodigo != "") {
                if ($scope.fecha != undefined && $scope.fecha != undefined && $scope.fecha != "") {
                    if ($scope.hora_aux[0].hora != undefined && $scope.hora_aux[0].hora != undefined && $scope.hora_aux[0].hora != "") {
                        Service.getCrud({ codigo: $scope.auxiliarcodigo, fecha: $scope.fecha, hora: $scope.hora_aux[0].hora }, "/Salud.svc/Imprimirvoucher").then(function (d) {
                            var rs = JSON.parse(d.data);
                            if (rs[0] == '?') {
                                alert(rs);
                                $scope.openWaiting = false;
                            } else {
                                $scope.voucher = rs;
                                console.log($scope.voucher);
                                $timeout(function () {
                                    $scope.openWaiting = false;
                                    window.print();
                                }, 800)

                            }
                        });
                    }
                }
            }
        }
    }
    function OrdenFisicaController($scope, $q, $timeout, ssDate, Service, Auth, Datos) {
        $scope.json = {};
        $scope.json2 = {};
        $scope.cod_user = Auth.getUserObj().codigo;
        $scope.auxuser = Auth.getUserObj().usuario1;
        console.log(Datos.info);
        $scope.json.usuario = Datos.info.item.medico;
        $scope.json.codigo = Datos.info.item.codigo;
        $scope.json.cod_admin = Datos.info.item.cod_admin;
        $scope.json.numero = Datos.info.item.numero2;
        $scope.json2.medico = Datos.info.item.medico;
        $scope.profilenivel = angular.toJson({ modulo: 'ADMISIONES', ventana: '03', usuario: $scope.auxuser });
        $scope.disabledbtn = { buscar: false, imprimir: true, nuevo: false, editar: true, eliminar: true };
        $scope.showbtn = { buscar: true, imprimir: true, nuevo: true, editar: true, eliminar: true };
        $scope.dataPanelPaciente = { codigo: Datos.info.item.codigo, cod_admin: Datos.info.item.cod_admin };
        $scope.datapanelpacienteprint = { codigo: Datos.info.item.codigo, cod_admin: Datos.info.item.cod_admin };
        $scope.validarfirma = function () {
            $scope.modelo = {};
            Service.getCrud({ codigo: $scope.json2.medico, cod_pac: '' }, '/Salud.svc/firma_medico1').then(function (d) {
                var rs = JSON.parse(d.data);
                $scope.model = rs[0];
                $scope.especi = rs[1];
                $scope.model1 = rs[2];
                $scope.nombre_medico = $scope.especi[0].nombre;
                $scope.registro_med = $scope.especi[0].registro_medico;
                $scope.espe = $scope.especi[1].nombre;
                $scope.upload = $scope.model.rutafirma;
                if ($scope.upload != null && $scope.upload.trim() != '') {
                    $('#labelfirma').css("margin-top", "100px");
                }
                else {
                    $('#labelfirma').css("margin-top", "30px");
                }
                if ($scope.model.rutafirma != null && $scope.model.rutafirma != undefined && $scope.model.rutafirma != '') {
                    $scope.upload = ('data:image/jpeg;base64,' + $scope.model.rutafirma);
                }
                $scope.upload1 = $scope.model1.huella;
                if ($scope.model1.huella != null && $scope.model1.huella != undefined && $scope.model1.huella != '') {
                    $scope.upload1 = ('data:image/jpeg;base64,' + $scope.model1.huella);
                }
            });
        }
        $scope.nuevo = function () {
            $scope.cancelar();
            $scope.validarfirma();
            $scope.json.fecha = new Date();
            $scope.json.hora = ssDate.dateToString(new Date(), "HH:mm")
        }
        $scope.cancelar = function () {
            $scope.json.fecha = null;
            $scope.json.hora = null;
        }
        $scope.cerrar = function () {
            $('#cerrar').click();
        }
        $scope.guardar = function () {
            var defender = $q.defer();
            var promise = defender.promise;
            $scope.json2.historia = Datos.info.item.historia;
            $scope.json2.tipo_histo = Datos.info.item.tipo_histo;
            $scope.json2.cod_proc = Datos.info.item.cod_proc;
            $scope.json2.idweb = Datos.info.item.idweb;
            $scope.json2.medico = Datos.info.item.medico;
            $timeout(function () {
                try {
                    var rs = false;
                    Service.getCrud({ json: angular.toJson($scope.json), codigo_usuario: $scope.cod_user, json2: angular.toJson($scope.json2) }, "/Admision.svc/CreateOrdenesFisioterapia").then(function (d) {
                        rs = JSON.parse(d.data);
                        if (rs[0] == '?') {
                            alert(rs);
                        }
                        if (rs == true) {
                            $scope.validarfirma();
                            $scope.disabledbtn.imprimir = false;
                        }
                        defender.resolve(rs);
                    });
                } catch (e) {
                    defered.reject(e);
                }
            });

            return promise;
        }
        $scope.imprimir = function () {
            $scope.validarfirma();
            $('#menu').addClass("ss-nav-inactive");
            $('#wrapper').addClass("ss-full-view");
            window.print();
        };
        $scope.actualizar = function () {
            var defender = $q.defer();
            var promise = defender.promise;
            $scope.json2.historia = Datos.info.item.historia;
            $scope.json2.tipo_histo = Datos.info.item.tipo_histo;
            $scope.json2.cod_proc = Datos.info.item.cod_proc;
            $scope.json2.idweb = Datos.info.item.idweb;
            $scope.json2.medico = Datos.info.item.medico;
            $timeout(function () {
                try {
                    var rs = false;
                    Service.getCrud({ json: angular.toJson($scope.json), codigo_usuario: $scope.cod_user }, "/Admision.svc/UpdateOrdenesFisioterapia").then(function (d) {
                        rs = JSON.parse(d.data);
                        if (rs[0] == '?') {
                            alert(rs);
                        }
                        if (rs == true) {
                            $scope.validarfirma();
                            $scope.disabledbtn.imprimir = false;
                        }
                        defender.resolve(rs);
                    });
                } catch (e) {
                    defered.reject(e);
                }
            });

            return promise;
        }
        if (Datos.info.item.estado == 'Realizado') {
            $scope.fecha2 = ssDate.militodate(Datos.info.item.fecha_resultado);
            $scope.hora2 = Datos.info.item.hora_resultado;
            Service.getCrud({ fecha: $scope.fecha2, codigo: $scope.json.codigo, hora: $scope.hora2 }, "/Admision.svc/ReadOrdenesFisioterapia").then(function (d) {
                var j = JSON.parse(d.data);
                console.log(j);
                $scope.json.fecha = ssDate.militodate(j.fecha);
                $scope.json.hora = j.hora;
                $scope.json.evolucion = j.evolucion;
                $scope.json.codigo = j.codigo;
                $scope.json2.medico = j.usuario.trim();
                $scope.json.idweb = j.idweb;
                $scope.disabledbtn.imprimir = false;
                $scope.showbtn.nuevo = false;
                $scope.disabledbtn.nuevo = true;
                $scope.disabledbtn.editar = false;
                $scope.validarfirma();
            });
        }
        else {
            $scope.disabledbtn.imprimir = true;
            $scope.disabledbtn.nuevo = false;
            $scope.disabledbtn.editar = true;
            $scope.disabledbtn.eliminar = true;
        }
    }
    function ReportesLabController($scope, $q, $timeout, ssDate, Datos, Service, Auth) {    
        $scope.json = {};
        $scope.json2 = {};
        $scope.datapanelpacienteprint = {};
        $scope.codigo = Datos.info.censourgencias_paciente;
        $scope.cod_admin = Datos.info.censourgencias_codadmin;
        $scope.mostrar1 = true;
        $scope.mostrar4 = false;
        $scope.nit = Datos.nit;
        $scope.auxuser = Auth.getUserObj().usuario1;
        Service.getCrud({}, "/Admision.svc/PreloadOrdenesMedicas").then(function (d) {
            var data = JSON.parse(d.data);
            $scope.centros = data[0];

        });

        $scope.consultar = function () {
            var rs = [];
            Service.getCrud({ codigo: $scope.codigo }, "/Admision.svc/ReadReporteLab").then(function (d) {
                rs = JSON.parse(d.data);
                if (rs != false) {
                    $scope.tabla = rs;
                }
                $scope.tabla.forEach(function (item, index) {
                    if (item.estado == undefined) {
                        if (item.usuario_reporte != null) {
                            if (item.usuario_reporte.trim() != '') {
                                item.estado = "Realizado";
                            } else if (item.usuario_reporte.trim() == '') {
                                item.estado = "Tomado";
                            }
                        } else {
                            item.estado = "Tomado";
                        }
                    }
                });
            });
        }
        $scope.consultar();
        $scope.convert = function (fecha) {
            return ssDate.militodateString(fecha, 'dd-MM-yyyy');
        }

        $scope.estado = function (t) {
            var aux;
            if (t.estado != undefined) {
                if (t.estado.trim() == '') {
                    aux = "TsinNivel";

                }
                else {
                    if (t.estado.trim() == 'Cumplida') {
                        aux = "Cumplida";
                    }
                    else if (t.estado.trim() == 'Tomado') {
                        aux = "Tomado";
                    }
                    else if (t.estado.trim() == 'Realizado') {
                        aux = "Realizado";
                    }
                }

            }
            if (t.cod_med_rep != undefined) {
                if (t.cod_med_rep.trim() == '') {
                    aux = "Tomado";

                } else {
                    if (t.cod_med_rep.trim() != '') {
                        aux = "Realizado";
                    }
                }
            }
            return aux;
        };

        $scope.seleted = function (item, index) {
            //3console.log(item);
            var estselect;
            Datos.info = {};
            $('#tabla-' + $scope.outseleted).removeClass("seleccion");

            $('#tabla-' + index).addClass("seleccion");

            $scope.outseleted = index;
            Datos.info.item = item;
            console.log(item);
            if(item.tiporesult == 2 ){
                $('#lab1').prop('disabled',true);
                $('#lab2').prop('disabled',true);
                $('#lab1').attr('disabled',true);
                $('#lab2').attr('disabled',true);
                $('#rx1').prop('disabled',false);
                $('#rx1').attr('disabled',false);
            }
            if(item.tiporesult == 1 ){
                $('#rx1').prop('disabled',true);
                $('#rx1').attr('disabled',true);
                $('#lab1').prop('disabled',false);
                $('#lab2').prop('disabled',false);
                $('#lab1').attr('disabled',false);
                $('#lab2').attr('disabled',false);
            }
        }
        $scope.outseleted = '-1';
        $scope.cerrar = function () {
            alert("CERRAR!");
        }
        $scope.val_disa = function (est) {
            if (est.trim() == "Realizado") {
                return false;
            } else {
                return true;
            }
        }
        $scope.und_atencion = function (item) {
            if (item == 3) {
                return "CONSULTA EXTERNA"
            } else if (item == 2) {
                return "URGENCIA"
            } else if (item == 1) {
                return "HOSPITALIZACION"
            }
        }
        $scope.cargarreport2 = function () {
            console.log(Datos.info.item);
            $scope.urlreport = Service.getReport('/LaboratorioClinico?title=Resultado de laboratorio clinico &f1=' + $scope.convert(Datos.info.item.fecha) + '&origen=' + $scope.und_atencion(Datos.info.item.tipo_histo) + '&historia=' + Datos.info.item.historia + '&nit=' + $scope.nit + '&numero=' + Datos.info.item.numero2 + '&centro=' + Datos.info.item.c_costo.trim());
            document.getElementById('reporte').src = $scope.urlreport;
        }
        $scope.imprimir2 = function(){
            $timeout(function(d){
                $scope.cargarreport2();
                $('#myModal').show();
            },500)
        }
        $scope.closed = function(){
            $('#myModal').hide();
        }
        $scope.cargarreport3 = function(){
            $scope.urlreport = Service.getReport('/LaboratorioClinicoInd?title=Resultado de laboratorio clinico &f1=' + $scope.convert(Datos.info.item.fecha) + '&origen=' + $scope.und_atencion(Datos.info.item.tipo_histo) + '&historia=' + Datos.info.item.historia + '&codproc=' + Datos.info.item.cod_proc.trim() + '&nit=' + $scope.nit + '&numero=' + Datos.info.item.numero2);
            document.getElementById('reporte2').src = $scope.urlreport;
        }
        $scope.imprimir3 = function(){
                $timeout(function(d){
                    $scope.cargarreport3();
                    $('#myModalito').show();
                },500)
        }
        $scope.closed2 = function(){
            $('#myModalito').hide();
        }

        /* APOYO DIAGNOSTICO */
        $scope.cargarreport4 = function(){
            if(Datos.info.item.tipo_histo == 1){
                $scope.urlreport = Service.getReport('/ApoyoDiagHosp?historia='+Datos.info.item.historia+'&cod_proc='+Datos.info.item.cod_proc.trim()+'&nit='+$scope.nit);
                document.getElementById('reporte3').src = $scope.urlreport;
            }else if(Datos.info.item.tipo_histo == 2){
                $scope.urlreport = Service.getReport('/ApoyoDiagUrg?historia='+Datos.info.item.historia+'&cod_proc='+Datos.info.item.cod_proc.trim()+'&nit='+$scope.nit);
                 document.getElementById('reporte3').src = $scope.urlreport;
            }else{
                $scope.urlreport = Service.getReport('/ApoyoDiagConsExt?historia='+Datos.info.item.historia+'&cod_proc='+Datos.info.item.cod_proc.trim()+'&nit='+$scope.nit);
                document.getElementById('reporte3').src = $scope.urlreport;
            }
        }
        $scope.imprimir4 = function(){
                $timeout(function(d){
                    $scope.cargarreport4();
                    $('#myModalito2').show();
                },500);
            
        }
        $scope.closed3 = function(){
            $('#myModalito2').hide();
        }
    }
    function RemisionesLabController($scope, $q, $timeout, ssDate, Datos, Service, Auth) {
        $scope.json = {};
        $scope.json2 = {};
        $scope.json3 ={};
        $scope.datapanelpacienteprint = {};
        $scope.mostrar1 = true;
        $scope.mostrar4 = false;
        $scope.tab =[];
        $scope.nit = Datos.nit;
        $scope.openModal = false;
        $scope.verotros = false;
        $scope.valest = true;
        $scope.json.fecharep = new Date();
        $scope.json.tipo_consulta = 1;
        $scope.json.tipo = Datos.info.Parametro;
        if ($scope.json.tipo == undefined) {
            $scope.json.tipo = 1;
        }
        if ($scope.json.tipo == 2) {
            $scope.sh_imp = true;
        } else {
            $scope.sh_imp = false;
        }

        $scope.json.numero = 0;
        $scope.fecha1 = new Date();
        $scope.fecha2 = new Date();
        $scope.json.codpac = '';
        $scope.json.centro = '';
        $scope.json.ingreso = '';
        $scope.valest2 = false;
        $scope.filtro = "1";
        $scope.auxuser = Auth.getUserObj().usuario1;
        $scope.cod_user = Auth.getUserObj().codigo;
        $scope.btnorden = true;
        $scope.cerrarVentana = function () {
            var currentButton = angular.element(document.getElementById('menucerrar'));
            $timeout(function () {
                currentButton.triggerHandler("click");
            });
        }
        $scope.cerrarVentana();
        $scope.filtrar = function (f) {
            if (f == 1) {
                $scope.mostrar1 = true;
                $scope.mostrar2 = false;
                $scope.mostrar3 = false;
                $scope.mostrar4 = false;
                $scope.json.tipo_consulta = 1;
            } else if (f == 2) {
                $scope.mostrar2 = true;
                $scope.mostrar1 = false;
                $scope.mostrar3 = false;
                $scope.mostrar4 = false;
                $scope.json.tipo_consulta = 2;

            } else if (f == 3) {
                $scope.mostrar3 = true;
                $scope.mostrar2 = false;
                $scope.mostrar1 = false;
                $scope.mostrar4 = false;
                $scope.json.tipo_consulta = 3;
            } else if (f == 4) {
                $scope.mostrar4 = true;
                $scope.mostrar2 = false;
                $scope.mostrar1 = false;
                $scope.mostrar3 = false;
                $scope.json.tipo_consulta = 4;
            }
        }
        $scope.openWaiting = false;
        $scope.sh_ordenes = false;
        $scope.orden_show = function () {
            if ($scope.filtro == "1") { $scope.btnorden = true; $scope.btnhist = false; }
            if ($scope.filtro == "2") { $scope.btnorden = false; $scope.btnhist = true; }
        }
        $scope.key2 = { value: '' };
        $scope.$watch('filtro', function (d) {
            $scope.tabla = [];
        });

        Service.getCrud({}, "/Admision.svc/PreloadOrdenesMedicas").then(function (d) {
            var data = JSON.parse(d.data);
            $scope.centros = data[0];
            $scope.est_orden = data[1];
            $scope.labo = data[2];
        });
        Service.getCrud({}, "/Admision.svc/PreloadCentro").then(function (d) {
            var data = JSON.parse(d.data);
			$scope.centro = data[0];
		});
        Service.getCrud({}, "/Generic.svc/DatosEmpresa").then(function (d) {
            var rs = JSON.parse(d.data)[0];
            $scope.sis = rs;
            $scope.nit = $scope.sis.nit;

            Service.getCrud({ nit: $scope.sis.nit }, "/Generic.svc/imgLogo").then(function (d) {
                var rs = JSON.parse(d.data);
                $scope.logo = rs;
                if (rs != null && rs != undefined && rs != '') {
                    $scope.logo = ('data:image/jpeg;base64,' + rs);
                }
            });

        });
        $scope.consultar = function () {
            var rs = [];
            $scope.openWaiting = true;
            $scope.json.fecha1 = $scope.fecha1;
            $scope.json.fecha2 = $scope.fecha2;
            $scope.json.fecha11 = ssDate.dateToString($scope.fecha1, "yyyy/MM/dd");
            $scope.json.fecha22 = ssDate.dateToString($scope.fecha2, "yyyy/MM/dd");
            if ($scope.json.ingreso.length <= 1) {
                $scope.json.ingreso = 0
            } else {
                $scope.json.ingreso = $scope.json.ingreso;
            }
            $scope.consultar_pendiente();
        }

        $scope.consultar2 = function () {
            var rs = [];
            $scope.openWaiting = true;
            $scope.json.fecha1 = $scope.fecha1;
            $scope.json.fecha2 = $scope.fecha2;
            $scope.json.fecha11 = ssDate.dateToString($scope.fecha1, "yyyy/MM/dd");
            $scope.json.fecha22 = ssDate.dateToString($scope.fecha2, "yyyy/MM/dd");
            if ($scope.json.ingreso.length <= 1) {
                $scope.json.ingreso = 0
            } else {
                $scope.json.ingreso = $scope.json.ingreso;
            }
            $scope.consultar_remitidos();
        }

        $scope.r = "";
        $scope.data = sessionStorage.getItem('key');
        $scope.$watch('data', function (d) {
        })

        $scope.$watch('json.esta_orden',function(d){
            if(d != undefined){
                if(d != null){
                    $scope.json3.est = d;
                    if(d =='Pendientes'){
                        $scope.consultar_pendiente();
                        $scope.valest = true;
                    }
                    if(d =='Remitidos'){
                        $scope.valest = false;
                        $scope.consultar_remitidos();
                    }
                }
            }
        });

        $scope.convert = function (fecha) {
            return ssDate.militodateString(fecha, 'dd-MM-yyyy');
        }
        $scope.showviewModalView = { state: false };
        $scope.dataModalView = {
            url: ''
        }
        $scope.estado = function (t) {
            var aux;
            if (t.estado != undefined) {
                if (t.estado.trim() == '') {
                    aux = "TsinNivel";

                }
                else {
                    if (t.estado.trim() == 'Cumplida') {
                        aux = "Cumplida";
                    }
                    else if (t.estado.trim() == 'Tomado') {
                        aux = "Tomado";
                    }
                    else if (t.estado.trim() == 'Realizado') {
                        aux = "Realizado";
                    }
                }

            }
            if (t.cod_med_rep != undefined) {
                if (t.cod_med_rep.trim() == '') {
                    aux = "Tomado";

                } else {
                    if (t.cod_med_rep.trim() != '') {
                        aux = "Realizado";
                    }
                }
            }
            return aux;
        };

        $scope.remicion = function(item){
            if(item != null){
                if($scope.tab.length > 0){
                    if($scope.tab != null){
                        $scope.tab.forEach(function (it, ind){
                            console.log(it);
                            if(item.cod_proc != it.cod_proc){
                                $scope.tab.push(item);
                            }else{
                                if(item.historia != it.historia){
                                    $scope.tab.push(item);
                                }else{
                                    if(item.numero != it.numero){
                                        $scope.tab.push(item);
                                    }else{
                                        alert("YA ESTE PROCEDIMIENTO HA SIDO ESCOGIDO!");
                                    }
                                }
                            }
                        });
                    }

                }else{
                    $scope.tab.push(item);
                }
            }
        }

        $scope.finrem = function(){
            if($scope.labora != undefined){
                    $scope.remPasteur($scope.labora);
            }else{
                alert("Debe escoger un laboratorio");
            }
        }

        $scope.devolver = function(item){
            if(item != null){
                console.log(item);
                $scope.proc = item.cod_proc.trim();
                $scope.code = item.codigo.trim();
                $scope.histo = item.historia+"";
                var rs = false;
                Service.getCrud({ cod_proc:$scope.proc, codigo: $scope.code, historia: $scope.histo, codigo_usuario: $scope.cod_user}, "/Admision.svc/DeleteRemisionLab").then(function (d) {
                    rs = JSON.parse(d.data);
                    if (rs[0] == '?') {
                        alert(rs);
                    }
                    if (rs == true) {
                        $scope.openModal = false;
                        $scope.consultar_remitidos();
                    }
                });
            }
        }

        $scope.close = function(){
            $scope.openModal = false;
        }

        $scope.seleted = function (item, index) {
            var estselect;
            Datos.info = {};
            $('#tabla-' + $scope.outseleted).removeClass("seleccion");

            $('#tabla-' + index).addClass("seleccion");

            $scope.outseleted = index;
            Datos.info.item = item;
        }

        $scope.outseleted = '-1';

        $scope.open_rs = function () {
            if (Datos.info.item.numero != undefined && Datos.info.item.numero != null) {
                if (Datos.info.item.estado == "Tomado" || Datos.info.item.estado == undefined || Datos.info.item.estado == "Realizado") {
                    $scope.showviewModalView.state = undefined;
                    Datos.info.tipo = $scope.json.tipo;
                    Datos.info.filtro = $scope.filtro;
                    $timeout(function () {
                        if ($scope.json.tipo == 1) {
                            $scope.dataModalView.url = 'js/app/Modulo/Laboratorio Clinico/Transcipcion.html';
                        } else if ($scope.json.tipo == 2) {
                            $scope.dataModalView.url = 'js/app/Modulo/Salud/Admisiones/ResultadoOrdenesDiagnosticas.html';
                        } else if ($scope.json.tipo == 3) {
                            $scope.dataModalView.url = 'js/app/Modulo/Laboratorio Clinico/Orden_Fisioterapia.html';
                        } else if ($scope.json.tipo == 4) {
                            $scope.dataModalView.url = 'js/app/Modulo/Laboratorio Clinico/resultadocitologia.html';
                        } else if ($scope.json.tipo == 6) {
                            $scope.dataModalView.url = 'js/app/Modulo/Salud/Historias/Histo_Espermograma.html';
                        }

                        $scope.showviewModalView.state = true;
                    });
                    $scope.showviewModalView.state = undefined;
                    $scope.dataModalView.url = "";
                } else {
                    alert("LA ORDEN NO HA SIDO TOMADA")
                }
            } else {
                alert("DEBE SELECCIONAR UN REGISTRO");
            }
        }

        $scope.cerrar = function () {
            $timeout(function () {
                var currentButton = angular.element(document.getElementById('salud-system'));
                $timeout(function () {
                    $('#salud-system').click();
                    currentButton.triggerHandler("salud-system");
                });
                Datos.info = {};
            });
        }

        $scope.remOtros = function(){
            //console.log("OTROS");
            $scope.verotros = false;
            $scope.json3.laboratorio = "OTRO";
            $scope.tabla.forEach(function(itm,index){
                if(itm.historia == $scope.json3.historia && itm.numero2 == $scope.json3.numero && itm.cod_proc.trim() == $scope.json3.cod_proc){
                    $scope.json3.codigo = itm.codigo.trim();
                    $scope.json3.cod_admin = itm.cod_admin.trim();
                    $scope.json3.fecha_orden = ssDate.militodate(itm.fecha);
                    $scope.json3.nom_proc = itm.servicio.trim();
                    $scope.json3.usuario = $scope.auxuser;
                    $scope.json3.numero = itm.numero;
                    var rs = false;
                    Service.getCrud({ json: angular.toJson($scope.json3), codigo_usuario: $scope.cod_user}, "/Admision.svc/CreateRemisionLab").then(function (d) {
                        rs = JSON.parse(d.data);
                        if (rs[0] == '?') {
                            alert(rs);
                        }
                        if (rs == true) {
                            $scope.openModal = false;
                            $scope.consultar_pendiente();
                        }
                    });
                }
            });
        }

        $scope.remPasteur = function(lab){
            $scope.verotros = false;
            $scope.json3.laboratorio = lab.toUpperCase();
            console.log("ENTRO2");
                    $scope.tab.forEach(function(i,ind){
                        console.log("ENTRO3");
                        console.log($scope.tab);
                        $scope.json3.historia = i.historia;
                        $scope.json3.numero = i.numero2;
                        $scope.json3.cod_proc = i.cod_proc.trim();
                        $scope.json3.codigo = i.codigo.trim();
                        $scope.json3.cod_admin = i.cod_admin.trim();
                        $scope.json3.cod_proc = i.cod_proc.trim();
                        $scope.json3.fecha_orden = ssDate.militodate(i.fecha);
                        $scope.json3.nom_proc = i.servicio.trim();
                        $scope.json3.ccosto = i.c_costo.trim();
                        console.log(i);
                        $scope.json3.usuario = $scope.auxuser;
                        var rs = false;
                        Service.getCrud({ json: angular.toJson($scope.json3), codigo_usuario: $scope.cod_user}, "/Admision.svc/CreateRemisionLab").then(function (d) {
                            rs = JSON.parse(d.data);
                            console.log("ENTRO4");
                            if (rs[0] == '?') {
                                alert(rs);
                            }
                            if (rs == true) {
                                $scope.openModal = false;
                                $scope.consultar_pendiente();
                            }
                        });
                    });  
        }
        
        $scope.consultar_pendiente = function () {
            var rs = [];
            $scope.tab = [];
            $scope.valest = true;
            $scope.valest2 = false;
            $scope.openWaiting = true;
            $scope.json.fecha1 = $scope.fecha1;
            $scope.json.fecha2 = $scope.fecha2;
            $scope.json.fecha11 = ssDate.dateToString($scope.fecha1, "yyyy/MM/dd");
            $scope.json.fecha22 = ssDate.dateToString($scope.fecha2, "yyyy/MM/dd");
            if ($scope.json.ingreso.length <= 1) {
                $scope.json.ingreso = 0
            } else {
                $scope.json.ingreso = $scope.json.ingreso;
            }

        Service.getCrud({ json: angular.toJson($scope.json), filtro: $scope.filtro }, "/Admision.svc/ReadOrdenesMedicasPendientes").then(function (d) {
                rs = JSON.parse(d.data);
                $scope.openWaiting = false;
                if (rs != false) {
                    $scope.tabla = rs;
                }
                if (rs == false || rs.length == 0) {
                    alert("LA CONSULTA NO GENERA SALIDA");
                }
                if ($scope.filtro == "1") {
                    $scope.tabla.forEach(function (item, index) {
                        if (item.estado == undefined) {
                            if (item.usuario_reporte != null) {
                                if (item.usuario_reporte.trim() != '') {
                                    item.estado = "Realizado";
                                } else if (item.usuario_reporte.trim() == '') {
                                    item.estado = "Tomado";
                                }
                            } else {
                                item.estado = "Tomado";
                            }
                        }
                    });
                } else {
                    $scope.tabla.forEach(function (item, index) {
                        if (item.estado == undefined) {
                            if (item.cod_med_rep != null) {
                                if (item.cod_med_rep.trim() != '') {
                                    item.estado = "Realizado";
                                } else if (item.cod_med_rep.trim() == '') {
                                    item.estado = "Tomado";
                                }
                            } else {
                                item.estado = "Tomado";
                            }
                        }
                    });
                }
        });
        }

        $scope.consultar_remitidos = function () {
            var rs = [];
            $scope.tab = [];
            $scope.valest = false;
            $scope.valest2 = true;
            $scope.openWaiting = true;
            $scope.json.fecha1 = $scope.fecha1;
            $scope.json.fecha2 = $scope.fecha2;
            $scope.json.fecha11 = ssDate.dateToString($scope.fecha1, "yyyy/MM/dd");
            $scope.json.fecha22 = ssDate.dateToString($scope.fecha2, "yyyy/MM/dd");
            if ($scope.json.ingreso.length <= 1) {
                $scope.json.ingreso = 0
            } else {
                $scope.json.ingreso = $scope.json.ingreso;
            }

        Service.getCrud({ json: angular.toJson($scope.json), filtro: $scope.filtro }, "/Admision.svc/ReadOrdenesMedicasRemitidos").then(function (d) {
                rs = JSON.parse(d.data);
                $scope.openWaiting = false;
                if (rs != false) {
                    $scope.tabla = rs;
                }
                if (rs == false || rs.length == 0) {
                    alert("LA CONSULTA NO GENERA SALIDA");
                }
                if ($scope.filtro == "1") {
                    $scope.tabla.forEach(function (item, index) {
                        if (item.estado == undefined) {
                            if (item.usuario_reporte != null) {
                                if (item.usuario_reporte.trim() != '') {
                                    item.estado = "Realizado";
                                } else if (item.usuario_reporte.trim() == '') {
                                    item.estado = "Tomado";
                                }
                            } else {
                                item.estado = "Tomado";
                            }
                        }
                    });
                } else {
                    $scope.tabla.forEach(function (item, index) {
                        if (item.estado == undefined) {
                            if (item.cod_med_rep != null) {
                                if (item.cod_med_rep.trim() != '') {
                                    item.estado = "Realizado";
                                } else if (item.cod_med_rep.trim() == '') {
                                    item.estado = "Tomado";
                                }
                            } else {
                                item.estado = "Tomado";
                            }
                        }
                    });
                }
        });
        }

        /* REPORTES */
        $scope.cargarreport2 = function(){
            $scope.urlreport = Service.getReport('/RemisionesLab?f1='+ssDate.dateToString($scope.json.fecharep, 'yyyyMMdd')+'&nit='+$scope.nit);
            document.getElementById('reporte').src = $scope.urlreport;
        }
        $scope.imprimir2 = function(){
            $timeout(function(d){
                $scope.cargarreport2();
                $('#myModal').show();
            },500)
        }
        $scope.closed = function(){
            $('#myModal').hide();
        }
        $scope.cargarreport3 = function(){
            $scope.urlreport2 = Service.getReport('/RemisionesLabCodigo?codigo='+$scope.json.cod_rep+'&nit='+$scope.nit+'&fe1='+ssDate.dateToString($scope.json.fecharep, 'yyyyMMdd'));
            document.getElementById('reporte2').src = $scope.urlreport2;
        }
        $scope.imprimir3 = function(){
            if($scope.json.cod_rep != undefined){
                $timeout(function(d){
                    $scope.cargarreport3();
                    $('#myModal2').show();
                },500)
            }else{
                alert("Debe escoger un paciente valido!");
                $('#brp2-clave').focus();
            }
           
        }
        $scope.closed2 = function(){
            $('#myModal2').hide();
        }
        $scope.cargarreport4 = function(){
            $scope.urlreport = Service.getReport('/RemisionesLabHistoria?historia='+$scope.json.historep+'&nit='+$scope.nit+'&fe1='+ssDate.dateToString($scope.json.fecharep, 'yyyyMMdd'));
            document.getElementById('reporte3').src = $scope.urlreport;
        }
        $scope.imprimir4 = function(){
            console.log($scope.json.historep);
            if($scope.json.historep != undefined){
                $timeout(function(d){
                    $scope.cargarreport4();
                    $('#myModal3').show();
                },500)
            }else{
                alert("Debe escoger un numero de historia valido!");
                $('#historep').focus();
            }
        }
        $scope.closed3 = function(){
            $('#myModal3').hide();
        }
        $scope.cargarreport5 = function(){
            $scope.urlreport = Service.getReport('/RemisionesLabCentro?centro='+$scope.json.ccosto+'&nit='+$scope.nit+'&fe1='+ssDate.dateToString($scope.json.fecharep, 'yyyyMMdd'));
            document.getElementById('reporte4').src = $scope.urlreport;
        }
        $scope.imprimir5 = function(){
            if($scope.json.ccosto != undefined){
                $timeout(function(d){
                    $scope.cargarreport5();
                    $('#myModal4').show();
                },500)
            }else{
                alert("Debe escoger un centro de costo valido!");
                $('#ccosto').focus();
            }
        }
        $scope.closed4 = function(){
            $('#myModal4').hide();
        }

        /*BUSQUEDA RAPIDA PACIENTE*/
        $scope.dataPaciente = {
            id: 'brPaciente',
            component: [{ id: 'brp', type: '' }],
            table: 'paciente',
            column: [
                { name: 'CODIGO', as: "'No. Documento'", visible: true },
                { name: 'PPELLIDO', as: "'1er Apellido'", visible: true },
                { name: 'SAPELLIDO', as: "'2do Apellido'", visible: true },
                { name: 'NOMBRE', as: "'1er Nombre'", visible: true },
                { name: 'SNOMBRE', as: "'2do Nombre'", visible: true },
                { name: 'nivel_afil', as: "'Nivel'", visible: false },
                { name: 'telefono', as: "'Telefono'", visible: false },
            ],
            where: {},
            groupby: false,
            orderby: ['CODIGO desc', 'PPELLIDO desc'],
            title: 'Buscar pacientes',
            required: false
        };
        $scope.rsPaciente = function (rs) {
            $scope.openModal = false;
            var j = JSON.parse(rs);
            if (!jQuery.isEmptyObject(j)) {
                //$scope.json.codigo = j["No. Documento"];
                $scope.json.codpac = j["No. Documento"];
                $scope.nombrepaciente = j["1er Nombre"] + " " + j["2do Nombre"] + " " + j["1er Apellido"] + " " + j["2do Apellido"];
                //$scope.nivel = j["Nivel"];
                //$scope.tel = j["Telefono"];
                //$scope.json.centro_costo = 'CE';
                //$scope.causa_ext = '13- ENFERMEDAD GENERAL';
            }

        }
        /* PACIENTE 2 */
        $scope.dataPaciente2 = {
            id: 'brPaciente2',
            component: [{ id: 'brp2', type: '' }],
            table: 'paciente',
            column: [
                { name: 'CODIGO', as: "'No. Documento'", visible: true },
                { name: 'PPELLIDO', as: "'1er Apellido'", visible: true },
                { name: 'SAPELLIDO', as: "'2do Apellido'", visible: true },
                { name: 'NOMBRE', as: "'1er Nombre'", visible: true },
                { name: 'SNOMBRE', as: "'2do Nombre'", visible: true },
                { name: 'nivel_afil', as: "'Nivel'", visible: false },
                { name: 'telefono', as: "'Telefono'", visible: false },
            ],
            where: {},
            groupby: false,
            orderby: ['CODIGO desc', 'PPELLIDO desc'],
            title: 'Buscar pacientes',
            required: false
        };
        $scope.rsPaciente2 = function (rs) {
            $scope.openModal = false;
            var j = JSON.parse(rs);
            if (!jQuery.isEmptyObject(j)) {
                $scope.json.cod_rep = j["No. Documento"];
            }

        }

        $scope.val_disa = function (est) {
            if (est.trim() != "Realizado") {
                return false;
            } else {
                return true;
            }
        }

        $scope.und_atencion = function (item) {
            if (item == 3) {
                return "CONSULTA EXTERNA"
            } else if (item == 2) {
                return "URGENCIA"
            } else if (item == 1) {
                return "HOSPITALIZACION"
            }
        }
    }
    function PanelFisioController($scope, $q, $timeout, ssDate, Datos, Service, Auth,Paciente,$sce){
        $scope.json = {};              
        $scope.menu = [
            { name: 'Terapia Fisica ', url: 'js/app/Modulo/Salud/Historias/Histo_Fisioterapia.html', icon: '../../../img/tooth.png', estado: function () { return false; } },
            { name: 'Evolucion Terapia Fisica', url: 'js/app/Modulo/Salud/Historias/TerapiaFisicaEvo.html', icon: '../../../img/tooth.png', estado: function () { return false; } },
            { name: 'Terapia Respiratoria ', url: 'js/app/Modulo/Salud/Historias/TerapiaRespiratoria.html', icon: '../../../img/tooth.png', estado: function () { return false; } },
            { name: 'Evolucion Terapia Respiratoria', url: 'js/app/Modulo/Salud/Historias/TerapiaRespiratoriaEvo.html', icon: '../../../img/tooth.png', estado: function () { return false;} },
            { name: 'Recomendaciones', url: 'js/app/Modulo/Salud/Citas_medicas/OtrasOrdenesMedicas.html', icon: '../../../img/otras_ord_med.png', estado: function () { return false; } },
            { name: 'Orden Farmaceutica', url: 'js/app/Modulo/Salud/Citas_medicas/OrdenesFarmaceuticas.html', icon: '../../../img/orden_farmaceutica.png', estado: function () { return false;} },
            { name: 'Ordenes de Servicios', url: 'js/app/Modulo/Salud/Citas_medicas/OrdenesDiag.html', icon: '../../../img/orden_diag.png', estado: function () { return false; } },
            { name: 'Incapacidades & Licencias de Maternidad', url: 'js/app/Modulo/Salud/Citas_medicas/Incapacidades.html', icon: '../../../img/incapacidad.png', estado: function () { return false; } },
            { name: 'Interconsulta', url: 'js/app/Modulo/Salud/Citas_medicas/InterConsultas.html', icon: '../../../img/interconsulta.png', estado: function () { return false; } },
        ];
            $scope.views = [];
            $scope.list = [];
            $scope.isSelected = true;
            $scope.find = false;
            $scope.orden = true;
            $scope.orden2 = false;
            $scope.showatendido = false;
            $scope.MostrarNoAtendido = true;
            $scope.MostrarAtendido = false;
            $scope.message = '';
            $scope.activeView = 'tabMain';
            $scope.startRegAnt = function () {  
                        console.log(Paciente);
                        Paciente.hora = ssDate.dateToString(new Date(),'HH:mm');
                        Paciente.fecha = new Date();
                        $scope.json.paciente = Paciente.sexo;   
                        $scope.reg=true;
                        var columns=[{ name: 'id', visible: false }, { name: 'Tipo', visible: true }, { name: 'Fecha', visible: true }, { name: 'Hora', visible: true }, { name: 'Sesiones', visible: true }];
                        Service.getCrud({ codigo: Paciente.codigo }, "/Salud.svc/RegAntFisioterapia").then(function (d) {
                            $scope.tablaregant = JSON.parse(d.data);
                            var GetHeaders = function () {
                                var cols = "";
                                for (var i in columns) {
                                    cols += "<th>" + columns[i].name + "</th>";
                                }
                                return cols;
                            }
                            $scope.t_head = $sce.trustAsHtml(GetHeaders('th'));
                            var body = GetTbody($scope.tablaregant);
                            $scope.t_body = $sce.trustAsHtml(body);
                            function GetTbody(obj) {
                                var cont = "";
                                for (var d in obj) {
                                    var p = obj[d];
                                    var a = 0;
                                    cont += "<tr>";

                                    for (var i in p) {

                                        var col = obj[a];
                                        cont += "<td>" + p[i] + "</td>";
                                        a++;
                                    }
                                    cont += "</tr>";
                                }

                                return cont;
                            }

                        })                                                
            }
            $scope.startRegAnt();
            $scope.setActiveView = function (d) {
                console.log(d);
                $scope.activeView = d;
            }
            $scope.validar = function () {
                if (!jQuery.isEmptyObject(Paciente)) {
                    $scope.setPaciente({ 'Paciente': Paciente });
                }
            }
            $scope.openModal2 = false;
            $scope.open2 = function () {
                $scope.openModal2 = true;

            }
            $scope.close2 = function () {
                $scope.openModal2 = false;
            }
            $scope.varAtendido = 0;
            $scope.openModal = false;
            $scope.open = function () {
                $scope.openModal = true;
            }
            $scope.close = function () {
                $scope.openModal = false;
                Datos.po_ordenesdx=[];
                Datos.po_ordenesdx2=[];
                Datos.po_ordenesqx=[];
                Datos.po_ordenesfarm=[];
                Datos.po_intercons=[];
                $scope.grilla = [];
                $scope.grillaalergias = [];
                $scope.grillaordenesquirurgicas = [];
                $scope.grillaaux = [];
                $scope.grillaedad = [];
                $scope.grillatalla = [];
                $scope.grillatallapeso = [];
                $scope.grillaInter = [];
                $scope.grilla_print = [];
                $scope.grilla_print2 = [];
                $scope.viaaux = [];
                $scope.grilla2 = [];
                $scope.json_nom_serv = {};
                $scope.jsonarray = [];
                $scope.jsonarray2 = [];
                $scope.jsonarray_serv = [];
                $scope.via = [];
                $scope.frecuencia = [];
                $scope.duracion = [];
                $scope.tabla = [];
            }
            $scope.addView = function (v) {
                $scope.isOpen = false;
                $scope.views.forEach(function (item, index) {
                    if (item.name == v.name) {
                        $scope.isOpen = true;
                        return;
                    }
                });
                if (!$scope.isOpen) {
                    $scope.views.push(v);
                    $scope.setActiveView(v.name);
                    $scope.isOpen = false;
                }
            }
            $scope.closeview = function (v) {
                $scope.views.forEach(function (item, index) {
                    if (v == item.name) {
                        if ($scope.views.length > 1) {
                            if (index == $scope.views.length - 1) {
                                $scope.activeView = $scope.views[index - 1].name;
                            } else {
                                $scope.activeView = $scope.views[index + 1].name;
                            }
                        }
                        $scope.views.splice(index, 1);
                        if ($scope.views.length == 0) {
                            $scope.activeView = "tabMain";
                        }
                    }
                });
            }
            $scope.datosUsuario = Auth.getUserObj();
            
            $scope._paciente = Paciente;
            $scope.cerrar = function () {
                var currentButton = angular.element(document.getElementById('salud-system'));
                $timeout(function () {
                    $('#salud-system').click();
                    currentButton.triggerHandler("click");
                });
            }
    } 
    function ConceptosController($scope, Service, Auth, $q, Paciente, ssDate, $timeout) {
        $scope.json = {};
        $scope.cod_user = Auth.getUserObj().codigo
        $scope.grilla = [];

        $scope.seleccionarConcepto = function (d) {
            console.log(d);
            if (d.id != null) {
                $scope.json.id = d.id;
                $scope.json.codigo = d.codigo;
                $scope.json.valor = d.nombre;
                $scope.json.horas = parseInt(d.tiempo);
                
                $scope.disabledbtn.eliminar = false;
            }
        }

        

        $scope.adicionar = function () {
            $scope.grilla.push($scope.json);
            $scope.json = {};
        };
        $scope.deleteGrilla = function (i) {
            $scope.grilla.splice($scope.grilla[i], 1);
        }
        $scope.auxuser = Auth.getUserObj().usuario1;

        $scope.profilenivel = angular.toJson({ modulo: 'AGENDA MEDICA', ventana: '7', usuario: $scope.auxuser });
        /*PANEL PACIENTE*/
        $scope.showbtn = { buscar: true, imprimir: false, nuevo: true, editar: false, eliminar: true };
        $scope.disabledbtn = { buscar: false, imprimir: true, nuevo: false, editar: true, eliminar: true };

        /*aqui crud --> ver siguiente ejemplo*/
        $scope.nuevo = function () {
            $scope.json.id = "";
            $scope.json.codigo = "";
            $scope.json.valor = "";
            $scope.json.horas = "";
        }
        $scope.refresh = function () {
            Service.getCrud({}, "/Salud2.svc/ReadAllConceptos").then(function (d) {
                $scope.tabla = JSON.parse(d.data);
            });
        }
        Service.getCrud({}, "/Salud2.svc/ReadAllConceptos").then(function (d) {
            $scope.tabla = JSON.parse(d.data);
        });

        $scope.cerrar = function () {
            var currentButton = angular.element(document.getElementById('salud-system'));
            $timeout(function () {
                $('#salud-system').click();
                currentButton.triggerHandler("click");
            });
        };

        $scope.guardar = function () {
            $scope.json.horas = $scope.json.horas + "";
            var defender = $q.defer();
            var promise = defender.promise;
            if (jQuery.isEmptyObject($scope.grilla)) {
                alert("Por Favor Agregar Concepto");
                defender.reject(false);
            } else {
                $timeout(function () {
                    try {
                        var rs = false;
                        Service.getCrud({ grilla: angular.toJson($scope.grilla), codigo_usuario: $scope.cod_user }, "/Salud2.svc/CreateConceptos").then(function (d) {
                            rs = JSON.parse(d.data);
                            defender.resolve(rs);
                            if (rs == false) {
                                defered.reject(rs);
                                $scope.refresh();
                            }
                            if (rs == true) {
                                $scope.grilla = [];
                                $scope.ref = false;
                                $scope.refresh();
                                
                            }
                        });
                    } catch (e) {
                        defered.reject(e);
                    }
                });
                return promise;
            }
            $scope.actualizar = function () {
                var defender = $q.defer();
                var promise = defender.promise;
                $timeout(function () {
                    try {
                        var rs = false;
                        Service.getCrud({ json: angular.toJson($scope.json), codigo_usuario: $scope.cod_user }, "/Salud2.svc/UpdateConceptos").then(function (d) {
                            rs = JSON.parse(d.data);
                            defender.resolve(rs);
                        });
                    } catch (e) {
                        defered.reject(e);
                    }
                }, 3000);
            }
            return promise;
        };

        $scope.rsConceptos = function (v) {
            $scope.id = JSON.parse(v).Id;
            if ($scope.id != '') {
                Service.getCrud({ id: parseInt($scope.id) }, "/Salud2.svc/ReadConceptos").then(function (d) {
                    $scope.json = JSON.parse(d.data);
                    $scope.json.horas = parseInt($scope.json.horas);
                    $scope.disabledbtn.editar = false;
                    $scope.disabledbtn.eliminar = false;
                });
            }
        };
        $scope.dataConceptos = {
            id: 'bvia',
            component: [{ id: 'Conceptos-buscar', type: 'btn' }],
            table: 'Conceptos_Cama',
            column: [
                { name: 'codigo', as: 'codigo', visible: true },
                { name: 'combre', as: 'Valor', visible: true },
                { name: 'tiempo', as: 'horas', visible: false }
            ],
            where: {},
            groupby: false,
            orderby: [],
            title: 'Busqueda de Conceptos'
        };



        /*  eliminar*/
        $scope.eliminar = function () {
            var defender = $q.defer();
            var promise = defender.promise;
            $timeout(function () {
                try {
                    var rs = false;
                    Service.getCrud({ id: parseInt($scope.json.id), codigo_usuario: $scope.cod_user }, "/Salud2.svc/DeleteConceptos").then(function (d) {
                        rs = JSON.parse(d.data);
                        defender.resolve(rs);
                        if (rs == true) {
                            $scope.ref = false;
                            $scope.refresh();
                        }
                    });
                } catch (e) {
                    defered.reject(e);
                }
            });
            return promise;
        };
        //validar
        $scope.ValidarConceptoCodigo = function (d) {
            if (d != null && d != undefined && d != "") {
                Service.getCrud({ codigo: d }, '/Salud2.svc/ValidarConceptoCodigo').then(function (d) {
                    var rs = JSON.parse(d.data);
                    if (rs == true) {
                        alert("El codigo ya existe");
                        $scope.json = {};
                    }
                });
            }
        }
        $scope.ValidarConceptoNombre = function (d) {
            if (d != null && d != undefined && d != "") {
                Service.getCrud({ valor: d }, '/Salud2.svc/ValidarConceptoNombre').then(function (d) {
                    var rs = JSON.parse(d.data);
                    if (rs == true) {
                        alert("El Nombre ya existe");
                        $scope.json = {};
                    }
                });
            }
        }
        $scope.ValidarConceptoHoras = function (d) {
            if (d != null && d != undefined && d != "") {
                console.log(d);
                 if (d > 24 || d < 0) {
                      alert("La hora sobrepasa el limite de tiempo");
                      $scope.json.horas = 0;
                 }
                
            }
        }
    }
    function ReservarCamaController($scope, Service, Auth, $q, Paciente, ssDate, $timeout) {
        //inicializar//
        $scope.json = {};
        $scope.servicios = [];
        $scope.json.conceptos = [];
        $scope.tabla = [];
        $scope.cod_user = Auth.getUserObj().codigo;
        $scope.json.usuario = $scope.cod_user;
        $scope.auxuser = Auth.getUserObj().usuario1;
        $scope.key = { value: '' };
       
        $scope.profilenivel = angular.toJson({ modulo: 'AGENDA MEDICA', ventana: '7', usuario: $scope.auxuser });
        /*PANEL PACIENTE*/
        $scope.showbtn = { buscar: false, imprimir: false, nuevo: true, editar: false, eliminar: true };
        $scope.disabledbtn = { buscar: true, imprimir: true, nuevo: false, editar: true, eliminar: true };


        //cargarConceptos//
        Service.getCrud({}, "/Salud2.svc/PreloadConceptos").then(function (d) {
            $scope.conceptos = JSON.parse(d.data)[0];
            $scope.servicios = JSON.parse(d.data)[1];
            
            console.log($scope.conceptos);
        });
       
        /*aqui crud --> ver siguiente ejemplo*/
        //limpiar para nuevo//
        $scope.nuevo = function () {
            $scope.auxuser1 = "";
            $scope.id = "";
            $scope.json.cama = "";
            $scope.json.servicio = "";
            
            $scope.json.fecha = "";
            $scope.json.horas = "";
            $scope.json.conceptos = "";
            $scope.json.tiempo = "";
            $scope.camasel = "";
            $timeout(function () {
                
                $scope.json.usuario = Auth.getUserObj().codigo;
                $scope.auxuser1 = $scope.auxuser;
            })
        }
        //refrescar//
        $scope.refresh = function () {
            Service.getCrud({}, "/Salud2.svc/ReadAllReserva").then(function (d) {
                $scope.tabla = JSON.parse(d.data);
                console.log($scope.tabla);
            });
        }
        //BR PACIENTE
        $scope.dataBuscador_Paciente = {
            id: 'buscadorPaciente',
            component: [{ id: 'brPaciente', type: '' }],
            table: 'paciente',
            column: [
                { name: 'CODIGO', as: 'Documento', visible: true },
                { name: 'RTRIM(LTRIM(PPELLIDO))', as: 'P_Apellido', visible: true },
                { name: 'RTRIM(LTRIM(SAPELLIDO))', as: 'S_Apellido', visible: true },
                { name: 'RTRIM(LTRIM(NOMBRE))', as: 'Nombre', visible: true },
                { name: 'RTRIM(LTRIM(SNOMBRE))', as: 'S_Nombre', visible: true }
            ],
            where: [],
            groupby: false,
            orderby: [],
            title: 'Buscar Paciente'
        };
        $scope.resultadoBRPaciente = function (d) {
            var j = JSON.parse(d);
            $scope.json.codigo_pac = j.Documento;
        }

        $scope.nomcon = function (r) {

            if (r != undefined && r != null) {
                $scope.conceptos.forEach(function (item, index) {
                    
                    if (item.FK_conceptos == r) {
                         console.log(r);
                        //console.log(item.nombre);
                        //console.log(item.codigo);
                        //console.log(item.nombre.trim());
                        return item.nombre;
                    }
                });
            }
        }

        //cagar registros//
        Service.getCrud({}, "/Salud2.svc/ReadAllReserva").then(function (d) {
            $scope.tabla = JSON.parse(d.data);
            console.log($scope.tabla);
        });



         //guardar//
        $scope.guardar = function () {
            $scope.json.horas = $scope.json.horas + "";
            var defender = $q.defer();
            var promise = defender.promise;
            if (jQuery.isEmptyObject($scope.json)) {
                alert("Por Favor Agregar Reserva");
                defender.reject(false);
            } else {
                $timeout(function () {
                    try {
                        var rs = false;
                        Service.getCrud({ json: angular.toJson($scope.json), codigo_usuario: $scope.cod_user }, "/Salud2.svc/CreateReserva").then(function (d) {
                            rs = JSON.parse(d.data);
                            defender.resolve(rs);
                            if (rs == false) {
                                defered.reject(rs);
                                $scope.refresh();
                            }
                            if (rs == true) {
                                $scope.json = {};
                                $scope.ref = false;
                                $scope.refresh();
                                $scope.camasel = "";

                            }
                        });
                    } catch (e) {
                        defered.reject(e);
                    }
                });
                return promise;
            }

            $scope.actualizar = function () {
                var defender = $q.defer();
                var promise = defender.promise;
                $timeout(function () {
                    try {
                        var rs = false;
                        Service.getCrud({ json: angular.toJson($scope.json), codigo_usuario: $scope.cod_user }, "/Salud2.svc/UpdateConceptos").then(function (d) {
                            rs = JSON.parse(d.data);
                            defender.resolve(rs);
                            $scope.camasel = "";
                        });
                    } catch (e) {
                        defered.reject(e);
                    }
                }, 3000);
            }
            return promise;
        };

        $scope.rsConceptos = function (v) {
            $scope.id = JSON.parse(v).Id;
            if ($scope.id != '') {
                Service.getCrud({ id: parseInt($scope.id) }, "/Salud2.svc/ReadConceptos").then(function (d) {
                    $scope.json = JSON.parse(d.data);
                    $scope.json.horas = parseInt($scope.json.horas);
                    $scope.disabledbtn.editar = false;
                    $scope.disabledbtn.eliminar = false;
                });
            }
        };
        $scope.dataConceptos = {
            id: 'bvia',
            component: [{ id: 'Conceptos-buscar', type: 'btn' }],
            table: 'Conceptos_Cama',
            column: [
                { name: 'codigo', as: 'codigo', visible: true },
                { name: 'combre', as: 'Valor', visible: true },
                { name: 'tiempo', as: 'horas', visible: false }
            ],
            where: {},
            groupby: false,
            orderby: [],
            title: 'Busqueda de Conceptos'
        };

        //SelecionarReserva//
        $scope.seleccionarReserva = function (d) {
            console.log(d);
            if (d.id != null) {
               
                $scope.json.id = d.id;
                $scope.json.servico = d.servico;
                $scope.json.usuario = d.usuario;
                $scope.json.fecha = new Date(d.fech);
                $scope.json.hora = d.hora;
                $scope.json.FK_conceptos = d.FK_conceptos;
                $scope.json.tiempo = parseInt(d.tiempo);
                $scope.camasel = d.nom_cama;
                $scope.json.usuario = Auth.getUserObj().codigo;
                $scope.auxuser1 = $scope.auxuser;
                $scope.key.value = d.codigo_pac;

                $scope.disabledbtn.eliminar = false;
            }
        }
        
        //  eliminar
        $scope.eliminar = function () {
            var defender = $q.defer();
            var promise = defender.promise;
            $timeout(function () {
                try {
                    var rs = false;
                    Service.getCrud({ id: parseInt($scope.json.id), codigo_usuario: $scope.cod_user }, "/Salud2.svc/DeleteReserva").then(function (d) {
                        rs = JSON.parse(d.data);
                        defender.resolve(rs);
                        if (rs == true) {
                            $scope.ref = false;
                            $scope.refresh();
                            $scope.camasel = "";
                        }
                    });
                } catch (e) {
                    defered.reject(e);
                }
            });
            return promise;
        };


        //modal//
        $scope.closed = function () {
            $('#myModal').hide();
        }
        $scope.listar = function () {
            if ($scope.camas.length > 0) {
                $('#myModal').show();
            } else {
                alert("NO EXISTEN CAMAS DISPONIBLES CON EL SERVICIO SELECCIONADO");
            }
        }

        $scope.$watch('json.servico', function (d) {
            if (d != undefined) {
                $scope.showhabitaciones = false;
                $scope.json.habitacion = '';
                $scope.json.hosp = 0;
                $scope.cent_ing = d;
                $scope.camas = [];
                Service.getCrud({ centro: $scope.json.servico }, "/Admision.svc/ReadCamas").then(function (d) {
                    $scope.camas = JSON.parse(d.data);
                    if ($scope.camas.length > 0) {
                        
                    } else {
                        alert("NO EXISTEN CAMAS DISPONIBLES CON EL SERVICIO SELECCIONADO");
                    }
                });
            }
        });
            //seleccionarCama
        $scope.seleccionarCama = function (x) {
            if (x != undefined && x != null) {
                $scope.json.FK_cama = x.codigo.trim();
                $scope.camasel = x.codigo.trim() + " - " + x.nombre.trim();
                $scope.closed();
            }
        }

        //digitar y validar hora//
        $scope.validarhora2 = function () {
            var reg = new RegExp("^([01]?[0-9]{1}|2[0-3]{1}):[0-5]{1}[0-9]{1}$");
            if ($scope.json.hora != ':') {
                if (!reg.test($scope.json.hora)) {
                    alert("La Hora Insertada es incorrecta");
                    $scope.json.hora = ':';
                    $('#hora').focus();
                }
               
            }
        }

        $scope.conceptoTiempo = function (d) {
            if (d != undefined && d != null) {
                $scope.conceptos.forEach(function (item, index) {
                    console.log(item);
                    console.log(d);
                    if (item.codigo == d) {
                        $scope.json.tiempo = parseInt(item.tiempo);
                    }
                });
            }
        }
        
    }
    function RecaudoCitaRESController($scope, $q, $timeout, ssDate, Service, Auth, Datos, Consultas) {
        $scope.json = {};
        $scope.table_gc = [];
        $scope.table_serv = [];
        $scope.showServ = false;
        $scope.showCan = false;
        $scope.cont = 1;
        $scope.sh_op = false;
        $scope.openWaiting = false;
        $scope.cont_cant = 0;
        $scope.cont_valor = 0;
        $scope.auxuser = Auth.getUserObj().usuario1;
        $scope.canasta = [];
        $scope.json.fecha = new Date();
        Service.getCrud({}, "/Salud.svc/PreloadRecaudo").then(function (d) {
            var data = JSON.parse(d.data);

            if (d.data[1] == '?') {
                alert(d.data);
            }
            $scope.causa = data[0];
            $scope.programa = data[1];
            $scope.centros = data[2];
            $scope.pago = data[3];
            $scope.tipo_cita = data[4];
            $scope.empresa = data[5];
            $scope.empresa.forEach(function (item, index) {
                if (item.estado != 2 || item.vigente != 1) {
                    $scope.empresa.splice(index, 1);
                }
            });
            $scope.formapago = data[6];
        });
        Service.getCrud({}, "/Salud2.svc/GetMedicos").then(function (d) {
            var rs = JSON.parse(d.data);
            if (rs[0] == '?') {
                alert(rs);
            } else {
                $scope.medicos = rs[0];
                $scope.json.historia = rs[1];
                $scope.admin = rs[2];
            }
        });
        $scope.abrirventana = function () {
            var currentButton = angular.element(document.getElementById('menucerrar'));
            $timeout(function () {
                currentButton.triggerHandler("click");
            });
        }
        $scope.abrirventana();

        $scope.BuscarPaciente = function () {
            console.log("x");
            if ($scope.json.codigo != null && $scope.json.codigo != undefined && $scope.json.codigo != "") {
                Service.getCrud({ codigo: $scope.json.codigo }, "/Salud2.svc/SearchPaciente").then(function (d) {
                    var rs = JSON.parse(d.data);
                    if (rs[0] == '?') {
                        alert(rs);
                    } else {
                        if (rs != null) {
                            $scope.json.codigo = rs.codigo;
                            $scope.json.paciente = rs.nombre.trim() + " " + rs.snombre.trim() + " " + rs.ppellido.trim() + " " + rs.sapellido.trim();
                        } else {
                            $scope.json.codigo = "";
                            $scope.json.paciente = "";
                        }

                    }
                })
            }
            
        }


        $scope.$watch('json.centro_costo', function (d) {
            if (d != undefined) {
                var query = " SELECT cc.grupo_fact, gr.nombre from ccostocontrol as cc inner join grupos as gr on cc.grupo_fact = gr.codigo where cc.codigo ='" + d + "'";
                Consultas.read(query).then(function (d) {
                    $scope.table_gc = d;
                });
            };
        });
        //MODAL SERVICIOS
        $scope.showmodalserv = function (x) {
            console.log(x);
            $scope.showServ = true;
            var query = "select codigo, nombre, valor, coopago from procedimientos where grupo_fact='" + x.grupo_fact + "' and desactivado=0";
            Consultas.read(query).then(function (d) {
                $scope.table_serv = d;
            });
        }
        $scope.closemostrar = function () {
            $scope.showServ = false;
        }
        //operaciones servicios
        $scope.op_serv = function (x) {
            $scope.sh_op = true;
            $scope.nombre = x.nombre.trim();
            $scope.codigo = x.codigo;
            $scope.valor = parseInt(x.valor);
            $scope.valor1 = $scope.valor;
        }

        $scope.sumar = function () {
            if ($scope.cont >= 1) {
                $scope.cont++;
                $scope.valor1 = $scope.valor * $scope.cont;

            }

        }
        $scope.restar = function () {
            if ($scope.cont > 1) {
                $scope.cont--;
                $scope.valor1 = $scope.valor * $scope.cont;

            }
        }
        $scope.agregar = function () {
            $scope.model = {};
            $scope.cont_cant = $scope.cont_cant + $scope.cont;
            $scope.cont_valor = $scope.cont_valor + $scope.valor1;
            $scope.model.nombre = $scope.nombre;
            $scope.model.codigo = $scope.codigo;
            $scope.model.valor = $scope.valor;
            $scope.model.valor1 = $scope.valor1;
            $scope.model.cntdad = $scope.cont;
            $scope.canasta.push($scope.model);
            console.log($scope.canasta);
            $scope.model = {};
            $scope.cont = 1;
            $scope.valor1 = 0;
            $scope.nombre = ''
            $scope.valor = 0;
            $scope.sh_op = false;
        }

        $scope.Actualizar = function () {
            console.log($scope.json);
            console.log($scope.canasta);
            console.log("ACTUALIZAR");
            Service.getCrud({ json: angular.toJson($scope.json), grilla: angular.toJson($scope.canasta), codigo_usuario: $scope.auxuser }, "/Salud2.svc/SaveIngresoIzamed").then(function (d) {
                var rs = JSON.parse(d.data);
                console.log(rs);
                if (rs > 0) {                    
                    $scope.canasta = [];
                    $scope.json = { fecha: new Date()};
                    $scope.closemostrar2();
                    alert("Registro guardado");
                } else {
                    alert(rs);
                }
                
            }) 
        }
        //MODAL CANASTA
        $scope.showCanasta = function (x) {
            $scope.showCan = true;
        }
        $scope.closemostrar2 = function () {
            $scope.showCan = false;
        }

        /*----------BUSCADOR DE CITAS----------------*/
        
        $scope.funcionauxiliar = function (x) {
            if (x == 1 || x == 2) {
                $('#tabla table tr').each(function () {
                    if ($.trim($(this).text()).length > 0) {
                        $(this).addClass("myclass");
                    }
                });
                $timeout(function () {
                    $("#colorear").addClass("myclass");
                });
            }
        }        



    }
    function cuadre_de_cajaController($scope, $q, $timeout, ssDate, Service, Auth, Datos, Consultas) {
        //inicializar//
        $scope.json = {};
        $scope.grilla_recibos = [];
        $scope.grilla_recibos2 = [];
        $scope.cod_user = Auth.getUserObj().codigo;
        $scope.json.usuario = $scope.cod_user;
        $scope.auxuser = Auth.getUserObj().usuario1;
        $scope.json.usuarioname = $scope.auxuser;
        $scope.dsbconsulta = true;
        $scope.dsbfecha = false;
        $scope.disbchek = false;
        $scope.actchek = 0;

       

        $scope.profilenivel = angular.toJson({ modulo: 'AGENDA MEDICA', ventana: '7', usuario: $scope.auxuser });
        /*PANEL PACIENTE*/
        $scope.showbtn = { buscar: false, imprimir: false, nuevo: true, editar: false, eliminar: false };
        $scope.disabledbtn = { buscar: false, imprimir: true, nuevo: false, editar: true, eliminar: true };



        /*aqui crud --> ver siguiente ejemplo*/
        //limpiar para nuevo//
        $scope.nuevo = function () {
            $scope.json = {};
            $scope.grilla_recibos = [];
            $scope.grilla_recibos = [];
            $scope.dsbconsulta = false;
            $scope.dsbfecha = false;
            $scope.json.saldo_caja = 0;
            $scope.json.total_egreso = 0;
            $scope.json.total_recaudo = 0;

            $timeout(function () {

                $scope.cod_user = Auth.getUserObj().codigo;
                $scope.json.usuario = $scope.cod_user;
                $scope.auxuser = Auth.getUserObj().usuario1;
                $scope.json.usuarioname = $scope.auxuser;
                $scope.json.fecha1 = new Date();
                $scope.json.fecha2 = new Date();

                var query2 = "select top 1 cuadre from caja_cuadre where usuario='" + $scope.auxuser + "' order by fecha,hora desc";
                Consultas.read(query2).then(function (d) {
                    var y = d[0];
                    $scope.json.saldo_anterior = parseInt(y.cuadre);//
                    console.log(y);
                });

            }, 1000);
        }
        //guardar//
        $scope.guardar = function () {
            var defender = $q.defer();
            var promise = defender.promise;
            if (jQuery.isEmptyObject($scope.json)) {
                alert("Por Favor Agregar datos");
                defender.reject(false);
            } else {
                $timeout(function () {
                    try {
                        $scope.grilla_recibos2 = [];
                        var rs = false;
                        $scope.grilla_recibos.forEach(function (item, index) {
                            if (item.check == 1) {
                                
                                $scope.grilla_recibos2.push(item);

                            }
                        });
                        if ($scope.grilla_recibos2.length > 0) {
                            Service.getCrud({ json: angular.toJson($scope.json), grilla_recibos: $scope.grilla_recibos2 }, "/Salud2.svc/CreateRecibosCuadre").then(function (d) {
                                rs = JSON.parse(d.data);
                                defender.resolve(rs);
                                if (rs > 0) {
                                    defered.reject(rs);
                                    $scope.consultar();
                                } else {
                                    alert(rs);
                                    $scope.consultar();
                                }
                                
                            });

                        } else {
                            alert("No selecciono ningun registro.")
                        }
                    } catch (e) {
                        defender.reject(false);
                        alert(e);
                    }
                });
               
            }

            return promise;
        };
        
        //BR 
        $scope.dataBuscador = {
            id: 'buscadorcuadre',
            component: [{ id: 'formcuadre_de_caja-buscar', type: 'btn' }],
            table: 'caja_cuadre',
            column: [
                { name: 'id', as: 'Id', visible: true },
                { name: 'fecha', as: 'Fecha', visible: true },
                { name: 'saldo_anterior', as: 'Saldo_Anterior', visible: true },
                { name: 'cuadre', as: 'Cuadre', visible: true },
                { name: 'usuario', as: 'Usuario', visible: true }
            ],
            where: [{ col: 'usuario', filter: "='" + Auth.getUserObj().usuario1 + "'", join: '' } ],
            groupby: false,
            orderby: [],
            title: 'Buscar Cuadres de caja'
        };
        $scope.rsBuscador = function (d) {
            var j = JSON.parse(d);
            $scope.dsbconsulta = true;
            $scope.dsbfecha = true;
            if (j != undefined && j != '') {
                $scope.id = j.Id;
                $timeout(function () {
                    try {
                        $scope.json.saldo_anterior = j.Saldo_Anterior;
                        $scope.json.saldo_caja = j.Cuadre;
                        $scope.json.total_recaudo = 0;
                        $scope.json.total_egreso = 0;
                        var rs = false;
                        if ($scope.id != null && $scope.id != undefined) {

                            var auxfecha = j.Fecha.split(" ")[0];
                            $scope.fecha1 = auxfecha.split("/")[0];
                            if ($scope.fecha1.length < 2) {
                                $scope.fecha1 = "0" + $scope.fecha1;
                            }
                            $scope.fecha2 = auxfecha.split("/")[1];
                            $scope.fecha3 = auxfecha.split("/")[2];
                           
                            var x = " select c1.fuente, c1.numero, c1.fecha, tp.forma_pago, c1.codigo, substring(p.ppellido, 1, Len(p.ppellido)) + ' ' + substring(p.sapellido, 1, Len(p.sapellido)) + ' ' + ";
                            x += " substring(p.nombre, 1, Len(p.nombre)) + ' ' + substring(p.snombre, 1, Len(p.snombre)) as paciente, t.nombre,";
                            x += " tp.valor, c1.usuario, c1.cuadre ";
                            x += " from caja01 c1 with (index(sistema))";
                            x += " inner join tipo_forma_pago tp on c1.id_web = tp.fk_caja01 ";
                            x += " left join paciente p with (index(codigo)) on p.codigo = c1.codigo ";
                            x += " left join terceros t with (index(codigo)) on t.codigo = c1.empresa where c1.cuadre ='" + j.Id + "' and c1.usuario = '" + j.Usuario + "' and c1.fecha='" + $scope.fecha3 + $scope.fecha2 + $scope.fecha1 + "'";
                           
                            Consultas.read(x).then(function (d) {
                                var rs = d;
                                console.log(rs);
                                if (rs.length > 0) {
                                    $scope.tabla_res = rs;

                                    $scope.tabla_res.forEach(function (item, index) {
                                        $scope.x = {};
                                        $scope.x.recibo = item.numero;
                                        $scope.x.fecha = item.fecha.split(" ")[0];
                                        $scope.x.forma_pago = item.forma_pago;
                                        $scope.x.paciente = item.paciente;
                                        $scope.x.recaudo = item.valor;
                                        $scope.x.egreso = 0;
                                        $scope.x.cuadre = item.cuadre;
                                        $scope.grilla_recibos.push($scope.x);


                                        $scope.json.total_egreso = $scope.json.total_egreso + parseFloat($scope.x.egreso);
                                        $scope.json.total_recaudo = $scope.json.total_recaudo + parseFloat($scope.x.recaudo);
                                       
                                    });
                                    $scope.disbchek = true;
                                    $scope.openWaiting = false;
                                }
                                else {
                                    $scope.openWaiting = false;
                                    alert("Laconsulta no arrojo resultados recaudos");
                                }
                            })

                            var query = "select ce.id,ce.fecha,ce.numero,ce.tipo_egreso,ce.valor,ce.tercero,ce.usuario,t.nombre   from caja_egresos ce left join terceros t with (index(codigo)) on t.codigo = ce.tercero where ce.cuadre_id ='" + $scope.id + "' and ce.usuario = '" + j.Usuario + "' and ce.fecha between '" + $scope.fecha3 + $scope.fecha2 + $scope.fecha1 + "' and '" + $scope.fecha3 + $scope.fecha2 + $scope.fecha1 + "' ;";
                            Consultas.read(query).then(function (d) {
                                if (d != null) {
                                    var rs = d;
                                    if (rs.length > 0) {
                                        console.log(rs);
                                        $scope.tabla_res2 = rs;
                                        $scope.tabla_res2.forEach(function (item, index) {
                                            $scope.x = {};
                                            $scope.x.recibo = item.numero;
                                            $scope.x.fecha = item.fecha.split(" ")[0];
                                            $scope.x.forma_pago = item.tipo_egreso;
                                            $scope.x.tercero = item.tercero;
                                            $scope.x.paciente = item.nombre;
                                            $scope.x.recaudo = 0;
                                            $scope.x.egreso = item.valor;
                                            $scope.x.cuadre = item.cuadre;
                                            $scope.grilla_recibos.push($scope.x);

                                            $scope.json.total_egreso = $scope.json.total_egreso + parseFloat($scope.x.egreso);
                                            $scope.json.total_recaudo = $scope.json.total_recaudo + parseFloat($scope.x.recaudo);
                                           
                                        });
                                        $scope.disbchek = true;
                                    }
                                    else {
                                        $scope.openWaiting = false;
                                        alert("La consulta no arrojo resultados de egresos");
                                    }

                                }
                            });

                        }
                    } catch (e) {
                        defender.reject(false);
                        alert(e);
                    }
                });

            }
            
            
        }

        //funciones
        $scope.consultar = function () {
            $scope.grilla_recibos = [];
            if ($scope.json.fecha1 != null && $scope.json.fecha2 !=null) {

                $scope.fecha1 = (ssDate.dateToString($scope.json.fecha1, 'dd')).split("'")[0];
                $scope.fecha2 = (ssDate.dateToString($scope.json.fecha1, 'MM')).split("'")[0];
                $scope.fecha3 = (ssDate.dateToString($scope.json.fecha1, 'yyyy')).split("'")[0];

                $scope.fecha4 = (ssDate.dateToString($scope.json.fecha2, 'dd')).split("'")[0];
                $scope.fecha5 = (ssDate.dateToString($scope.json.fecha2, 'MM')).split("'")[0];
                $scope.fecha6 = (ssDate.dateToString($scope.json.fecha2, 'yyyy')).split("'")[0];
                var x = "EXEC[dbo].[recibos_para_cuadre_caja01]";
                x += "@usuario = '" + $scope.json.usuarioname + "',";
                x += "@cuadre =  '0',";
                x += "@fecha1 = '" + $scope.fecha3 + $scope.fecha2 + $scope.fecha1 + "',";
                x += "@fecha2 ='" + $scope.fecha6 + $scope.fecha5 + $scope.fecha4 + "'";
                Consultas.read(x).then(function (d) {
                    var rs = d;
                    console.log(rs);
                    if (rs.length > 0) {
                        $scope.tabla_res = rs;

                        $scope.tabla_res.forEach(function (item, index) {
                            $scope.x = {};
                            $scope.x.recibo = item.numero;
                            $scope.x.fecha = item.fecha.split(" ")[0];
                            $scope.x.forma_pago = item.forma_pago;
                            $scope.x.paciente = item.paciente;
                            $scope.x.recaudo = item.valor;
                            $scope.x.egreso = 0;
                            $scope.x.cuadre = item.cuadre;
                            $scope.grilla_recibos.push($scope.x);
                        });

                        $scope.openWaiting = false;
                    }
                    else {
                        $scope.openWaiting = false;
                        alert("Laconsulta no arrojo resultados recaudos");
                    }
                })

                var query = "select ce.id,ce.fecha,ce.numero,ce.tipo_egreso,ce.valor,ce.tercero,ce.usuario,t.nombre   from caja_egresos ce left join terceros t with (index(codigo)) on t.codigo = ce.tercero where ce.cuadre_id = 0 and ce.usuario = '" + $scope.json.usuarioname + "' and ce.fecha between '" + ssDate.dateToString($scope.json.fecha1, 'yyyyMMdd') + "' and '" + ssDate.dateToString($scope.json.fecha2, 'yyyyMMdd') +"' ;";                   
                Consultas.read(query).then(function (d) {
                    if (d != null) {
                        var rs = d;
                        if (rs.length > 0) {
                            console.log(rs);
                            $scope.tabla_res2 = rs;
                            $scope.tabla_res2.forEach(function (item, index) {
                                $scope.x = {};
                                $scope.x.recibo = item.numero;
                                $scope.x.fecha = item.fecha.split(" ")[0];
                                $scope.x.forma_pago = item.tipo_egreso;
                                $scope.x.tercero = item.tercero;
                                $scope.x.paciente = item.nombre;
                                $scope.x.recaudo =0;
                                $scope.x.egreso = item.valor;
                                $scope.x.cuadre = item.cuadre;
                                $scope.grilla_recibos.push($scope.x);
                            });
                        }
                        else {
                            $scope.openWaiting = false;
                            alert("La consulta no arrojo resultados de egresos");
                        }
                        
                    }
                });
            }

           
        }

        $scope.checkcuadre = function (item, index) {
            if (item != null) {
                console.log(item);
                if (item.check == 1) {
                    $scope.json.total_egreso = $scope.json.total_egreso + parseFloat(item.egreso);
                    $scope.json.total_recaudo = $scope.json.total_recaudo + parseFloat(item.recaudo);
                    $scope.json.saldo_caja = $scope.json.total_recaudo - $scope.json.total_egreso;

                } else {
                    $scope.json.total_egreso = $scope.json.total_egreso - parseFloat(item.egreso);
                    $scope.json.total_recaudo = $scope.json.total_recaudo - parseFloat(item.recaudo);
                    $scope.json.saldo_caja = $scope.json.total_recaudo - $scope.json.total_egreso;
                }
            }
        }
        $scope.todoschek = function () {
            $scope.grilla_recibos.forEach(function (item, index) {
                if ($scope.actchek == 1) {
                    item.check = 1;
                    $scope.checkcuadre(item, index);

                } else {
                    item.check = 0;
                    $scope.checkcuadre(item, index);
                }
               

            });


        }

       
    }
    function caja_egresoController($scope, $q, $timeout, ssDate, Service, Auth, Datos, Consultas) {
        //inicializar//
        $scope.json = {};
        $scope.tipo_egreso = [];
        $scope.cod_user = Auth.getUserObj().codigo;
        $scope.json.usuario = $scope.cod_user;
        $scope.auxuser = Auth.getUserObj().usuario1;
        $scope.json.usuarioname = $scope.auxuser;
        $scope.dsbconsulta = true;
        $scope.key = { value: '' };
        $scope.key2 = { value: '' };


        var query2 = "select id, nombre from forma_pago ";
        Consultas.read(query2).then(function (d) {
            var y = d;
            $scope.tipo_egreso = y;//
            console.log(y);
        });




        $scope.profilenivel = angular.toJson({ modulo: 'AGENDA MEDICA', ventana: '7', usuario: $scope.auxuser });
        /*PANEL PACIENTE*/
        $scope.showbtn = { buscar: false, imprimir: false, nuevo: true, editar: false, eliminar: false };
        $scope.disabledbtn = { buscar: false, imprimir: true, nuevo: false, editar: true, eliminar: true };



        /*aqui crud --> ver siguiente ejemplo*/
        //limpiar para nuevo//
        $scope.nuevo = function () {
            $scope.json = {};
            $timeout(function () {
                $scope.cod_user = Auth.getUserObj().codigo;
                $scope.json.usuario = $scope.cod_user;
                $scope.auxuser = Auth.getUserObj().usuario1;
                $scope.json.usuarioname = $scope.auxuser;
                $scope.json.fecha = new Date();
                $scope.key2.value = 5312;
            }, 1000);
        }
        //guardar//
        $scope.guardar = function () {
            $scope.json.horas = $scope.json.horas + "";
            var defender = $q.defer();
            var promise = defender.promise;
            if (jQuery.isEmptyObject($scope.json)) {
                alert("Por Favor Agregar Datos");
                defender.reject(false);
            } else {
                $timeout(function () {
                    try {
                        var rs = false;
                        Service.getCrud({ json: angular.toJson($scope.json) }, "/Salud2.svc/CreateEgreso").then(function (d) {
                            rs = JSON.parse(d.data);
                            defender.resolve(rs);
                            if (rs == false) {
                                defered.reject(rs);
                                $scope.refresh();
                            }
                            if (rs == true) {
                              

                            }
                        });
                    } catch (e) {
                        defered.reject(e);
                    }
                });
                return promise;
            }
        };
      
        //BR 
        $scope.dataTercero = {
            id: 'buscadorTercero',
            component: [{ id: 'brTercero', type: '' }],
            table: 'terceros',
            column: [
                { name: 'codigo', as: 'Codigo', visible: true },
                { name: 'nombre', as: 'Nombre', visible: true },
            ],
            where: [],
            groupby: false,
            orderby: [],
            title: 'Buscar Tercero'
        };
        $scope.rsTercero = function (d) {
            var j = JSON.parse(d);
            $scope.json.tercero = j.Codigo;
        }
        $scope.dataBuscador = {
            id: 'buscadorBuscador',
            component: [{ id: 'formcaja_egreso-buscar', type: 'btn' }],
            table: 'caja_egresos',
            column: [
                { name: 'id', as: 'id', visible: false },
                { name: 'numero', as: 'Nunmero', visible: true },
                { name: 'fecha', as: 'Fecha', visible: true },
                { name: 'valor', as: 'Valor', visible: true },
            ],
            where: [],
            groupby: false,
            orderby: [],
            title: 'Buscar Egresos'
        };
        $scope.rsBuscador = function (d) {
            var j = JSON.parse(d);
            if (j != undefined && j != '') {
                $scope.id = j.id;
                Service.getCrud({ codigo: $scope.cod, id: $scope.id, historia: $scope.histo }, "/Salud2.svc/ReadEgreso").then(function (da) {
                    var d = JSON.parse(da.data);
                    $scope.json = d;
                    $scope.json.usuarioname = $scope.json.usuario;

                    $scope.key.value = d.tercero;
                    

                });

            }
        }
        $scope.data_fuente = {
            id: 'buscadorFuentes',
            component: [{ id: 'Fuentes-buscar', type: 'btn' }],
            table: 'fuentes',
            column: [
                { name: 'id_web', as: 'Id', visible: true },
                { name: 'nombre', as: 'Nombre', visible: true },
                { name: 'consecutivo', as: 'Consecutivo', visible: true },
                { name: 'fuente', as: 'Fuente', visible: false   },
                { name: 'concepto', as: 'Concepto', visible: false }
            ],
            inner_join: [],
            left_join: [],
            where: [],
            groupby: false,
            orderby: [],
            title: 'Fuentes Contables'
        };
        $scope.rs_fuente = function (v) {
            if (v != undefined) {
                var x = JSON.parse(v);
                $scope.json.conceptos = x.Id;
                $scope.json.numero = parseInt(x.Consecutivo) + 1;

            }
        };
        

    }

})();


